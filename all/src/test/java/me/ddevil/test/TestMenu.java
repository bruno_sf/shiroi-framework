package me.ddevil.test;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.ui.internal.scrollable.LowPanedScrollable;
import me.ddevil.shiroi.ui.menu.ShiroiMenu;
import me.ddevil.shiroi.ui.scrollable.ShiroiScrollableUpdater;
import org.bukkit.Material;

public class TestMenu extends ShiroiMenu<PrivatePlugin<?, ?>> {
    private final LowPanedScrollable<TestItem> items;

    public TestMenu(PrivatePlugin<?, ?> plugin, String name, int totalLanes) {
        super(plugin, name, totalLanes);
        items = new LowPanedScrollable<>(4, 4, new ShiroiScrollableUpdater(Material.EMERALD, plugin));
    }


    @Override
    protected void update0() {

    }

    @Override
    protected void setup1() {

    }
}
