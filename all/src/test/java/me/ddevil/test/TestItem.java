package me.ddevil.test;

import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import me.ddevil.shiroi.ui.internal.misc.ClickableItem;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 06/12/2016.
 */
public class TestItem extends ClickableItem {
    public TestItem(ItemStack itemStack, Action action) {
        super(itemStack, action);
    }

    public TestItem(ItemUpdater updater, Action action) {
        super(updater, action);
    }

    public TestItem(ItemStack itemStack) {
        super(itemStack);
    }

    public TestItem(ItemUpdater updater) {
        super(updater);
    }
}
