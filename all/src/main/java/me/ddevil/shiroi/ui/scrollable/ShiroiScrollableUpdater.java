package me.ddevil.shiroi.ui.scrollable;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.message.MessageColor;
import me.ddevil.shiroi.message.MessageManager;
import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.api.updater.ScrollableUpdater;
import me.ddevil.shiroi.ui.internal.scrollable.updater.BasicScrollableUpdater;
import me.ddevil.shiroi.ui.misc.ScrollDirection;
import me.ddevil.shiroi.util.item.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;

public class ShiroiScrollableUpdater implements ScrollableUpdater<Scrollable<?>> {

    private final ItemStack icon;
    private final String previous;
    private final String next;
    private final MessageManager messageManager;

    public ShiroiScrollableUpdater(Material icon, PrivatePlugin plugin) {
        this(new ItemStack(icon), plugin);
    }

    public ShiroiScrollableUpdater(ItemStack icon, PrivatePlugin plugin) {
        this(icon, plugin, BasicScrollableUpdater.DEFAULT_NEXT, BasicScrollableUpdater.DEFAULT_PREVIOUS);
    }

    public ShiroiScrollableUpdater(ItemStack icon, PrivatePlugin plugin, String next, String previous) {
        this.icon = icon;
        this.messageManager = plugin.getMessageManager();
        this.next = next;
        this.previous = previous;
    }

    @Override
    public ItemStack update(Scrollable<?> scrollable, ScrollDirection direction) {
        return new ItemBuilder(new ItemStack(icon), messageManager)
                .setName(MessageColor.PRIMARY.toString() + (direction == ScrollDirection.NEXT ? next : previous))
                .setLore(Collections.singletonList(MessageColor.PRIMARY.toString() + (scrollable.getCurrentIndex() + 1) + MessageColor.NEUTRAL + "/" + MessageColor.SECONDARY + scrollable.getTotalPages()))
                .toItemStack();
    }
}
