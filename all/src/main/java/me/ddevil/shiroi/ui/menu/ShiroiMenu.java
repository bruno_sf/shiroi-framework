package me.ddevil.shiroi.ui.menu;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.ui.internal.menu.AutonomousMenu;
import org.bukkit.inventory.Inventory;

/**
 * Created by bruno on 06/12/2016.
 */
public abstract class ShiroiMenu<P extends PrivatePlugin<?, ?>> extends AutonomousMenu<P> {
    public ShiroiMenu(P plugin, String name, int totalLanes) {
        super(plugin, plugin.getMessageManager().translateAll(name), totalLanes);
    }

    public ShiroiMenu(P plugin, Inventory bukkitInventory) {
        super(plugin, bukkitInventory);
    }
}
