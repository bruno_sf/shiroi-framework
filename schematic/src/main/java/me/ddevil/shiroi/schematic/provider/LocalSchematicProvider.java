package me.ddevil.shiroi.schematic.provider;

import me.ddevil.shiroi.util.vector.Vector3;
import me.ddevil.shiroi.schematic.Schematic;
import me.ddevil.shiroi.schematic.SchematicConstants;
import me.ddevil.shiroi.schematic.SchematicMaterialType;
import org.apache.commons.lang.WordUtils;
import org.jnbt.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by bruno on 09/10/2016.
 */
public class LocalSchematicProvider implements SchematicProvider {
    private File schematicFolder;

    public LocalSchematicProvider(File schematicFolder) {
        this.schematicFolder = schematicFolder;
    }

    public File getSchematicFolder() {
        return schematicFolder;
    }

    public void setSchematicFolder(File schematicFolder) {
        this.schematicFolder = schematicFolder;
    }

    public Schematic provide(File file) {
        try {
            NBTInputStream nbt = new NBTInputStream(new FileInputStream(file));
            Tag tag = nbt.readTag();
            Map<String, Tag> value = (Map<String, Tag>) tag.getValue();
            System.out.println(value);
            byte[] blocks = ((ByteArrayTag) value.get(Schematic.BLOCKS_IDENTIFIER)).getValue();
            byte[] data = ((ByteArrayTag) value.get(Schematic.DATA_IDENTIFIER)).getValue();
            System.out.println(Arrays.toString(data));
            SchematicMaterialType materialType = SchematicMaterialType.valueOf(((StringTag) value.get(Schematic.MATERIALS_IDENTIFIER)).getValue().toUpperCase());
            short height = ((ShortTag) value.get(Schematic.HEIGHT_IDENTIFIER)).getValue();
            short width = ((ShortTag) value.get(Schematic.WIDTH_IDENTIFIER)).getValue();
            short length = ((ShortTag) value.get(Schematic.LENGTH_IDENTIFIER)).getValue();
            Vector3<Integer> origin;
            Vector3<Integer> offset;
            if (hasWorldEditInfo(value)) {
                int originX = ((IntTag) value.get(Schematic.WE_ORIGIN_X_IDENTIFIER)).getValue();
                int originY = ((IntTag) value.get(Schematic.WE_ORIGIN_Y_IDENTIFIER)).getValue();
                int originZ = ((IntTag) value.get(Schematic.WE_ORIGIN_Z_IDENTIFIER)).getValue();
                origin = new Vector3<>(originX, originY, originZ);

                int offsetX = ((IntTag) value.get(Schematic.WE_OFFSET_X_IDENTIFIER)).getValue();
                int offsetY = ((IntTag) value.get(Schematic.WE_OFFSET_Y_IDENTIFIER)).getValue();
                int offsetZ = ((IntTag) value.get(Schematic.WE_OFFSET_Z_IDENTIFIER)).getValue();
                offset = new Vector3<>(offsetX, offsetY, offsetZ);
            } else {
                origin = Vector3.INT_ZERO_3;
                offset = Vector3.INT_ZERO_3;
            }
            List<Tag> entities = ((ListTag) value.get(Schematic.ENTITIES_IDENTIFIER)).getValue();
            List<Tag> tileEntities = ((ListTag) value.get(Schematic.TILE_ENTITIES_IDENTIFIER)).getValue();
            return new Schematic(origin, offset, length, width, height, materialType, blocks, data, tileEntities, entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean hasWorldEditInfo(Map<String, Tag> value) {
        return value.containsKey(Schematic.WE_ORIGIN_X_IDENTIFIER) &&
                value.containsKey(Schematic.WE_ORIGIN_Y_IDENTIFIER) &&
                value.containsKey(Schematic.WE_ORIGIN_Z_IDENTIFIER) &&
                value.containsKey(Schematic.WE_OFFSET_X_IDENTIFIER) &&
                value.containsKey(Schematic.WE_OFFSET_Y_IDENTIFIER) &&
                value.containsKey(Schematic.WE_OFFSET_Z_IDENTIFIER);
    }

    @Override
    public Schematic provide(String name) {
        return provide(getFile(name));
    }

    @Override
    public void save(Schematic schematic, String name) throws Exception {
        Map<String, Tag> out = new HashMap<>();
        out.put(Schematic.BLOCKS_IDENTIFIER, new ByteArrayTag(Schematic.BLOCKS_IDENTIFIER, schematic.getBlocks()));
        out.put(Schematic.DATA_IDENTIFIER, new ByteArrayTag(Schematic.DATA_IDENTIFIER, schematic.getData()));
        out.put(Schematic.HEIGHT_IDENTIFIER, new ShortTag(Schematic.HEIGHT_IDENTIFIER, schematic.getHeight()));
        out.put(Schematic.LENGTH_IDENTIFIER, new ShortTag(Schematic.LENGTH_IDENTIFIER, schematic.getLength()));
        out.put(Schematic.WIDTH_IDENTIFIER, new ShortTag(Schematic.WIDTH_IDENTIFIER, schematic.getWidth()));
        Vector3<Integer> origin = schematic.getOrigin();
        out.put(Schematic.WE_ORIGIN_X_IDENTIFIER, new IntTag(Schematic.WE_ORIGIN_X_IDENTIFIER, origin.getX()));
        out.put(Schematic.WE_ORIGIN_Y_IDENTIFIER, new IntTag(Schematic.WE_ORIGIN_Y_IDENTIFIER, origin.getY()));
        out.put(Schematic.WE_ORIGIN_Z_IDENTIFIER, new IntTag(Schematic.WE_ORIGIN_Z_IDENTIFIER, origin.getZ()));
        Vector3<Integer> offset = schematic.getOffset();
        out.put(Schematic.WE_OFFSET_X_IDENTIFIER, new IntTag(Schematic.WE_OFFSET_X_IDENTIFIER, offset.getX()));
        out.put(Schematic.WE_OFFSET_Y_IDENTIFIER, new IntTag(Schematic.WE_OFFSET_Y_IDENTIFIER, offset.getY()));
        out.put(Schematic.WE_OFFSET_Z_IDENTIFIER, new IntTag(Schematic.WE_OFFSET_Z_IDENTIFIER, offset.getZ()));
        out.put(Schematic.TILE_ENTITIES_IDENTIFIER, new ListTag(Schematic.TILE_ENTITIES_IDENTIFIER, CompoundTag.class, schematic.getTileEntities()));
        out.put(Schematic.ENTITIES_IDENTIFIER, new ListTag(Schematic.ENTITIES_IDENTIFIER, CompoundTag.class, schematic.getEntities()));
        out.put(Schematic.MATERIALS_IDENTIFIER, new StringTag(Schematic.MATERIALS_IDENTIFIER, WordUtils.capitalize(schematic.getMaterialType().name().toLowerCase())));
        NBTOutputStream stream = new NBTOutputStream(new FileOutputStream(getFile(name)));
        stream.writeTag(new CompoundTag(Schematic.SCHEMATIC_TAG_IDENTIFIER, out));

        stream.close();
    }

    private File getFile(String name) {
        if (schematicFolder == null) {
            throw new IllegalArgumentException("There is no schematic folder defined!");
        }
        return new File(schematicFolder, name + SchematicConstants.SCHEMATIC_EXTENSION);
    }
}
