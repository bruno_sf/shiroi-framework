package me.ddevil.shiroi.schematic;

/**
 * Created by bruno on 09/10/2016.
 */
public enum SchematicMaterialType {
    CLASSIC,
    ALPHA;
}
