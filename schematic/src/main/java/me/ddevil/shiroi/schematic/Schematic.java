package me.ddevil.shiroi.schematic;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Bytes;
import me.ddevil.shiroi.misc.Serializable;
import me.ddevil.shiroi.util.vector.Vector3;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.jnbt.Tag;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Created by bruno on 09/10/2016.
 */
public class Schematic implements Serializable, ConfigurationSerializable {
    public static final String BLOCKS_IDENTIFIER = "Blocks";
    public static final String MATERIALS_IDENTIFIER = "Materials";
    public static final String HEIGHT_IDENTIFIER = "Height";
    public static final String WIDTH_IDENTIFIER = "Width";
    public static final String WE_ORIGIN_X_IDENTIFIER = "WEOriginX";
    public static final String WE_ORIGIN_Y_IDENTIFIER = "WEOriginY";
    public static final String WE_ORIGIN_Z_IDENTIFIER = "WEOriginZ";
    public static final String WE_OFFSET_X_IDENTIFIER = "WEOffsetX";
    public static final String WE_OFFSET_Y_IDENTIFIER = "WEOffsetY";
    public static final String WE_OFFSET_Z_IDENTIFIER = "WEOffsetZ";
    public static final String DATA_IDENTIFIER = "Data";
    public static final String ENTITIES_IDENTIFIER = "Entities";
    public static final String TILE_ENTITIES_IDENTIFIER = "TileEntities";
    public static final String LENGTH_IDENTIFIER = "Length";
    public static final String SCHEMATIC_TAG_IDENTIFIER = "Schematic";
    private Vector3<Integer> origin;
    private Vector3<Integer> offset;
    private final short length;
    private final short width;
    private final short height;
    private final SchematicMaterialType materialType;
    private byte[] blocks;
    private byte[] data;
    private List<Tag> tileEntities;
    private List<Tag> entities;

    public Schematic(Vector3<Integer> origin, Vector3<Integer> offset, short length, short width, short height, SchematicMaterialType materialType, byte[] blocks, byte[] data, List<Tag> tileEntities, List<Tag> entities) {
        this.origin = origin;
        this.offset = offset;
        this.length = length;
        this.width = width;
        this.height = height;
        this.materialType = materialType;
        this.blocks = blocks;
        this.data = data;
        this.tileEntities = tileEntities;
        this.entities = entities;
    }

    public SchematicMaterialType getMaterialType() {
        return materialType;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(BLOCKS_IDENTIFIER, Bytes.asList(blocks))
                .put(HEIGHT_IDENTIFIER, height)
                .put(WIDTH_IDENTIFIER, width)
                .put(LENGTH_IDENTIFIER, length)
                .build();
    }

    public Vector3<Integer> getOrigin() {
        return origin;
    }

    public void setOrigin(Vector3<Integer> origin) {
        this.origin = origin;
    }

    public Vector3<Integer> getOffset() {
        return offset;
    }

    public void setOffset(Vector3<Integer> offset) {
        this.offset = offset;
    }

    public short getLength() {
        return length;
    }

    public short getWidth() {
        return width;
    }

    public short getHeight() {
        return height;
    }

    public byte[] getBlocks() {
        return blocks;
    }

    public void setBlocks(byte[] blocks) {
        this.blocks = blocks;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public List<Tag> getTileEntities() {
        return tileEntities;
    }

    public void setTileEntities(List<Tag> tileEntities) {
        this.tileEntities = tileEntities;
    }

    public List<Tag> getEntities() {
        return entities;
    }

    public void setEntities(List<Tag> entities) {
        this.entities = entities;
    }

    public void place(Location location) {
        World w = location.getWorld();
        for (int x = 0; x < length; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < width; z++) {
                    int index = y * width * length + z * width + x;
                    Material type = Material.getMaterial(blocks[z]);
                    Block block = w.getBlockAt(x + location.getBlockX(), y + location.getBlockY(), z + location.getBlockZ());
                    block.setType(type);
                    block.setData(data[index]);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Schematic{" +
                "length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", materialType=" + materialType +
                '}';
    }
}
