package me.ddevil.shiroi.schematic.provider;

import me.ddevil.shiroi.schematic.Schematic;

/**
 * Created by bruno on 09/10/2016.
 */
public interface SchematicProvider {
    Schematic provide(String name);

    void save(Schematic schematic, String name) throws Exception;
}
