package me.ddevil.shiroi.schematic;


import me.ddevil.shiroi.util.vector.Vector3;
import me.ddevil.shiroi.util.area.Cuboid;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.Collections;

/**
 * Created by bruno on 09/10/2016.
 */
public class SchematicFactory {
    public static Schematic from(World world, int x1, int y1, int z1, int x2, int y2, int z2) {
        int minX = Math.min(x1, x2);
        int minY = Math.min(y1, y2);
        int minZ = Math.min(z1, z2);
        int maxX = Math.max(x1, x2);
        int maxY = Math.max(y1, y2);
        int maxZ = Math.max(z1, z2);
        short height = (short) (maxY - minY);
        short width = (short) (maxZ - minZ);
        short length = (short) (maxX - minX);
        byte[] blocks = new byte[height * width * length];
        byte[] data = new byte[height * width * length];
        for (int x = minX; x < maxX; x++) {
            for (int y = minY; y < maxY; y++) {
                for (int z = minZ; z < maxZ; z++) {
                    int index = y * width * length + z * width + x;
                    Block block = world.getBlockAt(x, y, z);
                    blocks[index] = (byte) block.getType().getId();
                    data[index] = block.getData();
                }
            }
        }
        return new Schematic(
                Vector3.INT_ZERO_3,
                Vector3.INT_ZERO_3,
                length,
                width,
                height,
                SchematicMaterialType.ALPHA,
                blocks,
                data,
                Collections.emptyList(),
                Collections.emptyList()
        );
    }

    public static Schematic from(Location pos1, Location pos2) {
        if (!pos1.getWorld().equals(pos2.getWorld())) {
            throw new IllegalArgumentException("pos1 and pos2 are from different worlds!");
        }
        return from(pos1.getWorld(),
                pos1.getBlockX(),
                pos1.getBlockY(),
                pos1.getBlockZ(),
                pos2.getBlockX(),
                pos2.getBlockY(),
                pos2.getBlockZ());
    }

    public static Schematic from(Cuboid cuboid, World world) {
        return from(world, cuboid.getLowerX(), cuboid.getLowerY(), cuboid.getLowerZ(), cuboid.getUpperX(), cuboid.getUpperY(), cuboid.getUpperZ());
    }
}
