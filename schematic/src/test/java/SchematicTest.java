import me.ddevil.shiroi.schematic.Schematic;
import me.ddevil.shiroi.schematic.provider.LocalSchematicProvider;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.io.File;

/**
 * Created by bruno on 09/10/2016.
 */
public class SchematicTest {
    private static final File resFolder = new File("C:\\Work\\projects\\shiroi-framework\\schematic\\src\\test\\resources");
    private static final String secondName = "oin";

    public static void main(String[] args) {
        LocalSchematicProvider localSchematicProvider = new LocalSchematicProvider(resFolder);
        Schematic modernHouse = localSchematicProvider.provide("modern_house");
        try {
            localSchematicProvider.save(modernHouse, secondName);
            Schematic provide = localSchematicProvider.provide(secondName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
