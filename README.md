![ShiroiFramework](http://i.imgur.com/DKxUW1Q.png)
A framework that makes developing plugins much more easier and fun :D
Some features:

* UI support, allows you to easily create and edit Menus 
* Geometry shapes and math utils
* It's sexy
* Debugging utils
* Good humour
* Did I mention it's sexy?
