package me.ddevil.shiroi.effect.particle;

import me.ddevil.shiroi.effect.geometry.GeometricShape;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by ddevil on 04/09/2016.
 */
public class ParticleDisplayer extends BukkitRunnable {
    private final Particle particle;
    private final GeometricShape shape;

    public ParticleDisplayer(Particle particle, GeometricShape shape) {
        this.particle = particle;
        this.shape = shape;
    }

    @Override
    public void run() {
        shape.display(particle);
    }
}
