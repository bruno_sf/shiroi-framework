/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect.geometry.shape;

import me.ddevil.shiroi.effect.geometry.BaseGeometricShape;
import me.ddevil.shiroi.effect.geometry.GeometryResolution;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BRUNO II on 09/08/2016.
 */
public class LineShape extends BaseGeometricShape {
    public LineShape(World world, Vector pos1, Vector pos2, int totalPoints) {
        super(getLine(world, pos1, pos2, totalPoints));
    }

    public LineShape(Location pos1, Location pos2, int total) {
        this(pos1.getWorld(), pos1.toVector(), pos2.toVector(), total);
    }

    public LineShape(World world, Vector pos1, Vector pos2, GeometryResolution resolution) {
        this(world, pos1, pos2, resolution.getResolution());
    }

    public LineShape(Location pos1, Location pos2, GeometryResolution resolution) {
        this(pos1, pos2, resolution.getResolution());
    }

    public static List<Location> getLine(World world, Vector pos1, Vector pos2, GeometryResolution resolution) {
        return getLine(world, pos1, pos2, resolution.getResolution());
    }

    public static List<Location> getLine(World world, Vector pos1, Vector pos2, int totalPoints) {
        //Find out info
        Vector direction = pos2.clone().subtract(pos1);
        direction.normalize();
        double distance = pos1.distance(pos2);
        double distancePerPoint = distance / totalPoints;
        direction.multiply(distancePerPoint);

        //Find out points
        ArrayList<Location> points = new ArrayList();
        Location point = pos1.toLocation(world);
        for (int i = 0; i < totalPoints; i++) {
            point.add(direction);
            points.add(point.clone());
        }
        return points;
    }
}
