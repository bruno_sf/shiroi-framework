/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by BRUNO II on 20/07/2016.
 */
public class FireworkExplosion {
    private static class RandomGenerator {
        //Make the arraylists for the colors and api
        static ArrayList<Color> colors = new ArrayList<Color>();
        static ArrayList<FireworkEffect.Type> types = new ArrayList<FireworkEffect.Type>();
        static Random random = new Random();

        public static FireworkEffect.Type getRandomType() {
            int size = types.size();
            FireworkEffect.Type theType = types.get(random.nextInt(size));
            return theType;
        }


        public static Color getRandomColor() {
            int size = colors.size();
            Color color = colors.get(random.nextInt(size));
            return color;
        }

        public static void addColors() {
            colors.add(Color.WHITE);
            colors.add(Color.PURPLE);
            colors.add(Color.RED);
            colors.add(Color.GREEN);
            colors.add(Color.AQUA);
            colors.add(Color.BLUE);
            colors.add(Color.FUCHSIA);
            colors.add(Color.GRAY);
            colors.add(Color.LIME);
            colors.add(Color.MAROON);
            colors.add(Color.YELLOW);
            colors.add(Color.SILVER);
            colors.add(Color.TEAL);
            colors.add(Color.ORANGE);
            colors.add(Color.OLIVE);
            colors.add(Color.NAVY);
            colors.add(Color.BLACK);
        }

        public static void addTypes() {
            types.add(FireworkEffect.Type.BURST);
            types.add(FireworkEffect.Type.BALL);
            types.add(FireworkEffect.Type.BALL_LARGE);
            types.add(FireworkEffect.Type.CREEPER);
            types.add(FireworkEffect.Type.STAR);
        }

        public static FireworkEffect randomEffect() {
            Color primary = getRandomColor();
            Color darkColor = primary.mixColors(Color.BLACK);
            FireworkEffect.Builder builder = FireworkEffect.builder()
                    .withColor(primary, primary.mixColors(Color.WHITE))
                    .withFade(primary, darkColor, darkColor.mixColors(Color.BLACK))
                    .with(getRandomType());
            if (random.nextBoolean()) {
                builder.withFlicker();
            }
            if (random.nextBoolean()) {
                builder.withTrail();
            }
            return builder.build();
        }
    }

    private final Location location;
    private final Plugin plugin;
    private final FireworkEffect effect;

    public static FireworkEffect randomEffect() {
        return RandomGenerator.randomEffect();
    }

    public FireworkExplosion(Location location, Plugin plugin) {
        this(location, plugin, randomEffect());
    }

    public FireworkExplosion(Location location, Plugin plugin, FireworkEffect effect) {
        this.location = location;
        this.plugin = plugin;
        this.effect = effect;
    }

    public Firework explode() {
        return explode(2l);
    }

    public Firework explode(long delay) {
        Firework entity = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fireworkMeta = entity.getFireworkMeta();
        fireworkMeta.addEffect(effect);
        entity.setFireworkMeta(fireworkMeta);
        new BukkitRunnable() {
            @Override
            public void run() {
                entity.detonate();
            }
        }.runTaskLater(plugin, delay);
        return entity;
    }
}
