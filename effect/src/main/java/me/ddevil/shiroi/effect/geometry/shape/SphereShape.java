/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect.geometry.shape;

import me.ddevil.shiroi.effect.geometry.BaseGeometricShape;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by BRUNO II on 09/08/2016.
 */
public class SphereShape extends BaseGeometricShape {
    public SphereShape(Location center, double radius, double angleOffset, int totalPoints) {
        super(calculateCircle(center.toVector(), radius, angleOffset, totalPoints)
                .stream().map(vector -> vector.toLocation(center.getWorld())).collect(Collectors.toList()));
    }

    public static List<Vector> calculateCircle(Vector center, double radius, double angleOffset, int pointsPerCicle) {
        ArrayList<Vector> points = new ArrayList();
        double anglePerPoint = 360 / pointsPerCicle;
        for (int row = 0; row < pointsPerCicle; row++) {
            double y = 360 / row;
            for (int point = 0; point < pointsPerCicle; point++) {
                double angle = Math.toRadians(point * anglePerPoint + angleOffset);
                double x = Math.sin(angle) + center.getX();
                double z = Math.cos(angle) + center.getZ();
                points.add(new Vector(x, y, z));
            }
        }
        return points;
    }
}