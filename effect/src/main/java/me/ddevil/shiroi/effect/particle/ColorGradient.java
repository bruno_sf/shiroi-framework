package me.ddevil.shiroi.effect.particle;


import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Serializable;
import org.bukkit.Color;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ColorGradient implements Serializable {
    private final TreeMap<Double, Color> colorMap;

    public ColorGradient(Map<Double, Color> gradient) {
        colorMap = new TreeMap(gradient);
    }

    public ColorGradient(List<Color> colors) {
        this.colorMap = new TreeMap<>();
        double point = 0;
        double increment = (double) 1 / (colors.size() - 1);
        for (Color c : colors) {
            this.colorMap.put(point, c);
            point += increment;
        }
    }


    private static int lerp(float a, float b, double percent) {
        return (int) ((1 - percent) * a + percent * b);
    }

    public Color getColor(double point) {
        if (colorMap.containsKey(point)) {
            return colorMap.get(point);
        }
        Map.Entry<Double, Color> floor = colorMap.floorEntry(point);
        Map.Entry<Double, Color> ceiling = colorMap.ceilingEntry(point);
        //There is other design after that point
        if (floor == null) {
            return ceiling.getValue();
        }
        if (ceiling == null) {
            return floor.getValue();
        }
        if (point >= 1) {
            return floor.getValue();
        }
        if (point <= 0) {
            return ceiling.getValue();
        }
        double min = floor.getKey();
        double max = ceiling.getKey();

        Color a = floor.getValue();
        Color b = ceiling.getValue();
        double deltaPercentage = (max - min);
        double newpoint = point - min;
        double percentage = newpoint / deltaPercentage;
        return Color.fromRGB(
                lerp(a.getRed(), b.getRed(), percentage),
                lerp(a.getGreen(), b.getGreen(), percentage),
                lerp(a.getBlue(), b.getBlue(), percentage));
    }

    public List<ColorPoint> getPoints() {
        return colorMap.entrySet().stream().map(doubleColorEntry -> new ColorPoint(doubleColorEntry.getKey(), doubleColorEntry.getValue())).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put("colorMap", getPoints().stream().map(ColorPoint::serialize).collect(Collectors.toList())
                )
                .build();
    }

    public static class ColorPoint implements Serializable {
        public static final String POINT_IDENTIFIER = "point";
        public static final String COLOR_IDENTIFIER = "design";

        public static ColorPoint deserialize(Map<String, Object> map) {
            return new ColorPoint((Double) map.get(POINT_IDENTIFIER), Color.deserialize((Map<String, Object>) map.get(COLOR_IDENTIFIER)));
        }

        private final double point;
        private final Color color;

        public ColorPoint(double point, Color color) {
            this.point = point;
            this.color = color;
        }

        @NotNull
        @Override
        public Map<String, Object> serialize() {
            return new ImmutableMap.Builder<String, Object>()
                    .put(POINT_IDENTIFIER, point)
                    .put(COLOR_IDENTIFIER, color.serialize())
                    .build();
        }
    }
}
