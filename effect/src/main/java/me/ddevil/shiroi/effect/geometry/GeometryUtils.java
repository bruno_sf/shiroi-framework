/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect.geometry;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by BRUNO II on 16/06/2016.
 */
public class GeometryUtils {
    public static List<Location> getRandomSquare(Location center, double radius, GeometryResolution geometryResolution) {
        return getRandomSquare(center, radius, geometryResolution.getResolution());
    }

    public static List<Location> getRandomSquare(Location center, double radius, int totalPoints) {
        Random r = new Random();
        List<Location> points = new ArrayList();
        for (int i = 0; i < totalPoints; i++) {
            double x = r.nextDouble() * radius;
            if (r.nextBoolean()) {
                x *= -1;
            }
            double y = r.nextDouble() * radius;
            if (r.nextBoolean()) {
                y *= -1;
            }
            double z = r.nextDouble() * radius;
            if (r.nextBoolean()) {
                z *= -1;
            }
            points.add(center.clone().add(x, y, z));
        }
        return points;
    }

    public static List<Location> getLine(Location pos1, Location pos2, GeometryResolution resolution) {
        return getLine(pos1, pos2, resolution.getResolution());
    }

    public static List<Location> getLine(Location pos1, Location pos2, int totalPoints) {
        //Find out info
        World world = pos1.getWorld();
        Vector direction = pos2.clone().subtract(pos1).toVector();
        direction.normalize();
        double distance = pos1.distance(pos2);
        double ratio = distance / totalPoints;
        direction.multiply(ratio);

        //Find out points
        ArrayList<Location> points = new ArrayList();
        Location point = pos1.clone();
        for (int i = 0; i < totalPoints; i++) {
            point.add(direction);
            points.add(point.clone());
        }
        return points;
    }

    public static List<Vector> getLine(Vector pos1, Vector pos2, int totalPoints) {
        //Find out info
        Vector direction = pos2.clone().subtract(pos1);
        direction.normalize();
        double distance = pos1.distance(pos2);
        double ratio = distance / totalPoints;
        direction.multiply(ratio);

        //Find out points
        ArrayList<Vector> points = new ArrayList();
        Vector point = pos1.clone();
        for (int i = 0; i < totalPoints; i++) {
            point.add(direction);
            points.add(point.clone());
        }
        return points;
    }


    public static List<Location> getHelix(Location center, float radius, float maxY, int branches, int pointsPerBranch) {
        //Find out info
        World world = center.getWorld();
        double ratio = (2 * Math.PI) / pointsPerBranch;

        double currentY = center.getY();
        double yIncrease = maxY / currentY;
        ArrayList<Location> locations = new ArrayList<>();
        //Find out points
        for (int i = 0; i < branches; i++) {
            for (int p = 0; p < pointsPerBranch; p++) {
                double angle = p * ratio;
                double x = center.getX() + (radius * Math.cos(angle));
                double z = center.getZ() + (radius * Math.sin(angle));
                locations.add(new Location(world, x, center.getY(), z));
            }
        }
        return locations;
    }


}
