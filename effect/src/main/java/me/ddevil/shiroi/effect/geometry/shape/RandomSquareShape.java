package me.ddevil.shiroi.effect.geometry.shape;

import me.ddevil.shiroi.effect.geometry.BaseGeometricShape;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by ddevil on 04/09/2016.
 */
public class RandomSquareShape extends BaseGeometricShape {
    public RandomSquareShape(Location center, double radius, int totalPoints) {
        super(calculatePoints(center.toVector(), radius, totalPoints).stream().map(vector -> vector.toLocation(center.getWorld())).collect(Collectors.toList()));
    }

    private static List<Vector> calculatePoints(Vector center, double radius, int totalPoints) {
        Random r = new Random();
        ArrayList<Vector> arrayList = new ArrayList();
        for (int i = 0; i < totalPoints; i++) {
            Vector vector = new Vector(r.nextDouble() * radius, r.nextDouble() * radius, r.nextDouble() * radius);
            vector.add(center);
            arrayList.add(vector);
        }
        return arrayList;
    }
}
