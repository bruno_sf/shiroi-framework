/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect.particle;


import me.ddevil.shiroi.util.MinecraftVersion;

/**
 * Created by BRUNO II on 04/08/2016.
 */
public enum ParticleType {
    EXPLOSION_NORMAL("explode"),
    EXPLOSION_LARGE("largeexplode"),
    EXPLOSION_HUGE("hugeexplosion"),
    FIREWORKS_SPARK("fireworksSpark"),
    WATER_BUBBLE("bubble"),
    WATER_SPLASH("splash"),
    WATER_WAKE("wake"),
    SUSPENDED("suspended"),
    SUSPENDED_DEPTH("depthsuspend"),
    CRIT("crit"),
    CRIT_MAGIC("magicCrit"),
    SMOKE_NORMAL("smoke"),
    SMOKE_LARGE("largesmoke"),
    SPELL("spell"),
    SPELL_INSTANT("instantSpell"),
    SPELL_MOB("mobSpell", MinecraftVersion.V1_7, ParticleFeature.COLOR),
    SPELL_MOB_AMBIENT("mobSpellAmbient"),
    SPELL_WITCH("witchMagic"),
    DRIP_WATER("dripWater"),
    DRIP_LAVA("dripLava"),
    VILLAGER_ANGRY("angryVillager"),
    VILLAGER_HAPPY("happyVillager"),
    TOWN_AURA("townaura"),
    NOTE("note", MinecraftVersion.V1_7, ParticleFeature.COLOR),
    PORTAL("portal"),
    ENCHANTMENT_TABLE("enchantmenttable"),
    FLAME("flame"),
    LAVA("lava"),
    FOOTSTEP("footstep"),
    CLOUD("cloud"),
    REDSTONE("reddust", MinecraftVersion.V1_7, ParticleFeature.COLOR),
    SNOWBALL("snowballpoof"),
    SNOW_SHOVEL("snowshovel"),
    SLIME("slime"),
    HEART("heart"),
    BARRIER("barrier", MinecraftVersion.V1_8),
    ITEM_CRACK("iconcrack_", MinecraftVersion.V1_7, ParticleFeature.DATA),
    BLOCK_CRACK("blockcrack_", MinecraftVersion.V1_7, ParticleFeature.DATA),
    BLOCK_DUST("blockdust_", MinecraftVersion.V1_7, ParticleFeature.DATA),
    WATER_DROP("droplet", MinecraftVersion.V1_8),
    ITEM_TAKE("take", MinecraftVersion.V1_8),
    MOB_APPEARANCE("mobappearance", MinecraftVersion.V1_8),
    DRAGON_BREATH("dragonbreath", MinecraftVersion.V1_9),
    END_ROD("endRod", MinecraftVersion.V1_9),
    DAMAGE_INDICATOR("damageIndicator", MinecraftVersion.V1_9),
    SWEEP_ATTACK("sweepAttack", MinecraftVersion.V1_9),
    FALLING_DUST("fallingDust", MinecraftVersion.V1_10), ;


    ParticleType(String name, MinecraftVersion minimumVersion, ParticleFeature feature) {
        this.name = name;
        this.minimumVersion = minimumVersion;
        this.feature = feature;
    }

    ParticleType(String name, MinecraftVersion minimumVersion) {
        this(name, minimumVersion, null);
    }

    ParticleType(String name) {
        this(name, MinecraftVersion.V1_7);
    }

    private final String name;
    private final MinecraftVersion minimumVersion;
    private final ParticleFeature feature;

    public String getName() {
        return name;
    }

    public MinecraftVersion getMinimumVersion() {
        return minimumVersion;
    }

    public boolean hasFeature(ParticleFeature feature) {
    return this.feature == feature;
    }

    public ParticleFeature getFeature() {
        return feature;
    }
}
