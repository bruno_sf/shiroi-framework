/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect.particle;

import me.ddevil.shiroi.effect.utils.ReflectionUtils;
import me.ddevil.shiroi.util.MinecraftVersion;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by BRUNO II on 09/08/2016.
 */
public class ParticlePacket {
    private static final MinecraftVersion SERVER_VERSION = ReflectionUtils.getServerVersion();

    protected static final Constructor PACKET_CONSTRUCTOR = load();

    protected static Class PARTICLE_ENUM_CLASS;
    protected static Class PACKET_CLASS;
    protected static Class CRAFT_ENTITY_CLASS;
    protected static Class PLAYER_ENTITY_CLASS;
    protected static Class PLAYER_CONNECTION_CLASS;
    protected static Method GET_HANDLE_PLAYER;
    protected static Method SEND_PACKET;
    protected static Field CONNECTION_FIELD;
    private static boolean work;

    public ParticlePacket(Particle particle) {
        this.particle = particle;
    }

    protected static Constructor load() {
        Logger logger = Bukkit.getLogger();
        try {
            logger.info("Loading particle me.ddevil.shiroi.effect...");
            CRAFT_ENTITY_CLASS = ReflectionUtils.getOBCClass("entity.CraftEntity");
            logger.info("Loaded Craft Entity class: " + CRAFT_ENTITY_CLASS.getName());
            PLAYER_ENTITY_CLASS = ReflectionUtils.getNMSClass("EntityPlayer");
            logger.info("Loaded Player Entity class: " + PLAYER_ENTITY_CLASS.getName());
            PLAYER_CONNECTION_CLASS = ReflectionUtils.getNMSClass("PlayerConnection");
            logger.info("Loaded Player Connection class: " + PLAYER_CONNECTION_CLASS.getName());
            PARTICLE_ENUM_CLASS = ReflectionUtils.getNMSClass("EnumParticle");
            logger.info("Loaded Particle enum class: " + PARTICLE_ENUM_CLASS.getName());
            PACKET_CLASS = ReflectionUtils.getNMSClass("PacketPlayOutWorldParticles");
            GET_HANDLE_PLAYER = CRAFT_ENTITY_CLASS.getDeclaredMethod("getHandle");
            GET_HANDLE_PLAYER.setAccessible(true);
            logger.info("Loaded loadValue handle: " + GET_HANDLE_PLAYER.getName());
            PACKET_CLASS = ReflectionUtils.getNMSClass("PacketPlayOutWorldParticles");
            logger.info("Loaded packet class: " + PACKET_CLASS.getName());
            CONNECTION_FIELD = PLAYER_ENTITY_CLASS.getField("playerConnection");
            CONNECTION_FIELD.setAccessible(true);
            logger.info("Loaded connection field: " + CONNECTION_FIELD.getName());
            SEND_PACKET = PLAYER_CONNECTION_CLASS.getDeclaredMethod("sendPacket", ReflectionUtils.getNMSClass("Packet"));
            logger.info("Loaded sendPacket method: " + SEND_PACKET.getName());
            //Check for 1.8 particle packet
            Constructor cons;
            logger.info("Loading for server version " + SERVER_VERSION);
            if (SERVER_VERSION.newerThan(MinecraftVersion.V1_8)) {
                cons = PACKET_CLASS.getConstructor(new Class[]{
                        PARTICLE_ENUM_CLASS,
                        boolean.class,
                        float.class,//x
                        float.class,//y
                        float.class,//z
                        float.class,//ox
                        float.class,//oy
                        float.class,//oz
                        float.class,
                        int.class,//count
                        int[].class});
            } else {
                cons = PACKET_CLASS.getConstructor(new Class[]{
                        String.class,
                        float.class,
                        float.class,
                        float.class,
                        float.class,
                        float.class,
                        float.class,
                        float.class,
                        int.class});
            }
            if (cons != null) {
                logger.info("Loaded packet constructor: " + cons.getName());
            } else {
                logger.info("Couldn't find constructor!");
                work = false;
            }
            return cons;
        } catch (Exception e) {
            work = false;
        }
        return null;
    }

    private final Particle particle;

    static Enum<?> getEnum(String enumFullName) {
        String[] x = enumFullName.split("\\.(?=[^\\.]+$)");
        if (x.length == 2) {
            String enumClassName = x[0];
            String enumName = x[1];
            try {
                Class<Enum> cl = (Class<Enum>) Class.forName(enumClassName);
                return Enum.valueOf(cl, enumName);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private Object generatePacket(double x, double y, double z, double offsetX, double offsetY, double offsetZ, double speed, int count, int[] extra) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        String name = particle.getType().name();
        if (SERVER_VERSION.newerThan(MinecraftVersion.V1_8)) {
            return PACKET_CONSTRUCTOR.newInstance(
                    getEnum(PARTICLE_ENUM_CLASS.getName() + "." + name),
                    true,
                    (float) x,
                    (float) y,
                    (float) z,
                    (float) offsetX,
                    (float) offsetY,
                    (float) offsetZ,
                    (float) speed,
                    count,
                    extra);
        } else {//1.7
            return PACKET_CONSTRUCTOR.newInstance(
                    name,//Particle name
                    (float) x,//X
                    (float) y,//Y
                    (float) z, //Z
                    (float) offsetX,//X
                    (float) offsetY,//Y
                    (float) offsetZ,//Z
                    (float) speed,//Speed
                    count
            );
        }
    }

    private double getColor(double rgbValue) {
        if (rgbValue <= 0) {
            rgbValue = -1;
        }
        return rgbValue / 255;
    }

    protected static void sendPacket(Object packet, Player p) throws InvocationTargetException, IllegalAccessException {
        Object handle = GET_HANDLE_PLAYER.invoke(p, (Object) null);
        final Object connection = CONNECTION_FIELD.get(handle);
        SEND_PACKET.invoke(connection, packet);
    }

    public void send(double x, double y, double z, double speed, int count, List<Player> players) {
        ParticleType type = particle.getType();
        Object packet;
        double offsetX = 0, offsetY = 0, offsetZ = 0;
        int[] extra = null;
        try {
            if (type.hasFeature(ParticleFeature.COLOR)) {
                Color displayColor = particle.getDisplayColor();
                offsetX = getColor(displayColor.getRed());
                offsetY = getColor(displayColor.getBlue());
                offsetZ = getColor(displayColor.getGreen());
            }
            if (type.hasFeature(ParticleFeature.DATA)) {
                extra = new int[2];
            }
            packet = generatePacket(x, y, z, offsetX, offsetY, offsetZ, speed, count, extra);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return;
        }
        for (Player p : players) {
            try {
                sendPacket(packet, p);
            } catch (InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
                break;
            }
        }

    }
}
