/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.effect.particle;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;

public class Particle {

    private final ParticleType type;
    private Color displayColor;

    public Particle(ParticleType type) {
        this.type = type;
    }

    public Color getDisplayColor() {
        return displayColor;
    }

    public void setDisplayColor(Color displayColor) {
        this.displayColor = displayColor;
    }


    public void display(Location l) {
        display(l.getWorld(), l.getX(), l.getY(), l.getZ());
    }

    public void display(Location l, List<Player> players) {
        display(l.getX(), l.getY(), l.getZ(), players);
    }

    public void display(World world, double x, double y, double z) {
        display(x, y, z, world.getPlayers());
    }

    public void display(double x, double y, double z, List<Player> players) {
        display(x, y, z, 1d, 1, players);

    }

    public void display(double x, double y, double z, double speed, int count, List<Player> players) {
        new ParticlePacket(this).send(x, y, z, speed, count, players);
    }


    public void display(World world, Vector pos) {
        display(world, pos.getX(), pos.getY(), pos.getZ());
    }


    public ParticleType getType() {
        return type;
    }
}
