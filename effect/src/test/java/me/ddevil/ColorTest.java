package me.ddevil;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.effect.particle.ColorGradient;
import org.bukkit.Color;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bruno on 25/09/2016.
 */
public class ColorTest {
    @Test
    public void gradientMapping() {

        ColorGradient[] colorGradients = {
                new ColorGradient(
                        new ImmutableMap.Builder<Double, Color>()
                                .put(0.25, Color.AQUA)
                                .put(0.5, Color.BLACK)
                                .put(0.75, Color.RED)
                                .build()
                ),
                new ColorGradient(
                        new ImmutableMap.Builder<Double, Color>()
                                .put(0.0, Color.BLUE)
                                .put(0.20, Color.FUCHSIA)
                                .put(0.80, Color.ORANGE)
                                .build()
                ),
                new ColorGradient(
                        new ImmutableMap.Builder<Double, Color>()
                                .put(0.10, Color.RED)
                                .put(0.30, Color.BLACK)
                                .put(0.50, Color.TEAL)
                                .build()
                )
        };
        for (ColorGradient colorGradient : colorGradients) {
            System.out.println("Using gradient");
            System.out.println(new JSONObject(colorGradient.serialize()).toJSONString());
            List<ColorGradient.ColorPoint> points = new ArrayList();
            for (int i = 0; i < 100; i++) {
                double area = (double) i / 100;
                Color color = colorGradient.getColor(area);
                points.add(new ColorGradient.ColorPoint(area, color));
            }
            JSONObject json = new JSONObject();
            json.put("values", points.stream().map(ColorGradient.ColorPoint::serialize).collect(Collectors.toList()));
            System.out.println(json.toJSONString());
        }
    }
}
