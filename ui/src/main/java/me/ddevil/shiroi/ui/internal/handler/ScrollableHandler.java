package me.ddevil.shiroi.ui.internal.handler;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.jetbrains.annotations.NotNull;

/**
 * Created by bruno on 06/12/2016.
 */
public class ScrollableHandler<D extends Drawable> extends BaseHandler<Scrollable<D>, D> {

    public ScrollableHandler(Scrollable<D> container) {
        super(container);
    }

    @Override
    protected UIPosition getPosition(@NotNull Scrollable<D> container, @NotNull D object) {
        return UIPosition.fromSlot(container.getPosition(object), container.getWidth());
    }

    @Override
    protected D getDrawable(@NotNull Scrollable<D> container, @NotNull UIPosition position) {
        return container.getDrawable(position.toInvSlot(container.getWidth()));
    }

    @Override
    protected boolean hasObjectIn(@NotNull Scrollable<D> container, @NotNull UIPosition position) {
        return container.hasObjectInSlot(position.toInvSlot(container.getWidth()));
    }

}
