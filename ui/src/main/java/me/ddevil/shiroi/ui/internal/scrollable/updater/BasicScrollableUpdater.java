package me.ddevil.shiroi.ui.internal.scrollable.updater;

import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.misc.ScrollDirection;
import me.ddevil.shiroi.ui.api.updater.ScrollableUpdater;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;

/**
 * Created by bruno on 04/11/2016.
 */
public class BasicScrollableUpdater implements ScrollableUpdater<Scrollable<?>> {
    public static final String DEFAULT_NEXT = "Next";
    public static final String DEFAULT_PREVIOUS = "Previous";
    private final ItemStack icon;
    private final String next;
    private final String previous;
    private final ChatColor primary;
    private final ChatColor secondary;
    private final ChatColor neutral;


    public BasicScrollableUpdater(Material icon, ChatColor primary, ChatColor secondary, ChatColor neutral) {
        this(new ItemStack(icon), primary, secondary, neutral);
    }

    public BasicScrollableUpdater(ItemStack icon, ChatColor primary, ChatColor secondary, ChatColor neutral) {
        this(icon, DEFAULT_NEXT, DEFAULT_PREVIOUS, primary, secondary, neutral);
    }

    public BasicScrollableUpdater(Material icon, String next, String previous, ChatColor primary, ChatColor secondary, ChatColor neutral) {
        this(new ItemStack(icon), next, previous, primary, secondary, neutral);
    }

    public BasicScrollableUpdater(ItemStack icon, String next, String previous, ChatColor primary, ChatColor secondary, ChatColor neutral) {
        this.icon = icon;
        this.next = next;
        this.previous = previous;
        this.primary = primary;
        this.secondary = secondary;
        this.neutral = neutral;
    }

    @Override
    public ItemStack update(Scrollable<?> scrollable, ScrollDirection direction) {
        ItemStack ticon = new ItemStack(icon);
        ItemMeta itemMeta = ticon.getItemMeta();
        itemMeta.setDisplayName((primary + (direction == ScrollDirection.NEXT ? next : previous)));
        itemMeta.setLore(Collections.singletonList(primary.toString() + (scrollable.getCurrentIndex() + 1) + neutral + "/" + secondary + scrollable.getTotalPages()));
        ticon.setItemMeta(itemMeta);
        return ticon;
    }
}
