package me.ddevil.shiroi.ui.internal.container;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Created by bruno on 02/11/2016.
 */
public class ArrayContainer<H extends Drawable> extends BaseContainer<H> {
    private final Drawable[][] objectMap;

    public ArrayContainer(int width, int height, Map<UIPosition, H> components) {
        super(width, height, components);
        this.objectMap = new Drawable[width][height];
    }

    public ArrayContainer(int width, int height) {
        this(width, height, Collections.emptyMap());
    }

    @Override
    protected void remove0(UIPosition position) {
        remove0(position.getX(), position.getY());
    }

    @Override
    protected void remove0(int x, int y) {
        objectMap[x][y] = null;
    }


    @Override
    public boolean hasObjectIn(UIPosition position) {
        return objectMap[position.getX()][position.getY()] != null;
    }

    @Override
    public H getDrawable(UIPosition position) {
        return getDrawable(position.getX(), position.getY());
    }

    @Override
    public H getDrawable(int x, int y) {
        return (H) objectMap[x][y];
    }

    @NotNull
    @Override
    public SortedMap<UIPosition, H> getMenuMap() {
        SortedMap<UIPosition, H> object = new TreeMap<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (hasObjectIn(x, y)) {
                    H Drawable = getDrawable(x, y);
                    object.put(new UIPosition(x, y), Drawable);
                }

            }
        }
        return object;
    }

    @Override
    public UIPosition getPosition(@NotNull H Drawable) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                H h = getDrawable(x, y);
                if (h != null && h == Drawable) {
                    return new UIPosition(x, y);
                }

            }
        }
        return null;
    }

    @Override
    public boolean hasObjectIn(int x, int y) {
        return objectMap[x][y] != null;
    }

    @Override
    protected void place0(H Drawable, UIPosition position) {
        objectMap[position.getX()][position.getY()] = Drawable;
    }


    @Override
    public void add(@NotNull H Drawable) {
        for (int i = 0; i < getSize(); i++) {
            UIPosition place = UIPosition.fromSlot(i, width);
            if (canPlaceIn(place)) {
                place(Drawable, place);
                return;
            }
        }
    }

    @Override
    public void clear() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                objectMap[x][y] = null;
            }
        }
    }

    @Override
    public boolean isOwnerOf(@NotNull H obj) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                H Drawable = getDrawable(x, y);
                if (Drawable != null) {
                    if (Drawable.equals(obj)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @NotNull
    @Override
    public List<H> getAllDrawables() {
        List<H> l = new ArrayList<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                H Drawable = getDrawable(x, y);
                if (Drawable != null) {
                    l.add(Drawable);
                }
            }
        }
        return l;
    }

    @Override
    public boolean contains(int x, int y) {
        return false;
    }
}
