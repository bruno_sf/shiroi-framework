package me.ddevil.shiroi.ui.internal.container;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.ui.api.*;
import me.ddevil.shiroi.ui.api.holder.Container;
import me.ddevil.shiroi.ui.exception.PositionOutOfBoundsException;
import me.ddevil.shiroi.ui.internal.area.BaseAreaObject;
import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.internal.handler.ContainerHandler;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.BiConsumer;

public abstract class BaseContainer<H extends Drawable> extends BaseAreaObject implements Container<H> {

    private ItemStack background;
    protected final Action handler;


    public BaseContainer(int width, int height) {
        this(width, height, Collections.emptyMap());
    }

    public BaseContainer(int width, int height, Map<UIPosition, H> components) {
        super(width, height);
        this.handler = new ContainerHandler<>(this);
        if (!components.isEmpty()) {
            components.forEach((position, h) -> place(h, position));
        }
    }

    @Override
    public void remove(@NotNull UIPosition position) {
        if (contains(position)) {
            if (hasObjectIn(position)) {
                remove0(position);
            }
        }
    }

    @Override
    public final void remove(@NotNull H drawable) {
        remove(getPosition(drawable));
    }

    protected abstract void remove0(UIPosition position);

    @Override
    public void remove(int x, int y) {
        if (contains(x, y)) {
            if (hasObjectIn(x, y)) {
                remove0(x, y);
            }
        }
    }

    protected abstract void remove0(int x, int y);

    @Override
    public boolean canPlaceIn(@NotNull UIPosition position) {
        return !hasObjectIn(position);
    }

    @Override
    public void update() {
        getAllDrawables().forEach(Component::update);
    }

    @Override
    public Action getAction() {
        return handler;
    }

    @Override
    public void setAction(Action action) {
        throw new IllegalAccessError("You can't modify a container's handler!");
    }

    @Override
    public boolean canPlaceIn(int x, int y) {
        return !hasObjectIn(x, y);
    }

    @Override
    public void place(@NotNull H drawable, @NotNull UIPosition position) {
        if (canPlaceIn(position)) {
            place0(drawable, position);
            if (drawable instanceof AreaObject) {
                addAreaObject((AreaObject) drawable, position);
            }
        } else {
            throw new PositionOutOfBoundsException(this, position);
        }
    }

    private void addAreaObject(AreaObject drawable, UIPosition position) {
        drawable.getArea().forEach(o -> {
            UIPosition pos = position.add(o);
            if (hasObjectIn(pos)) {
                if (getDrawable(pos) != drawable) {
                    throw new IllegalArgumentException("There is already a different object in position " + pos + "!");
                }
            }
            place0((H) drawable, pos);
        });
    }

    protected abstract void place0(H drawable, UIPosition position);

    @Override
    public void add(@NotNull H drawable) {
        for (int i = 0; i < getSize(); i++) {
            UIPosition pos = UIPosition.fromSlot(i, width);
            if (canPlaceIn(pos)) {
                place(drawable, pos);
                return;
            }
        }
        throw new IllegalStateException("There are no slots available!");
    }

    @Override
    public boolean isOwnerOf(@NotNull H obj) {
        return getMenuMap().containsValue(obj);
    }


    @Override
    public void setBackground(ItemStack background) {
        this.background = background;
    }

    @Override
    public ItemStack getBackground() {
        return background;
    }

    @Override
    public boolean hasBackground() {
        return background != null;
    }

    @Override
    public Map<UIPosition, ItemStack> drawUI() {
        Map<UIPosition, H> objMap = getMenuMap();
        ImmutableMap.Builder<UIPosition, ItemStack> builder = new ImmutableMap.Builder<>();
        Set<Component> addedObjects = new HashSet<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                UIPosition currentPos = new UIPosition(x, y);
                H uiObject = objMap.get(currentPos);
                if (uiObject != null) {
                    if (!addedObjects.contains(uiObject)) {
                        addedObjects.add(uiObject);
                        Map<UIPosition, ItemStack> childUI = uiObject.drawUI();
                        childUI.forEach((uiPosition, itemStack) ->
                                builder.put(currentPos.add(uiPosition), itemStack));
                    } else {
                    }
                } else if (hasBackground()) {
                    builder.put(currentPos, getBackground());
                }
            }
        }
        return builder.build();
    }


}
