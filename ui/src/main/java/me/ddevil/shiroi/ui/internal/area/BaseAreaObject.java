package me.ddevil.shiroi.ui.internal.area;

import com.google.common.collect.ImmutableSortedSet;
import me.ddevil.shiroi.ui.api.AreaObject;
import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.misc.UIPosition;

import java.util.SortedSet;

/**
 * Created by bruno on 05/10/2016.
 */
public abstract class BaseAreaObject implements AreaObject {

    protected int width;
    protected int height;

    public BaseAreaObject(int width, int height) {
        redefineArea(width, height);
    }

    @Override
    public void redefineArea(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public SortedSet<UIPosition> getArea() {

        int maxX = getMaxX();
        int maxY = getMaxY();
        ImmutableSortedSet.Builder<UIPosition> menuPositionBuilder = new ImmutableSortedSet.Builder<>(UIPosition::compareTo);
        for (int x = 0; x <= maxX; x++) {
            for (int y = 0; y <= maxY; y++) {
                menuPositionBuilder.add(new UIPosition(x, y));
            }
        }
        return menuPositionBuilder.build();
    }

    @Override
    public boolean contains(UIPosition position) {
        return contains(position.getX(), position.getY());
    }

    @Override
    public boolean contains(int x, int y) {
        return x < width && y < height;
    }

    @Override
    public int getSize() {
        return width * height;
    }

    @Override
    public int getMaxX() {
        return width - 1;
    }

    @Override
    public int getMaxY() {
        return height - 1;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
}
