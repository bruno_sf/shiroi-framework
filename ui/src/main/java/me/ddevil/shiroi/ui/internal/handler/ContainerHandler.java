package me.ddevil.shiroi.ui.internal.handler;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Container;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.jetbrains.annotations.NotNull;

/**
 * Created by bruno on 06/12/2016.
 */
public class ContainerHandler<D extends Drawable> extends BaseHandler<Container<D>, D> {
    public ContainerHandler(Container<D> container) {
        super(container);
    }

    @Override
    protected UIPosition getPosition(@NotNull Container<D> container, @NotNull D object) {
        return container.getPosition(object);
    }

    @Override
    protected D getDrawable(@NotNull Container<D> container, @NotNull UIPosition position) {
        return container.getDrawable(position);
    }

    @Override
    protected boolean hasObjectIn(@NotNull Container<D> container, @NotNull UIPosition position) {
        return container.hasObjectIn(position);
    }
}
