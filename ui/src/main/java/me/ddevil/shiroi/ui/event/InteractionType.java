package me.ddevil.shiroi.ui.event;

/**
 * Created by bruno on 26/10/2016.
 */
public enum InteractionType {

    INVENTORY_CLICK_RIGHT,
    INVENTORY_CLICK_LEFT,
    INTERACT_CLICK_RIGHT,
    INTERACT_CLICK_LEFT
}
