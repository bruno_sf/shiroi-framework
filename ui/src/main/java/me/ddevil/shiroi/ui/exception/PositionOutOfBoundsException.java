package me.ddevil.shiroi.ui.exception;

import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.misc.UIPosition;

/**
 * Created by bruno on 18/10/2016.
 */
public class PositionOutOfBoundsException extends RuntimeException {
    private final Holder<?> menu;
    private final UIPosition position;

    public PositionOutOfBoundsException(Holder<?> menu, UIPosition position) {
        this(menu, position, false);
    }

    public PositionOutOfBoundsException(Holder<?> menu, UIPosition position, boolean child) {
        super((child ? "Child " : "") + "Position " + position + " is out of bounds of container " + menu + "!");
        this.menu = menu;
        this.position = position;
    }

    public Holder<?> getMenu() {
        return menu;
    }

    public UIPosition getPosition() {
        return position;
    }
}
