/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.shiroi.ui.internal.misc.base;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.api.Item;
import me.ddevil.shiroi.ui.api.updater.Updater;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.inventory.ItemStack;

import java.util.Map;


/**
 * @author HP
 */
public abstract class BasicItem<U extends Updater> implements Item<U> {

    private ItemStack icon;
    private U updater;

    public BasicItem(U updater) {
        this.updater = updater;
    }

    public BasicItem(ItemStack icon) {
        this.icon = icon;
    }

    @Override
    public final U getUpdater() {
        return updater;
    }

    @Override
    public final void setUpdater(U updater) {
        this.updater = updater;
    }

    @Override
    public final ItemStack getIcon() {
        update();
        return icon;
    }

    @Override
    public final boolean hasIcon() {
        return icon != null;
    }

    @Override
    public final void update() {
        onUpdate();
        if (updater != null) {
            this.icon = updateIcon(updater);
        }
    }

    private void onUpdate() {
    }

    protected abstract ItemStack updateIcon(U updater);


    @Override
    public final Map<UIPosition, ItemStack> drawUI() {
        return new ImmutableMap.Builder<UIPosition, ItemStack>()
                .put(UIPosition.ZERO, icon)
                .build();
    }
}
