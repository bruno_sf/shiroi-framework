package me.ddevil.shiroi.ui.internal.handler;

import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.api.AreaObject;
import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.event.UIActionEvent;
import me.ddevil.shiroi.ui.event.UIClickEvent;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.jetbrains.annotations.NotNull;

public abstract class BaseHandler<H extends Holder<D>, D extends Drawable> implements Action {
    @NotNull
    private final H container;

    public BaseHandler(@NotNull H container) {
        this.container = container;
    }

    @Override
    public void onInteract(@NotNull UIClickEvent e, @NotNull UIPosition localPosition) {
        if (hasObjectIn(container, localPosition)) {
            D drawable = getDrawable(container, localPosition);
            if (drawable instanceof Clickable) {
                Clickable c = (Clickable) drawable;
                UIPosition objLocalPos;
                if (drawable instanceof AreaObject) {
                    objLocalPos = localPosition.subtract(getPosition(container, drawable));
                } else {
                    objLocalPos = UIPosition.ZERO;
                }
                Action action = c.getAction();
                if (action != null) {
                    action.onInteract(e, objLocalPos);
                    new UIActionEvent(c, e.getClickedSlot(), e.getPlayer(), e.getClickType(), e.getPlacedBlock()).call();
                }
            }
        }
    }

    protected abstract UIPosition getPosition(@NotNull H container, @NotNull D object);

    protected abstract D getDrawable(@NotNull H container, @NotNull UIPosition position);

    protected abstract boolean hasObjectIn(@NotNull H container, @NotNull UIPosition position);
}
