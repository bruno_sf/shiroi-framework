package me.ddevil.shiroi.ui.misc;

import me.ddevil.shiroi.util.vector.Vector2;
import me.ddevil.shiroi.ui.UIConstants;

/**
 * Created by bruno on 10/10/2016.
 */
public class UIPosition extends Vector2<Integer> implements Comparable<UIPosition> {
    public static final UIPosition ZERO = new UIPosition(0, 0);

    public UIPosition(int x, int y) {
        super(x, y);
    }


    @Override
    public int compareTo(UIPosition o) {
        int me = toInvSlot();
        int other = o.toInvSlot();
        if (other == me) {
            return 0;
        } else if (me > other) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof UIPosition)) return false;
        UIPosition vector2 = (UIPosition) o;
        return x.equals(vector2.getX()) && y.equals(vector2.getY());
    }

    public UIPosition add(UIPosition key) {
        return new UIPosition(x + key.getX(), y + key.getY());
    }

    public UIPosition subtract(UIPosition key) {
        return new UIPosition(x - key.getX(), y - key.getY());
    }

    @Override
    public String toString() {
        return "UIPosition{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static UIPosition fromSlot(int slot, int parentWidth) {
        return new UIPosition(slot % parentWidth, slot / parentWidth);
    }

    public int toInvSlot(int width) {
        return y * width + x;
    }

    public int toInvSlot() {
        return toInvSlot(UIConstants.INVENTORY_SIZE_X);
    }

}
