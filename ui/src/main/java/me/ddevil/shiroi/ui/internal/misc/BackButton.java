package me.ddevil.shiroi.ui.internal.misc;

import me.ddevil.shiroi.ui.api.Menu;
import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.inventory.ItemStack;

public class BackButton extends ClickableItem {

    private final Menu<?> returningMenu;

    public BackButton(ItemStack itemStack, Menu<?> returningMenu) {
        super(itemStack, (e, localPosition) -> returningMenu.open(e.getPlayer()));
        this.returningMenu = returningMenu;
    }

    public BackButton(ItemUpdater updater, Menu<?> returningMenu) {
        super(updater, (e, localPosition) -> returningMenu.open(e.getPlayer()));
        this.returningMenu = returningMenu;
    }

    public Menu<?> getReturningMenu() {
        return returningMenu;
    }

}
