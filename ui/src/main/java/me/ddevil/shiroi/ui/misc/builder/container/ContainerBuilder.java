package me.ddevil.shiroi.ui.misc.builder.container;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Container;
import me.ddevil.shiroi.ui.internal.container.ArrayContainer;

/**
 * Created by bruno on 10/12/2016.
 */
public class ContainerBuilder<D extends Drawable> extends AbstractContainerBuilder<D, Container<D>> {
    public ContainerBuilder(int width, int height) {
        super(width, height);
    }

    @Override
    public Container<D> build() {
        return new ArrayContainer<>(width, height, components);
    }
}
