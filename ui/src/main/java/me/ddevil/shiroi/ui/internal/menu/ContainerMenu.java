package me.ddevil.shiroi.ui.internal.menu;

import me.ddevil.shiroi.ui.UIConstants;
import me.ddevil.shiroi.ui.api.AreaObject;
import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.Menu;
import me.ddevil.shiroi.ui.event.UIActionEvent;
import me.ddevil.shiroi.ui.event.UIClickEvent;
import me.ddevil.shiroi.ui.internal.container.MapContainer;
import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Map;

/**
 * Created by bruno on 27/10/2016.
 */
public abstract class ContainerMenu<PL extends JavaPlugin> extends MapContainer<Drawable> implements Menu<PL> {
    protected final PL plugin;
    protected final Inventory mainInventory;

    public ContainerMenu(PL plugin, String name, Menu.Size rows) {
        this(plugin, name, rows, Collections.emptyMap());
    }

    public ContainerMenu(PL plugin, String name, Menu.Size rows, Map<UIPosition, Drawable> components) {
        this(plugin, Bukkit.createInventory(null, rows.getRows() * 9, name), components);
    }

    public ContainerMenu(PL plugin, Inventory bukkitInventory) {
        this(plugin, bukkitInventory, Collections.emptyMap());
    }

    public ContainerMenu(PL plugin, Inventory bukkitInventory, Map<UIPosition, Drawable> components) {
        super(UIConstants.INVENTORY_SIZE_X, bukkitInventory.getSize() / 9, components);
        this.plugin = plugin;
        this.mainInventory = bukkitInventory;
    }


    private boolean enabled = false;

    @Override
    public final void setup() {
        this.enabled = true;
        setup0();
    }

    protected abstract void setup0();


    @Override
    public final void disable() {
        this.enabled = false;
        disable0();
    }

    protected abstract void disable0();


    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void update() {
        super.update();
        drawUI().forEach((uiPosition, itemStack) -> mainInventory.setItem(uiPosition.toInvSlot(width), itemStack));
        update0();
    }

    protected abstract void update0();

    @Override
    public Inventory getBukkitInventory() {
        return mainInventory;
    }

    @Override
    public PL getPlugin() {
        return plugin;
    }
}
