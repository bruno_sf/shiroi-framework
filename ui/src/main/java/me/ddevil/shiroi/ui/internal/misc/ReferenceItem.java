package me.ddevil.shiroi.ui.internal.misc;

import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import me.ddevil.shiroi.ui.api.Action;
import org.bukkit.inventory.ItemStack;

public class ReferenceItem<T> extends ClickableItem {
    private T value;

    public ReferenceItem(ItemStack itemStack, Action action, T value) {
        super(itemStack, action);
        this.value = value;
    }

    public ReferenceItem(ItemUpdater updater, Action action, T value) {
        super(updater, action);
        this.value = value;
    }

    public ReferenceItem(ItemStack itemStack, T value) {
        super(itemStack);
        this.value = value;
    }

    public ReferenceItem(ItemUpdater updater, T value) {
        super(updater);
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
