package me.ddevil.shiroi.ui.internal.container;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.misc.UIPosition;
import me.ddevil.shiroi.util.MiscUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by bruno on 02/11/2016.
 */
public class MapContainer<H extends Drawable> extends BaseContainer<H> {

    public MapContainer(int width, int height) {
        this(width, height, Collections.emptyMap());
    }

    public MapContainer(int width, int height, Map<UIPosition, H> components) {
        super(width, height, components);
    }

    @Override
    protected void remove0(UIPosition position) {
        objectMap.remove(position);
    }

    @Override
    protected void remove0(int x, int y) {
        remove0(new UIPosition(x, y));
    }

    protected final Map<UIPosition, H> objectMap = new HashMap<>();

    @NotNull
    @Override
    public List<H> getAllDrawables() {
        return new ArrayList<>(objectMap.values());
    }

    @NotNull
    @Override
    public SortedMap<UIPosition, H> getMenuMap() {
        return new TreeMap<>(objectMap);
    }

    @Override
    public UIPosition getPosition(@NotNull H drawable) {
        if (objectMap.containsValue(drawable)) {
            return MiscUtils.min(objectMap.keySet().stream().filter(uiPosition -> objectMap.get(uiPosition) == drawable).collect(Collectors.toSet()));
        }
        return null;
    }

    @Override
    protected void place0(H drawable, UIPosition position) {
        objectMap.put(position, drawable);
    }

    @Override
    public boolean hasObjectIn(int x, int y) {
        return hasObjectIn(new UIPosition(x, y));
    }

    @Override
    public H getDrawable(int x, int y) {
        return getDrawable(new UIPosition(x, y));
    }

    @Override
    public void clear() {
        objectMap.clear();
    }


    @Override
    public boolean hasObjectIn(UIPosition position) {
        return objectMap.containsKey(position);
    }

    @Nullable
    @Override
    public H getDrawable(UIPosition position) {
        return objectMap.get(position);
    }
}
