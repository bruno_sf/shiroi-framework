package me.ddevil.shiroi.ui.internal.misc;

import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import me.ddevil.shiroi.ui.internal.misc.base.BasicClickableItem;
import me.ddevil.shiroi.ui.api.Action;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 04/11/2016.
 */
public abstract class ClickableItem extends BasicClickableItem<ItemUpdater> {
    public ClickableItem(ItemStack itemStack, Action action) {
        super(itemStack, action);
    }

    public ClickableItem(ItemUpdater updater, Action action) {
        super(updater, action);
    }

    public ClickableItem(ItemStack itemStack) {
        super(itemStack);
    }

    public ClickableItem(ItemUpdater updater) {
        super(updater);
    }

    @Override
    protected ItemStack updateIcon(ItemUpdater updater) {
        return updater.update();
    }


}
