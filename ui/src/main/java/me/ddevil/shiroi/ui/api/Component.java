package me.ddevil.shiroi.ui.api;

/**
 * Represents an UI element.
 * This element has a purpose somewhere in a menu, or maybe even the menu itself
 */
public interface Component {
    /**
     * Called every time something changes on the object's menu.
     * Ex: When the menu is opened, when another object interacts with something.
     */
    void update();
}
