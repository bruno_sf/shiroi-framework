package me.ddevil.shiroi.ui;

public class UIConstants {
    public static final int INVENTORY_SIZE_Y = 6;
    public static final int INVENTORY_SIZE_X = 9;
    public static final int FINAL_INVENTORY_SLOT = INVENTORY_SIZE_X * INVENTORY_SIZE_Y - 1;

}
