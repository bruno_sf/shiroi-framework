package me.ddevil.shiroi.ui.api;

/**
 * Represents an object that performs an handler when clicked;
 */
public interface Clickable extends Component {
    /**
     * This function is mainly used by menus to handle clicks.
     * I don't really see why you would really use it in another
     * case and don't really recommend it, but if you want to, go ahead.
     * As longs as you don't bother me with errors about it is fine :D
     *
     * @return The handler to be performed when clicked
     */
    Action getAction();

    /**
     * Sets the handler to be performed when clicked.
     *
     * @param action The handler to be set.
     */
    void setAction(Action action);

}
