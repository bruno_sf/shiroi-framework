package me.ddevil.shiroi.ui.api;

import me.ddevil.shiroi.ui.api.updater.Updater;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 05/10/2016.
 */
public interface Item<U extends Updater> extends Drawable {

    void setUpdater(U itemUpdater);

    U getUpdater();

    default boolean hasItemUpdater() {
        return getUpdater() != null;
    }

    ItemStack getIcon();

    default boolean hasIcon() {
        return getIcon() != null;
    }

}
