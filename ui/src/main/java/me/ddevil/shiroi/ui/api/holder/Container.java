package me.ddevil.shiroi.ui.api.holder;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.SortedMap;

/**
 * Created by bruno on 25/10/2016.
 */
public interface Container<H extends Drawable> extends Holder<H> {

    void place(@NotNull H drawable, @NotNull UIPosition position);

    boolean canPlaceIn(@NotNull UIPosition position);

    boolean canPlaceIn(int x, int y);

    void remove(@NotNull UIPosition position);

    void remove(int x, int y);

    /**
     * Check if the given slot is
     *
     * @param position
     * @return
     */
    boolean hasObjectIn(UIPosition position);

    boolean hasObjectIn(int x, int y);

    @Nullable H getDrawable(UIPosition position);

    @Nullable H getDrawable(int x, int y);

    /**
     * @return
     */
    @NotNull SortedMap<UIPosition, H> getMenuMap();

    UIPosition getPosition(@NotNull H drawable);

}
