package me.ddevil.shiroi.ui.internal.menu;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.event.InteractionType;
import me.ddevil.shiroi.ui.event.UIActionEvent;
import me.ddevil.shiroi.ui.event.UIClickEvent;
import me.ddevil.shiroi.ui.UIConstants;
import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;

/**
 * Created by bruno on 06/10/2016.
 */
public abstract class AutonomousMenu<P extends JavaPlugin> extends ContainerMenu<P> implements Listener {
    public AutonomousMenu(P plugin, String name, Size rows) {
        super(plugin, name, rows);
    }

    public AutonomousMenu(P plugin, String name, Size rows, Map<UIPosition, Drawable> components) {
        super(plugin, name, rows, components);
    }

    public AutonomousMenu(P plugin, Inventory bukkitInventory) {
        super(plugin, bukkitInventory);
    }

    public AutonomousMenu(P plugin, Inventory bukkitInventory, Map<UIPosition, Drawable> components) {
        super(plugin, bukkitInventory, components);
    }

    @Override
    protected final void setup0() {
        Bukkit.getPluginManager().registerEvents(this, plugin);
        setup1();
    }

    protected abstract void setup1();

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (!isEnabled()) {
            return;
        }
        Inventory clickedInventory = e.getInventory();
        if ((clickedInventory != null) && (this.mainInventory != null) && (clickedInventory.equals(this.mainInventory))) {
            if (e.getRawSlot() < clickedInventory.getSize()) {
                e.setCancelled(true);
                int slot = e.getSlot();
                UIPosition position = UIPosition.fromSlot(slot, UIConstants.INVENTORY_SIZE_X);
                if (this.objectMap.containsKey(position)) {
                    Drawable drawable = getDrawable(position);
                    if (drawable instanceof Clickable) {
                        InteractionType type;
                        if (e.isLeftClick())
                            type = InteractionType.INVENTORY_CLICK_LEFT;
                        else {
                            type = InteractionType.INVENTORY_CLICK_RIGHT;
                        }

                        UIClickEvent call = new UIClickEvent(
                                ((Clickable) drawable),
                                position,
                                (Player) e.getWhoClicked(),
                                type,
                                null,
                                e.getCursor()
                        ).call();
                        handler.onInteract(call, position);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onAction(UIActionEvent e) {
        update();
    }

    @Override
    public void disable0() {
        HandlerList.unregisterAll(this);
    }
}
