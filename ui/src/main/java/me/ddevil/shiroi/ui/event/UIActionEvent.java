package me.ddevil.shiroi.ui.event;

import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.api.Component;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by bruno on 26/10/2016.
 */
public class UIActionEvent extends Event {

    private final Clickable object;
    private final Player player;
    private final UIPosition clickedSlot;
    private final me.ddevil.shiroi.ui.event.InteractionType clickType;
    private final Block placedBlock;

    public UIActionEvent(Clickable object, UIPosition slot, Player clicker, me.ddevil.shiroi.ui.event.InteractionType click, Block block) {
        this.object = object;
        this.player = clicker;
        this.clickedSlot = slot;
        this.clickType = click;
        this.placedBlock = block;
    }

    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public final UIActionEvent call() {
        Bukkit.getPluginManager().callEvent(this);
        return this;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Component getObject() {
        return object;
    }

    public Player getPlayer() {
        return player;
    }

    public UIPosition getClickedSlot() {
        return clickedSlot;
    }

    public me.ddevil.shiroi.ui.event.InteractionType getClickType() {
        return clickType;
    }

    public Block getPlacedBlock() {
        return placedBlock;
    }

}
