package me.ddevil.shiroi.ui.api.updater;

import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.misc.ScrollDirection;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 05/10/2016.
 */
public interface ScrollableUpdater<O extends Scrollable<?>> extends Updater {
    ItemStack update(O scrollable, ScrollDirection direction);
}
