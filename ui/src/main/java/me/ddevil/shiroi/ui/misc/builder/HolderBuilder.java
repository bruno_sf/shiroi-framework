package me.ddevil.shiroi.ui.misc.builder;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Holder;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 10/12/2016.
 */
public abstract class HolderBuilder<D extends Drawable, H extends Holder<D>> {
    protected ItemStack background;
    protected int width;
    protected int height;


    public HolderBuilder(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public HolderBuilder withBackground(ItemStack background) {
        this.background = background;
        return this;
    }

    public abstract H build();
}