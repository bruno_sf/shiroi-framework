package me.ddevil.shiroi.ui.internal.misc;

import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import org.bukkit.inventory.ItemStack;

public class CloseButton extends ClickableItem {

    public CloseButton(ItemStack itemStack) {
        super(itemStack, (e, localPosition) -> e.getPlayer().closeInventory());
    }

    public CloseButton(ItemUpdater updater) {
        super(updater, (e, localPosition) -> e.getPlayer().closeInventory());
    }
}
