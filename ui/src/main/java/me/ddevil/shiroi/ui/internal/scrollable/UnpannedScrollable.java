package me.ddevil.shiroi.ui.internal.scrollable;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.Component;
import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.event.UIActionEvent;
import me.ddevil.shiroi.ui.event.UIClickEvent;
import me.ddevil.shiroi.ui.internal.area.BaseAreaObject;
import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.misc.ScrollDirection;
import me.ddevil.shiroi.ui.misc.UIPosition;
import me.ddevil.shiroi.util.MiscUtils;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;


public class UnpannedScrollable<H extends Drawable> extends BaseAreaObject implements Scrollable<H> {

    private int currentIndex = 0;
    protected final SortedMap<Integer, H> positionMap;
    protected final SortedMap<Integer, H> currentPage;
    private ItemStack background;
    private Action listener;

    public UnpannedScrollable(int width, int height) {
        super(width, height);
        if (height <= 0 || width <= 0) {
            throw new IllegalArgumentException("The height and width must be larger than 0!");
        }
        this.positionMap = new TreeMap<>();
        this.currentPage = new TreeMap<>();
        this.listener = new ScrollableClickHandler();
    }

    @Override
    public void update() {
        currentPage.values().forEach(Component::update);
    }

    private class ScrollableClickHandler implements Action {

        @Override
        public void onInteract(@NotNull UIClickEvent e, @NotNull UIPosition localPosition) {
            int clickedSlot = e.getClickedSlot().toInvSlot();
            if (currentPage.containsKey(clickedSlot)) {
                Component obj = currentPage.get(clickedSlot);
                if (obj instanceof Clickable) {
                    Clickable c = (Clickable) obj;
                    c.getAction().onInteract(e, UIPosition.ZERO);
                    new UIActionEvent(c, e.getClickedSlot(), e.getPlayer(), e.getClickType(), e.getPlacedBlock()).call();
                }
            }
        }
    }


    @Override
    public void add(@NotNull H drawable) {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (canPlaceIn(i)) {
                place(drawable, i);
                break;
            }
        }
    }

    @Override
    public void clear() {
        this.positionMap.clear();
    }

    @Override
    public boolean isOwnerOf(@NotNull H obj) {
        return positionMap.containsValue(obj);
    }

    @NotNull
    @Override
    public Map<Integer, H> getMenuMap() {
        return new TreeMap<>(currentPage);
    }

    @Override
    public boolean hasObjectInSlot(int position) {
        return currentPage.containsKey(position);
    }

    @Override
    public H getDrawable(int position) {
        return currentPage.get(position);
    }

    @NotNull
    @Override
    public List<H> getAllDrawables() {
        return new ArrayList<H>(positionMap.values());
    }


    @Override
    public void place(@NotNull H drawable, int position) {
        if (canPlaceIn(position)) {
            synchronized (positionMap) {
                positionMap.put(position, drawable);
            }
            recalculateCurrentPage();
        } else {
            throw new IllegalStateException("There is already an object in position " + position + "!");
        }
    }

    @Override
    public void remove(@NotNull H drawable) {
        if (isOwnerOf(drawable)) {
            positionMap.remove(getPosition(drawable));
        }
    }

    @Override
    public void remove(int position) {
        if (hasObjectInSlot(position)) {
            positionMap.remove(position);
        }
    }

    protected void recalculateCurrentPage() {
        currentPage.clear();
        int pageSize = getPageSize();
        int start = currentIndex * pageSize;
        int end = start + pageSize - 1;
        int pos = 0;
        for (int i = start; i <= end; i++) {
            H get = positionMap.get(i);
            if (get != null) {
                currentPage.put(pos, get);
                get.update();
            }
            pos++;
        }
    }

    @Override
    public boolean canPlaceIn(int position) {
        return !positionMap.containsKey(position);
    }

    @Override
    public int getCurrentIndex() {
        return currentIndex;
    }

    @Override
    public int getTotalPages() {
        int lastSlot = getLastSlot();
        if (lastSlot == 0) {
            return 1;
        }
        return lastSlot / getPageSize() + 1;
    }

    @Override
    public int getLastSlot() {
        if (positionMap.isEmpty()) {
            return 0;
        }
        return positionMap.lastKey();
    }

    @Override
    public int getPageSize() {
        return width * height;
    }

    @Override
    public void scroll(@NotNull ScrollDirection direction) {
        int page = currentIndex + direction.getScrollQuantity();
        if (page >= getTotalPages() || page < 0) {
            return;
        }
        currentIndex += direction.getScrollQuantity();
        recalculateCurrentPage();
    }

    @Override
    public void goToPage(int page) {
        if (page >= getTotalPages()) {
            page = getTotalPages() - 1;
        } else if (page < 0) {
            page = 0;
        }
        this.currentIndex = page;
        recalculateCurrentPage();
    }

    @Override
    public int getPosition(@NotNull H drawable) {
        if (positionMap.containsValue(drawable)) {
            return MiscUtils.min(positionMap.keySet().stream().filter(uiPosition -> positionMap.get(uiPosition) == drawable).collect(Collectors.toSet()));
        }
        throw new IllegalArgumentException("There is no drawable " + drawable + " in this scrollable!");
    }

    @Override
    public void setBackground(ItemStack background) {
        this.background = background;
    }

    @Override
    public ItemStack getBackground() {
        return background;
    }

    public Action getAction() {
        return listener;
    }

    public void setAction(Action action) {
        this.listener = action;
    }

    @Override
    public Map<UIPosition, ItemStack> drawUI() {
        ImmutableMap.Builder<UIPosition, ItemStack> builder = new ImmutableMap.Builder<>();
        Set<Component> addedObjects = new HashSet<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                UIPosition currentPos = new UIPosition(x, y);
                H uiObject = currentPage.get(currentPos.toInvSlot(width));
                if (uiObject != null) {
                    if (!addedObjects.contains(uiObject)) {
                        addedObjects.add(uiObject);
                        Map<UIPosition, ItemStack> childUI = uiObject.drawUI();
                        childUI.forEach((uiPosition, itemStack) -> builder.put(currentPos.add(uiPosition), itemStack));
                    }
                } else if (hasBackground()) {
                    builder.put(currentPos, getBackground());
                }
            }
        }
        return builder.build();
    }
}
