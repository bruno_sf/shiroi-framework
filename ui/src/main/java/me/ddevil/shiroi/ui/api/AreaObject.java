package me.ddevil.shiroi.ui.api;


import me.ddevil.shiroi.ui.misc.UIPosition;

import java.util.SortedSet;


/**
 * Represents an {@link Component} that can hold more than one minecraft slots inside a menu.
 */
public interface AreaObject extends Drawable {
    /**
     * Redefines the area to fit the desired sizes.
     *
     * @param width  The new width
     * @param height The new height
     */
    void redefineArea(int width, int height);

    /**
     * @return A set sorted by the minecraft slot position with all the UIPositions contained by this object.
     */
    SortedSet<UIPosition> getArea();

    /**
     * Checks if the given position is within the relative area of this object.
     *
     * @param position The position to check
     * @return true if the given position is within bounds, false otherwise.
     */
    boolean contains(UIPosition position);

    boolean contains(int x, int y);

    /**
     * @return The total number of slots that this object contains.
     */
    int getSize();

    /**
     * @return The maximum relative X of this object
     */
    int getMaxX();

    /**
     * @return The maximum relative Y of this object
     */
    int getMaxY();

    /**
     * @return The width of the object
     */
    int getWidth();

    /**
     * @return The height of the object
     */
    int getHeight();
}
