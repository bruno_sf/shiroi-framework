package me.ddevil.shiroi.ui.api.holder;

import me.ddevil.shiroi.ui.api.AreaObject;
import me.ddevil.shiroi.ui.api.Backgroundable;
import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.api.Drawable;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Represents an object that can hold {@link Drawable}s, like menus and containers.
 */
public interface Holder<H extends Drawable> extends AreaObject, Backgroundable, Clickable {
    /**
     * Adds the given drawable in the first available slot it finds.
     *
     * @param drawable The drawable to add.
     */
    void add(@NotNull H drawable);

    void remove(@NotNull H drawable);

    /**
     * Removes every single drawable that this holder contains
     */
    void clear();

    /**
     * Checks if the given object is contained within this holder.
     *
     * @param obj The object to check
     * @return True if this holder contains the given object, false otherwise.
     */
    boolean isOwnerOf(@NotNull H obj);

    /**
     * @return Every drawable that is contained in this holder
     */
    @NotNull List<H> getAllDrawables();


}
