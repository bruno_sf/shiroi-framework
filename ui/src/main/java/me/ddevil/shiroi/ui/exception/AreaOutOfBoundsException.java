package me.ddevil.shiroi.ui.exception;

import me.ddevil.shiroi.ui.api.AreaObject;
import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.misc.UIPosition;

/**
 * Created by bruno on 26/10/2016.
 */
public class AreaOutOfBoundsException extends PositionOutOfBoundsException {
    private final AreaObject areaObject;

    public AreaOutOfBoundsException(Holder<?> menu, UIPosition crashPoint, AreaObject areaObject) {
        super(menu, crashPoint);
        this.areaObject = areaObject;
    }

    public AreaObject getAreaObject() {
        return areaObject;
    }
}
