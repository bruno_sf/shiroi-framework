package me.ddevil.shiroi.ui.misc.builder.scrollable;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.misc.builder.HolderBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bruno on 10/12/2016.
 */
public abstract class AbstractScrollableBuilder<D extends Drawable, S extends Holder<D>> extends HolderBuilder<D, S> {
    protected final Map<Integer, D> map = new HashMap<>();

    public AbstractScrollableBuilder(int width, int height) {
        super(width, height);
    }

    public AbstractScrollableBuilder<D, S> place(Integer position, D drawable) {
        D put = map.put(position, drawable);
        return this;
    }
}
