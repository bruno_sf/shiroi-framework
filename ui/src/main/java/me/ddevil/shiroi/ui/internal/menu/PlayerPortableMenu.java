package me.ddevil.shiroi.ui.internal.menu;

import me.ddevil.shiroi.ui.UIConstants;
import me.ddevil.shiroi.ui.api.Clickable;
import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.event.InteractionType;
import me.ddevil.shiroi.ui.event.UIClickEvent;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class PlayerPortableMenu<P extends JavaPlugin> extends AutonomousMenu<P> {

    public PlayerPortableMenu(P plugin, Player player) {
        super(plugin, player.getInventory());
    }

    @Override
    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (!isEnabled()) {
            return;
        }
        Inventory clickedInventory = e.getInventory();
        if ((clickedInventory != null) && (this.mainInventory != null) && (clickedInventory.equals(this.mainInventory))) {
            e.setCancelled(true);
            int slot = e.getSlot();
            UIPosition position = UIPosition.fromSlot(slot, UIConstants.INVENTORY_SIZE_X);
            if (this.objectMap.containsKey(position)) {
                Drawable drawable = getDrawable(position);
                if (drawable instanceof Clickable) {
                    InteractionType type;
                    if (e.isLeftClick())
                        type = InteractionType.INTERACT_CLICK_LEFT;
                    else {
                        type = InteractionType.INTERACT_CLICK_RIGHT;
                    }
                    UIClickEvent call = new UIClickEvent(
                            ((Clickable) drawable),
                            position,
                            (Player) e.getWhoClicked(),
                            type,
                            null,
                            e.getCursor()
                    ).call();
                    handler.onInteract(call, position);
                }
            }
        }
    }

    @Override
    public void open(Player p) {
        update();
    }
}
