package me.ddevil.shiroi.ui.internal.scrollable;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import me.ddevil.shiroi.ui.api.*;
import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.event.UIActionEvent;
import me.ddevil.shiroi.ui.event.UIClickEvent;
import me.ddevil.shiroi.ui.api.Action;
import me.ddevil.shiroi.ui.misc.ScrollDirection;
import me.ddevil.shiroi.ui.misc.UIPosition;
import me.ddevil.shiroi.ui.api.updater.ScrollableUpdater;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class LowPanedScrollable<H extends Item<?>> extends UnpannedScrollable<H> {

    private final Map<Integer, Item> lowPanel;
    private ItemStack lowPanelBackground;


    public LowPanedScrollable(int width, int height, ScrollableUpdater<Scrollable<?>> updater) {
        super(width, height);
        if (height < 1) {
            throw new IllegalArgumentException("A low paned scrollable requires a minimum height of 2! Or maybe you could also use a SingleLaneScrollable :D");
        }
        redefineArea(width, height - 1);
        setAction(new PanelScrollableClickHandler());
        this.lowPanel = new HashMap<>();
        lowPanel.put(0, new PageScroller(this, updater, ScrollDirection.PREVIOUS));
        lowPanel.put(width - 1, new PageScroller(this, updater, ScrollDirection.NEXT));
    }

    private class PanelScrollableClickHandler implements Action {


        @Override
        public void onInteract(UIClickEvent e, UIPosition pos) {
            int clickedSlot = pos.toInvSlot(width);
            Component obj = null;
            if (currentPage.containsKey(clickedSlot)) {
                obj = currentPage.get(clickedSlot);
            } else if (pos.getY() == getMaxY() + 1) {
                //Was on panel
                Integer x = pos.getX();
                if (lowPanel.containsKey(x)) {
                    obj = lowPanel.get(x);
                }
            }
            if (obj != null) {
                obj.update();
                if (obj instanceof Clickable) {
                    Clickable c = (Clickable) obj;
                    UIPosition objLocalPos = objLocalPos = e.getClickedSlot().subtract(pos);
                    c.getAction().onInteract(e, objLocalPos);
                    new UIActionEvent(c, e.getClickedSlot(), e.getPlayer(), e.getClickType(), e.getPlacedBlock()).call();
                }
            }
        }
    }

    @Override
    public int getSize() {
        return (height + 1) * width;
    }

    @Override
    public SortedSet<UIPosition> getArea() {
        int maxX = getMaxX();
        int maxY = getMaxY() + 1;
        ImmutableSortedSet.Builder<UIPosition> menuPositionBuilder = new ImmutableSortedSet.Builder<>(UIPosition::compareTo);
        for (int x = 0; x <= maxX; x++) {
            for (int y = 0; y <= maxY; y++) {
                menuPositionBuilder.add(new UIPosition(x, y));
            }
        }
        return menuPositionBuilder.build();
    }

    @Override
    public Map<UIPosition, ItemStack> drawUI() {
        ImmutableMap.Builder<UIPosition, ItemStack> builder = new ImmutableMap.Builder<UIPosition, ItemStack>().putAll(super.drawUI());
        int maxY = getMaxY() + 1;
        for (int x = 0; x < width; x++) {
            Item item = lowPanel.get(x);
            UIPosition pos = new UIPosition(x, maxY);
            if (item != null) {
                item.update();
                ItemStack icon = item.getIcon();
                if (icon != null) {
                    builder.put(pos, icon);
                }
            } else if (hasLowPanelBackground()) {
                builder.put(pos, getLowPanelBackground());
            }
        }
        return builder.build();
    }

    public boolean hasObjectInLowPanel(int slot) {
        return lowPanel.containsKey(slot);
    }

    public void placeInLowPanel(Item item, int slot) {
        if (hasObjectInLowPanel(slot)) {
            throw new IllegalArgumentException("There is already an object in low panel slot " + slot + "!");
        }
        lowPanel.put(slot, item);
    }

    public void removeFromLowPanel(int slot) {
        if (hasObjectInLowPanel(slot)) {
            lowPanel.remove(slot);
        }
    }

    public boolean hasLowPanelBackground() {
        return getLowPanelBackground() != null;
    }

    public ItemStack getLowPanelBackground() {
        return lowPanelBackground;
    }

    public void setLowPanelBackground(ItemStack lowPanelBackground) {
        this.lowPanelBackground = lowPanelBackground;
    }
}
