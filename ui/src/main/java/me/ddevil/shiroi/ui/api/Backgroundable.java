package me.ddevil.shiroi.ui.api;

import org.bukkit.inventory.ItemStack;

/**
 * Represents an object that can contain a background.
 */
public interface Backgroundable extends Component {
    /**
     * Sets the given item as the background
     *
     * @param background The background item to be set
     */
    void setBackground(ItemStack background);

    /**
     * @return The item that represents the background
     */
    ItemStack getBackground();

    /**
     * @return true if there is a background set.
     */
    default boolean hasBackground() {
        return getBackground() != null;
    }
}
