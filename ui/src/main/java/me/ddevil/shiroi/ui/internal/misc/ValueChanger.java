package me.ddevil.shiroi.ui.internal.misc;

import me.ddevil.shiroi.ui.api.holder.Holder;
import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 30/10/2016.
 */
public class ValueChanger<H extends Holder<?>> extends ClickableItem {

    private final double changeValue;

    public ValueChanger(ItemStack itemStack, OnValueChangedListener listener, double changeValue) {
        super(itemStack, (e, localPosition) -> listener.onValueChanged(changeValue));
        this.changeValue = changeValue;
    }

    public ValueChanger(ItemUpdater updater, OnValueChangedListener listener, double changeValue) {
        super(updater, (e, localPosition) -> listener.onValueChanged(changeValue));
        this.changeValue = changeValue;
    }

    public double getChangeValue() {
        return changeValue;
    }

    public interface OnValueChangedListener {
        void onValueChanged(double newValue);
    }
}
