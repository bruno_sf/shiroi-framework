package me.ddevil.shiroi.ui.misc;

import me.ddevil.shiroi.ui.api.Menu;
import me.ddevil.shiroi.ui.api.holder.Scrollable;

/**
 * Created by BRUNO II on 18/07/2016.
 */
public enum SlotType {
    /**
     * Represents a raw slot in the inventory, for example, slot 9 would be a {@link me.ddevil.shiroi.ui.misc.UIPosition} with x=0 and y=1
     */
    INVENTORY,
    /**
     * Represents a slot inside a {@link me.ddevil.shiroi.ui.api.holder.Container}, said container could be a {@link Menu}, or {@link Scrollable}, etc.
     * <br>
     * Anything that can contains objects inside it.
     */
    MENU;

}
