package me.ddevil.shiroi.ui.internal.scrollable;

import me.ddevil.shiroi.ui.api.holder.Scrollable;
import me.ddevil.shiroi.ui.internal.misc.base.BasicItem;
import me.ddevil.shiroi.ui.api.updater.ScrollableUpdater;
import me.ddevil.shiroi.ui.misc.ScrollDirection;
import org.bukkit.inventory.ItemStack;

public class PageScroller<O extends Scrollable<?>> extends BasicItem<ScrollableUpdater<O>> {

    private final O parent;
    private final ScrollDirection direction;

    public PageScroller(O parent, ScrollableUpdater<O> updater, ScrollDirection direction) {
        super(updater);
        this.parent = parent;
        this.direction = direction;
    }

    public PageScroller(O parent, ItemStack icon, ScrollDirection direction) {
        super(icon);
        this.parent = parent;
        this.direction = direction;
    }

    @Override
    protected ItemStack updateIcon(ScrollableUpdater<O> updater) {
        return updater.update(parent, direction);
    }
}
