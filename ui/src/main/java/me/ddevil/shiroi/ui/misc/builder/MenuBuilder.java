package me.ddevil.shiroi.ui.misc.builder;

import me.ddevil.shiroi.ui.UIConstants;
import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.Menu;
import me.ddevil.shiroi.ui.internal.menu.AutonomousMenu;
import me.ddevil.shiroi.ui.internal.menu.ContainerMenu;
import me.ddevil.shiroi.ui.misc.builder.container.AbstractContainerBuilder;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by bruno on 10/12/2016.
 */
public class MenuBuilder<P extends JavaPlugin> extends AbstractContainerBuilder<Drawable, Menu<P>> {


    private final P plugin;
    private boolean autonomous = true;
    private String title;
    private Menu.Size size;

    public MenuBuilder(Menu.Size size, P plugin) {
        super(UIConstants.INVENTORY_SIZE_X, size.getRows());
        this.size = size;
        this.plugin = plugin;
    }

    public MenuBuilder<P> autonomous(boolean autonomous) {
        this.autonomous = autonomous;
        return this;
    }

    public MenuBuilder<P> withTitle(String title) {
        this.title = title;
        return this;
    }

    @Override
    public Menu<P> build() {
        if (autonomous) {
            return new AutonomousMenu<P>(plugin, title, size, components) {
                @Override
                protected void setup1() {

                }

                @Override
                protected void update0() {

                }
            };
        } else {
            return new ContainerMenu<P>(plugin, title, size, components) {
                @Override
                protected void setup0() {

                }

                @Override
                protected void disable0() {

                }

                @Override
                protected void update0() {

                }
            };
        }
    }
}
