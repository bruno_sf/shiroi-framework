package me.ddevil.shiroi.ui.internal.misc;

import me.ddevil.shiroi.ui.api.updater.ItemUpdater;
import me.ddevil.shiroi.ui.api.updater.Updater;
import me.ddevil.shiroi.ui.internal.misc.base.BasicItem;
import org.bukkit.inventory.ItemStack;

public class NormalItem extends BasicItem<ItemUpdater> {
    public NormalItem(ItemUpdater updater) {
        super(updater);
    }

    public NormalItem(ItemStack icon) {
        super(icon);
    }

    @Override
    protected ItemStack updateIcon(ItemUpdater updater) {
        return updater.update();
    }
}
