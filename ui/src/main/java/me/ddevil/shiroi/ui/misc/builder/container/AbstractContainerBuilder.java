package me.ddevil.shiroi.ui.misc.builder.container;

import me.ddevil.shiroi.ui.api.Drawable;
import me.ddevil.shiroi.ui.api.holder.Container;
import me.ddevil.shiroi.ui.misc.UIPosition;
import me.ddevil.shiroi.ui.misc.builder.HolderBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by bruno on 10/12/2016.
 */
public abstract class AbstractContainerBuilder<D extends Drawable, C extends Container<D>> extends HolderBuilder<D, C> {
    protected final Map<UIPosition, D> components = new HashMap<>();

    public AbstractContainerBuilder(int width, int height) {
        super(width, height);
    }

    public AbstractContainerBuilder<D, C> place(UIPosition position, D item) {
        D removed = components.put(position, item);
        if (removed != null) {
            Logger.getAnonymousLogger().log(Level.WARNING, "A item " + removed.toString() + " was removed from position " + position + " while building a container!");
        }
        return this;
    }
}
