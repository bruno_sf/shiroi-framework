package me.ddevil.shiroi.ui.api;

import me.ddevil.shiroi.misc.Toggleable;
import me.ddevil.shiroi.ui.api.holder.Container;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public interface Menu<P extends JavaPlugin> extends Toggleable, Container<Drawable> {
    enum Size {
        ONE_ROW,
        TWO_ROWS,
        THREE_ROWS,
        FOUR_ROWS,
        FIVE_ROWS,
        SIX_ROWS;

        public int getRows() {
            return ordinal() + 1;
        }
    }

    default void open(Player p) {
        update();
        p.openInventory(getBukkitInventory());
    }

    Inventory getBukkitInventory();

    P getPlugin();

}
