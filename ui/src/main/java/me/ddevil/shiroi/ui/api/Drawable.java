package me.ddevil.shiroi.ui.api;

import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.inventory.ItemStack;

import java.util.Map;


/**
 * Represents an object that can be displayed with ItemStacks in a menu
 */
public interface Drawable extends Component {
    /**
     * <b>This is one of the most important functions in this framework. DON'T SCREW IT UP!</b>
     * The drawUI function must return a map that shows where every item should be displayed relatively to itself.
     * For example, a drawable that only contains one slots should return a singleton map where the position points to (0,0).
     *
     * @return The map that will be used to display this object in the menu.
     */
    Map<UIPosition, ItemStack> drawUI();

}
