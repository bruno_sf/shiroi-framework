package me.ddevil.shiroi.ui.api.updater;

import org.bukkit.inventory.ItemStack;

/**
 * Created by bruno on 25/09/2016.
 */
public interface ItemUpdater extends Updater {
    ItemStack update();
}
