package me.ddevil.shiroi.ui.test;

import me.ddevil.shiroi.ui.UIConstants;
import me.ddevil.shiroi.ui.api.Menu;
import me.ddevil.shiroi.ui.exception.PositionOutOfBoundsException;
import me.ddevil.shiroi.ui.internal.container.ArrayContainer;
import me.ddevil.shiroi.ui.internal.menu.ContainerMenu;
import me.ddevil.shiroi.ui.misc.UIPosition;
import org.bukkit.plugin.java.JavaPlugin;
import org.junit.Test;

/**
 * Created by bruno on 13/10/2016.
 */
public class BaseAreaTest {
    private static final int MULTIPLIER = 1;

    @Test
    public void areaTest() {
        System.out.println(new PositionOutOfBoundsException(null, new UIPosition(3, 2), true).getMessage());
        ContainerMenu<JavaPlugin> containerMenu = new ContainerMenu<JavaPlugin>(null, null) {
            @Override
            protected void setup0() {

            }

            @Override
            protected void disable0() {

            }

            @Override
            protected void update0() {

            }
        };
        for (int y = 0; y < UIConstants.INVENTORY_SIZE_Y * MULTIPLIER; y++) {
            for (int x = 0; x < UIConstants.INVENTORY_SIZE_X * MULTIPLIER; x++) {
                UIPosition pos = new UIPosition(x, y);
                System.out.println("inBounds (" + pos + ") ? " + isInBounds(pos));
            }
        }
        UIPosition pos = new UIPosition(0, 0);
        containerMenu.place(new ArrayContainer<>(5, 3), pos);
        UIPosition uiPosition = new UIPosition(0, 1);
        System.out.println(containerMenu.hasObjectIn(uiPosition));
    }

    private boolean isInBounds(UIPosition position) {
        return position.getX() <= UIConstants.INVENTORY_SIZE_X && position.getY() <= UIConstants.INVENTORY_SIZE_Y;
    }
}
