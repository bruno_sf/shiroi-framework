import me.ddevil.shiroi.mojang.request.internal.NameHistoryRequest;
import me.ddevil.shiroi.mojang.request.internal.ProfileAndTexturesRequest;
import me.ddevil.shiroi.mojang.request.internal.UUIDRequest;
import org.junit.Test;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * Created by bruno on 08/12/2016.
 */
public class RequestTests {
    public static final String player = "DDevil_";

    @Test
    public void test() {
        System.out.println("Trying to get uuid for player " + player);
        UUID uuid;
        try {
            uuid = new UUIDRequest(player).call().getUUID();
            System.out.println("UUID: ");
            System.out.println(uuid);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        System.out.println();
        try {
            System.out.println("Getting profile and texture for uuid " + uuid + "...");
            ProfileAndTexturesRequest.ProfileAndTexturesResult result = new ProfileAndTexturesRequest(uuid).call();
            System.out.println("Name:");
            System.out.println(result.getName());
            ProfileAndTexturesRequest.TextureInfo info = result.getTextureInfo();
            System.out.println("Skin: ");
            System.out.println(info.getSkinUrl());
            System.out.println("Cape: ");
            System.out.println(info.getCapeUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Getting previous names for player " + player + "(" + uuid + ")");
        try {
            NameHistoryRequest.NameHistoryResult result = new NameHistoryRequest(uuid).call();
            result.getNames().forEach(minecraftName -> {
                System.out.println("--------");
                System.out.println("Found name: ");
                System.out.println(minecraftName.getName());
                System.out.println("Changed to at");
                System.out.println(minecraftName.getChangedToAt() + " / " + minecraftName.isFirstUsername());
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
