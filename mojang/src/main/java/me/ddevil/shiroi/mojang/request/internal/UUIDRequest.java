package me.ddevil.shiroi.mojang.request.internal;

import me.ddevil.shiroi.misc.request.internal.BaseResult;
import me.ddevil.shiroi.misc.request.internal.HttpJSONRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.simple.JSONObject;

import java.util.UUID;

/**
 * Created by bruno on 08/12/2016.
 */
public class UUIDRequest extends HttpJSONRequest<UUIDRequest.UUIDResponse, JSONObject> {
    private static final String API = "https://api.mojang.com/users/profiles/minecraft/";
    private final String name;

    public UUIDRequest(String name) {
        this.name = name;
    }

    @Override
    protected UUIDResponse handle(JSONObject json, HttpResponse response, long startTime, long endTime) {
        return new UUIDResponse(String.valueOf(json.get("id")), startTime, endTime);
    }

    @Override
    protected HttpUriRequest createRequest0() {
        System.out.println("Using api '"+API + name+"'");
        return new HttpGet(API + name);
    }

    @Override
    protected JSONObject createJson() {
        return null;
    }

    public class UUIDResponse extends BaseResult {
        private final UUID uuid;

        public UUIDResponse(String uuid, long startTime, long endTime) {
            super(startTime, endTime);
            this.uuid = UUID.fromString(uuid.substring(0, 8) + "-" + uuid.substring(8, 12) + "-" + uuid.substring(12, 16) + "-" + uuid.substring(16, 20) + "-" +uuid.substring(20, 32));
        }

        public UUID getUUID() {
            return uuid;
        }
    }
}
