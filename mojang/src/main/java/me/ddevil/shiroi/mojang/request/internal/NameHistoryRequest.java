package me.ddevil.shiroi.mojang.request.internal;

import me.ddevil.shiroi.misc.request.internal.BaseResult;
import me.ddevil.shiroi.misc.request.internal.HttpJSONRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by bruno on 08/12/2016.
 */
public class NameHistoryRequest extends HttpJSONRequest<NameHistoryRequest.NameHistoryResult, JSONArray> {
    private final static String API = "https://api.mojang.com/user/profiles/";
    private final UUID uuid;

    public NameHistoryRequest(UUID uuid) {
        this.uuid = uuid;
    }

    public class NameHistoryResult extends BaseResult {
        private final List<MinecraftName> names;

        public NameHistoryResult(List<Map<String, Object>> maps, long startTime, long endTime) {
            super(startTime, endTime);
            this.names = maps.stream().map(MinecraftName::new).collect(Collectors.toList());
        }

        public List<MinecraftName> getNames() {
            return names;
        }
    }

    public class MinecraftName {
        private final String name;
        private final long changedToAt;
        private final boolean isFirstUsername;

        public MinecraftName(Map<String, Object> map) {
            this.name = String.valueOf(map.get("name"));
            if (map.containsKey("changedToAt")) {
                this.isFirstUsername = false;
                this.changedToAt = ((Number) map.get("changedToAt")).longValue();
            } else {
                this.isFirstUsername = true;
                this.changedToAt = -1;
            }
        }

        public String getName() {
            return name;
        }

        public long getChangedToAt() {
            return changedToAt;
        }

        public boolean isFirstUsername() {
            return isFirstUsername;
        }
    }

    @Override
    protected NameHistoryResult handle(JSONArray json, HttpResponse response, long startTime, long endTime) {
        return new NameHistoryResult(json, startTime, endTime);
    }

    @Override
    protected HttpUriRequest createRequest0() {
        return new HttpGet(API + uuid.toString().replace("-", "") + "/names");
    }

    @Override
    protected JSONObject createJson() {
        return null;
    }


}
