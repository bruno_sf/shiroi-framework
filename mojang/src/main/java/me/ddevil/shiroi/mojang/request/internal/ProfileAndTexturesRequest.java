/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.mojang.request.internal;

import me.ddevil.shiroi.misc.request.internal.BaseResult;
import me.ddevil.shiroi.misc.request.internal.HttpJSONRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.net.URI;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ProfileAndTexturesRequest extends HttpJSONRequest<ProfileAndTexturesRequest.ProfileAndTexturesResult, JSONObject> {
    private static final String PROFILE_URL = "https://sessionserver.mojang.com/session/minecraft/profile/";

    public static class ProfileAndTexturesResult extends BaseResult {
        private final String name;
        private final TextureInfo textureInfo;

        public ProfileAndTexturesResult(Map<String, Object> map, long startTime, long endTime) throws ParseException {
            super(startTime, endTime);
            this.name = String.valueOf(map.get("name"));
            String json = new String(Base64.getUrlDecoder().decode(String.valueOf(((List<Map<String, Object>>) map.get("properties")).get(0).get("value"))));
            this.textureInfo = new TextureInfo((JSONObject) new JSONParser().parse(json));
        }

        public TextureInfo getTextureInfo() {
            return textureInfo;
        }

        public String getName() {
            return name;
        }
    }

    public static class TextureInfo {
        private final long timestamp;
        private final String skinUrl;
        private final String capeUrl;

        public TextureInfo(Map<String, Object> map) {
            this.timestamp = ((Number) map.get("timestamp")).longValue();
            Map<String, Object> textures = (Map<String, Object>) map.get("textures");
            if (textures.containsKey("SKIN")) {
                skinUrl = String.valueOf(((Map<String, Object>) textures.get("SKIN")).get("url"));
            } else {
                skinUrl = null;
            }
            if (textures.containsKey("CAPE")) {
                capeUrl = String.valueOf(((Map<String, Object>) textures.get("CAPE")).get("url"));
            } else {
                capeUrl = null;
            }
        }

        public long getTimestamp() {
            return timestamp;
        }

        public String getSkinUrl() {
            return skinUrl;
        }

        public String getCapeUrl() {
            return capeUrl;
        }
    }


    private final UUID uuid;


    public ProfileAndTexturesRequest(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    protected ProfileAndTexturesResult handle(JSONObject json, HttpResponse response, long startTime, long endTime) {
        try {
            return new ProfileAndTexturesResult(json, startTime, endTime);
        } catch (Exception e) {
            throw new IllegalStateException("There was a problem while parsing result for uuid " + uuid + "!", e);
        }
    }

    @Override
    protected HttpUriRequest createRequest0() {
        return new HttpGet(URI.create(PROFILE_URL + uuid.toString().replace("-", "")));

    }

    @Override
    protected JSONObject createJson() {
        return null;
    }


}