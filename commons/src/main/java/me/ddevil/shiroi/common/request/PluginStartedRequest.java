package me.ddevil.shiroi.common.request;

import me.ddevil.shiroi.misc.request.internal.BaseRequest;
import me.ddevil.shiroi.misc.request.internal.DiscardResult;

public class PluginStartedRequest extends BaseRequest<DiscardResult> {
    private final String pluginName;
    private final String groupId;
    private final String version;

    public PluginStartedRequest(String pluginName, String groupId, String version) {
        this.pluginName = pluginName;
        this.groupId = groupId;
        this.version = version;
    }

    public String getPluginName() {
        return pluginName;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getVersion() {
        return version;
    }

    @Override
    protected DiscardResult call0() throws Exception {
        return new DiscardResult();
    }
}
