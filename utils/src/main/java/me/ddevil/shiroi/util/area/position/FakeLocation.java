package me.ddevil.shiroi.util.area.position;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.ShiroiConstants;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class FakeLocation extends SimpleLocation {
    private static final String WORLD_IDENTIFIER = "world";
    private String world;

    public FakeLocation(String world, double x, double y, double z) {
        super(x, y, z);
        this.world = world;
    }

    public FakeLocation(String world, double x, double y, double z, float yaw, float pitch) {
        super(x, y, z, yaw, pitch);
        this.world = world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public String getWorld() {
        return this.world;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(WORLD_IDENTIFIER, world)
                .build();
    }

    public static FakeLocation deserialize(Map<String, Object> args) {
        return new FakeLocation(
                (String) args.get(WORLD_IDENTIFIER), ((Number) args.get(ShiroiConstants.X_IDENTIFIER)).doubleValue(),
                ((Number) args.get(ShiroiConstants.Y_IDENTIFIER)).doubleValue(),
                ((Number) args.get(ShiroiConstants.Z_IDENTIFIER)).doubleValue(),
                ((Number) args.get(YAW_IDENTIFIER)).floatValue(),
                ((Number) args.get(PITCH_IDENTIFIER)).floatValue()
        );

    }
}
