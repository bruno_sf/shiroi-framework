package me.ddevil.shiroi.util.vector;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.ShiroiConstants;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

/**
 * Created by bruno on 08/10/2016.
 */
public class Vector3<N extends Number> extends Vector2<N> {
    public static final Vector3<Integer> INT_ZERO_3 = new Vector3<>(0, 0, 0);

    private static Number get(String key, Map<String, Object> map) {
        return ((Number) map.get(key));
    }

    public static Vector3<Integer> deserializeInt(Map<String, Object> map) {
        return new Vector3<>(
                get(ShiroiConstants.X_IDENTIFIER, map).intValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).intValue(),
                get(ShiroiConstants.Z_IDENTIFIER, map).intValue()
        );
    }

    public static Vector3<Long> deserializeLong(Map<String, Object> map) {
        return new Vector3<>(
                get(ShiroiConstants.X_IDENTIFIER, map).longValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).longValue(),
                get(ShiroiConstants.Z_IDENTIFIER, map).longValue()
        );
    }

    public static Vector3<Float> deserializeFloat(Map<String, Object> map) {
        return new Vector3<>(
                get(ShiroiConstants.X_IDENTIFIER, map).floatValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).floatValue(),
                get(ShiroiConstants.Z_IDENTIFIER, map).floatValue()
        );
    }

    public static Vector3<Double> deserializeDouble(Map<String, Object> map) {
        return new Vector3<>(
                get(ShiroiConstants.X_IDENTIFIER, map).doubleValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).doubleValue(),
                get(ShiroiConstants.Z_IDENTIFIER, map).doubleValue()
        );
    }


    @NotNull
    protected N z;

    public Vector3(@NotNull N x, @NotNull N y, @NotNull N z) {
        super(x, y);
        this.z = z;
    }

    public void setZ(@NotNull N z) {
        this.z = z;
    }

    @NotNull
    public N getZ() {
        return z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3<?> vector2 = (Vector3<?>) o;

        if (!x.equals(vector2.x)) return false;
        return z.equals(vector2.z) && y.equals(vector2.y);
    }

    @Override
    public String
    toString() {
        return "Vector3{x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(ShiroiConstants.Z_IDENTIFIER, z)
                .build();
    }

    public double distance(Vector3<?> o) {
        double xDist = x.doubleValue() - o.x.doubleValue();
        double yDist = y.doubleValue() - o.y.doubleValue();
        double zDist = z.doubleValue() - o.z.doubleValue();
        return Math.sqrt(xDist * xDist + yDist * yDist + zDist * zDist);
    }
}
