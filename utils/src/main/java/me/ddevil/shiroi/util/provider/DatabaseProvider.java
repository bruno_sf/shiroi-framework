package me.ddevil.shiroi.util.provider;

import java.net.InetAddress;

/**
 * Created by bruno on 28/11/2016.
 */
public interface DatabaseProvider<K, V> extends Provider<K, V> {

    InetAddress getHost();

    String getUsername();

    char[] getPassword();

}
