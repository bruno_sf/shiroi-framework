package me.ddevil.shiroi.util.item;

import me.ddevil.shiroi.misc.Serializable;

import org.jetbrains.annotations.NotNull;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Represents a stack of items
 */
public class Item implements Cloneable, Serializable {
    private int type = 0;
    private int amount = 0;
    private short durability = 0;

    protected Item() {
    }

    /**
     * Defaults stack size to 1, with no extra data
     *
     * @param type item material id
     * @deprecated Magic value
     */
    @Deprecated
    public Item(final int type) {
        this(type, 1);
    }

    /**
     * Defaults stack size to 1, with no extra data
     *
     * @param type item material
     */
    public Item(final Material type) {
        this(type, 1);
    }

    /**
     * An item stack with no extra data
     *
     * @param type   item material id
     * @param amount stack size
     * @deprecated Magic value
     */
    @Deprecated
    public Item(final int type, final int amount) {
        this(type, amount, (short) 0);
    }

    /**
     * An item stack with no extra data
     *
     * @param type   item material
     * @param amount stack size
     */
    public Item(final Material type, final int amount) {
        this(type.getId(), amount);
    }

    /**
     * An item stack with the specified damage / durability
     *
     * @param type   item material id
     * @param amount stack size
     * @param damage durability / damage
     * @deprecated Magic value
     */
    @Deprecated
    public Item(final int type, final int amount, final short damage) {
        this.type = type;
        this.amount = amount;
        this.durability = damage;
    }

    /**
     * An item stack with the specified damage / durabiltiy
     *
     * @param type   item material
     * @param amount stack size
     * @param damage durability / damage
     */
    public Item(final Material type, final int amount, final short damage) {
        this(type.getId(), amount, damage);
    }

    /**
     * @param type   the raw type id
     * @param amount the amount in the stack
     * @param damage the damage value of the item
     * @param data   the data value or null
     * @deprecated this method uses an ambiguous data byte internal
     */
    @Deprecated
    public Item(final int type, final int amount, final short damage, final Byte data) {
        this.type = type;
        this.amount = amount;
        this.durability = damage;
        if (data != null) {
            createData(data);
            this.durability = data;
        }
    }

    /**
     * @param type   the type
     * @param amount the amount in the stack
     * @param damage the damage value of the item
     * @param data   the data value or null
     * @deprecated this method uses an ambiguous data byte internal
     */
    @Deprecated
    public Item(final Material type, final int amount, final short damage, final Byte data) {
        this(type.getId(), amount, damage, data);
    }

    /**
     * Creates a new item stack derived from the specified stack
     *
     * @param stack the stack to copy
     * @throws IllegalArgumentException if the specified stack is null or
     *                                  returns an item meta not created by the item factory
     */
    public Item(final Item stack) throws IllegalArgumentException {
        this.type = stack.getTypeId();
        this.amount = stack.getAmount();
        this.durability = stack.getDurability();
    }

    /**
     * Gets the type of this item
     *
     * @return Type of the items in this stack
     */
    public Material getType() {
        return getType0(getTypeId());
    }

    private Material getType0() {
        return getType0(this.type);
    }

    private static Material getType0(int id) {
        Material material = Material.getMaterial(id);
        return material == null ? Material.AIR : material;
    }

    /**
     * Sets the type of this item
     * <p>
     * Note that in doing so you will reset the MaterialData for this stack
     *
     * @param type New type to set the items in this stack to
     */
    public void setType(Material type) {
        setTypeId(type.getId());
    }

    /**
     * Gets the type id of this item
     *
     * @return Type Id of the items in this stack
     * @deprecated Magic value
     */
    @Deprecated
    public int getTypeId() {
        return type;
    }

    /**
     * Sets the type id of this item
     * <p>
     * Note that in doing so you will reset the MaterialData for this stack
     *
     * @param type New type id to set the items in this stack to
     * @deprecated Magic value
     */
    @Deprecated
    public void setTypeId(int type) {
        this.type = type;
        createData((byte) 0);
    }

    /**
     * Gets the amount of items in this stack
     *
     * @return Amount of items in this stack
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the amount of items in this stack
     *
     * @param amount New amount of items in this stack
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Sets the durability of this item
     *
     * @param durability Durability of this item
     */
    public void setDurability(final short durability) {
        this.durability = durability;
    }

    /**
     * Gets the durability of this item
     *
     * @return Durability of this item
     */
    public short getDurability() {
        return durability;
    }

    /**
     * Get the maximum stacksize for the material hold in this Item.
     * (Returns -1 if it has no idea)
     *
     * @return The maximum you can stack this material to.
     */
    public int getMaxStackSize() {
        Material material = getType();
        if (material != null) {
            return material.getMaxStackSize();
        }
        return -1;
    }

    private void createData(final byte data) {
        Material mat = Material.getMaterial(type);
    }

    @Override
    public String toString() {
        return "Item{" +
                "type=" + type +
                ", amount=" + amount +
                ", durability=" + durability +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Item)) {
            return false;
        }

        Item stack = (Item) obj;
        return getAmount() == stack.getAmount() && isSimilar(stack);
    }

    /**
     * This method is the same as equals, but does not consider stack size
     * (amount).
     *
     * @param stack the item stack to compare to
     * @return true if the two stacks are equal, ignoring the amount
     */
    public boolean isSimilar(Item stack) {
        if (stack == null) {
            return false;
        }
        if (stack == this) {
            return true;
        }
        return getTypeId() == stack.getTypeId() && getDurability() == stack.getDurability();
    }

    @Override
    public Item clone() {
        try {
            Item itemStack = (Item) super.clone();
            return itemStack;
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }


    @NotNull
    public Map<String, Object> serialize() {
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        result.put("type", getType().name());
        result.put("amount", getAmount());
        if (getDurability() != 0) {
            result.put("damage", getDurability());
        }
        return result;
    }

    /**
     * Required method for configuration serialization
     *
     * @param args map to deserialize
     * @return deserialized item stack
     */
    public static Item deserialize(Map<String, Object> args) {
        Material type = Material.getMaterial((String) args.get("type"));
        short damage = 0;
        int amount = 1;

        if (args.containsKey("damage")) {
            damage = ((Number) args.get("damage")).shortValue();
        }

        if (args.containsKey("amount")) {
            amount = ((Number) args.get("amount")).intValue();
        }

        Item result = new Item(type, amount, damage);
        return result;
    }
}