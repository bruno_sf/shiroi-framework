package me.ddevil.shiroi.util.serialization;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import me.ddevil.shiroi.ShiroiConstants;
import me.ddevil.shiroi.misc.Nameable;
import me.ddevil.shiroi.misc.Serializable;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SerializationUtils {

    public interface Loader<O extends Nameable> {
        O load(Map<String, Object> serializedObject, Map<String, O> loadedObjects);
    }

    @NotNull
    public static <O extends Nameable> Collection<O> loadAndResolveDependentNameables(@NotNull List<Map<String, Object>> objects, @NotNull Loader<O> loader) {
        return loadAndResolveDependentNameables(objects, loader, ShiroiConstants.PARENT_IDENTIFIER);
    }

    @NotNull
    public static <O extends Nameable> Collection<O> loadAndResolveDependentNameables(@NotNull List<Map<String, Object>> objects, @NotNull Loader<O> loader, @NotNull String parentIdentifier) {
        return loadAndResolveDependentNameables(objects, loader, ShiroiConstants.NAMEABLE_NAME_IDENTIFIER, parentIdentifier);
    }

    @NotNull
    public static <O extends Nameable> Collection<O> loadAndResolveDependentNameables(@NotNull List<Map<String, Object>> objects, @NotNull Loader<O> loader, @NotNull String nameIdentifier, @NotNull String parentIdentifier) {
        Map<String, Map<String, Object>> unloadedObjectsMap = new HashMap<>();
        Map<String, O> loadedObjects = new HashMap<>();

        Map<String, String> dependencyMap = new HashMap<>();
        objects.forEach(map -> unloadedObjectsMap.put(String.valueOf(map.get(nameIdentifier)), map));
        unloadedObjectsMap.entrySet().stream()
                .filter(stringMapEntry -> stringMapEntry.getValue().containsKey(parentIdentifier))
                .forEach(stringMapEntry -> dependencyMap.put(stringMapEntry.getKey(), String.valueOf(stringMapEntry.getValue().get(parentIdentifier))));

        for (Map.Entry<String, Map<String, Object>> entry : unloadedObjectsMap.entrySet()) {
            String objectName = entry.getKey();
            //Check if was not loaded as a dependency
            if (!loadedObjects.containsKey(objectName)) {
                //Load object
                //Check if has a dependency of its own
                if (dependencyMap.containsKey(objectName)) {
                    //Check if dependency is loaded
                    String dependencyName = dependencyMap.get(objectName);
                    if (!loadedObjects.containsKey(dependencyName)) {
                        //Load dependency
                        O dependency = loader.load(unloadedObjectsMap.get(dependencyName), loadedObjects);
                        loadedObjects.put(dependencyName, dependency);
                    }
                }
                Map<String, Object> serializedObject = entry.getValue();
                loadedObjects.put(objectName, loader.load(serializedObject, loadedObjects));
            }
        }
        return loadedObjects.values();
    }


    @NotNull

    public static <K, V, FK, FV> Map<FK, FV> transformMap(@NotNull Map<K, V> map, @NotNull Function<K, FK> keyMapper, @NotNull Function<V, FV> valueMapper) {
        return map.entrySet().stream().collect(Collectors.toMap(
                kvEntry -> keyMapper.apply(kvEntry.getKey()),
                kvEntry -> valueMapper.apply(kvEntry.getValue())
        ));
    }

    @NotNull
    public static <K, V, FK, FV> Map<FK, Collection<FV>> serializeMultimap(@NotNull Multimap<K, V> multimap, @NotNull Function<K, FK> keyMapper, @NotNull Function<Collection<V>, Collection<FV>> valueMapper) {
        Stream<Map.Entry<K, Collection<V>>> stream = multimap.asMap().entrySet().stream();
        return stream.collect(Collectors.toMap(
                kvEntry -> keyMapper.apply(kvEntry.getKey()),
                kvEntry -> valueMapper.apply(kvEntry.getValue())
        ));
    }

    @NotNull
    public static List<Map<String, Object>> serializeCollection(@NotNull Collection<? extends Serializable> list) {
        return list.stream().map(Serializable::serialize).collect(Collectors.toList());
    }

    @NotNull
    public static List<String> serializeCollectionToReference(@NotNull Collection<? extends Nameable> list) {
        return list.stream().map(Nameable::getName).collect(Collectors.toList());
    }

    @NotNull
    public static <K, V, FK, FV> Multimap<FK, FV> deserializeMultimap(@NotNull Map<K, List<V>> serializedMap, @NotNull Function<K, FK> keyMapper, @NotNull Function<List<V>, List<FV>> valueMapper) {
        HashMultimap<FK, FV> multimap = HashMultimap.create();
        Map<FK, List<FV>> collect = serializedMap.entrySet().stream().collect(Collectors.toMap(
                kvEntry -> keyMapper.apply(kvEntry.getKey()),
                kvEntry -> valueMapper.apply(kvEntry.getValue())
        ));
        collect.forEach(multimap::putAll);
        return multimap;
    }

    @NotNull
    public static <O> Set<O> deserializeCollection(@NotNull Collection<Map<String, Object>> list, @NotNull Deserializer<O> function) {
        return list.stream().map(function::deserialize).collect(Collectors.toSet());
    }

    @NotNull
    public static <O> Set<O> deserializeCollectionFromReference(@NotNull Collection<String> list, @NotNull Function<String, O> function) {
        return list.stream().map(function).collect(Collectors.toSet());
    }

    @NotNull
    public static <O> Set<O> deserializeCollection(@NotNull Map<String, Object> map, @NotNull String listIdentifier, @NotNull Deserializer<O> function) {
        return deserializeCollection((Collection<Map<String, Object>>) map.get(listIdentifier), function);
    }

}
