package me.ddevil.shiroi.util.provider;

import java.net.InetAddress;

/**
 * Created by bruno on 28/11/2016.
 */
public abstract class BaseDatabaseProvider<K, V> implements DatabaseProvider<K, V> {
    protected final InetAddress host;
    protected final String username;
    protected final String password;
    protected final String database;
    protected final String clanCollection;
    protected final String userCollection;

    public BaseDatabaseProvider(InetAddress host, String username, String password, String database, String clanCollection, String userCollection) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.database = database;
        this.clanCollection = clanCollection;
        this.userCollection = userCollection;
    }

    @Override
    public InetAddress getHost() {
        return host;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public char[] getPassword() {
        return password.toCharArray();
    }
}
