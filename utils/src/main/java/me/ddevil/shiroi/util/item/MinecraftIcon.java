package me.ddevil.shiroi.util.item;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Serializable;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

public class MinecraftIcon implements Serializable {
    private static final String MATERIAL_IDENTIFIER = "type";
    private static final String DATA_IDENTIFIER = "data";
    public static final String ICON_IDENTIFIER = "icon";
    public static final MinecraftIcon PLACEHOLDER = new MinecraftIcon(Material.STONE);
    private Material material = Material.AIR;
    private byte data = 0;

    public MinecraftIcon(Material material, byte data) {
        this.material = material;
        this.data = data;
    }

    public MinecraftIcon(Material material) {
        this(material, (byte) 0);
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }


    @NotNull
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(MATERIAL_IDENTIFIER, material.name())
                .put(DATA_IDENTIFIER, data)
                .build();
    }

    /**
     * Required method for configuration serialization
     *
     * @param args map to deserialize
     * @return deserialized item stack
     */
    public static MinecraftIcon deserialize(Map<String, Object> args) {
        Material type = Material.getMaterial((String) args.get("type"));
        byte data = ((Number) args.get(DATA_IDENTIFIER)).byteValue();
        MinecraftIcon result = new MinecraftIcon(type, data);
        return result;
    }

    @Override
    public String toString() {
        return "MinecraftIcon{" + material + ":" + data + '}';
    }


    public static final class Builder {
        private static String MATERIAL_IDENTIFIER = "type";
        private static String DATA_IDENTIFIER = "data";
        private Material material;
        private byte data = 0;

        public Builder(Material material, byte data) {
            this.material = material;
            this.data = data;
        }

        public Builder(Material material) {
            this.material = material;
        }

        public Builder() {
            this(Material.STONE);
        }

        public Builder withMaterial(Material material) {
            this.material = material;
            return this;
        }

        public Builder withData(byte data) {
            this.data = data;
            return this;
        }

        public MinecraftIcon build() {
            return new MinecraftIcon(material, data);
        }
    }
}
