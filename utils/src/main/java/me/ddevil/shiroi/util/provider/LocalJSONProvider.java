package me.ddevil.shiroi.util.provider;

import com.google.common.cache.LoadingCache;
import me.ddevil.shiroi.misc.Serializable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public abstract class LocalJSONProvider<K, V extends Serializable> extends LocalProvider<K, V> {
    private static final String JSON_EXTENSION = ".json";

    public LocalJSONProvider(LoadingCache<K, V> cache, File folder) {
        super(cache, folder, JSON_EXTENSION);
    }

    public LocalJSONProvider(long timeOut, TimeUnit unit, File folder) {
        super(timeOut, unit, folder, JSON_EXTENSION);
    }

    @Override
    public void save(V object) {
        File file = getFile(object);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(new JSONObject(object.serialize()).toJSONString());
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException("There was a problem while saving object" + object + "!", e);
        }
    }

    protected abstract File getFile(V object);

    @Override
    protected final V loadValue(String loadedFile) {
        try {
            return load0((JSONObject) new JSONParser().parse(loadedFile));
        } catch (ParseException e) {
            throw new IllegalStateException("There was a problem while parsing the file!", e);
        }
    }

    protected abstract V load0(JSONObject json);
}
