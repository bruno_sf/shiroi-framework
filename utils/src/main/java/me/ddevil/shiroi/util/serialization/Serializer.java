package me.ddevil.shiroi.util.serialization;

import java.util.Map;

public interface Serializer<T> {
    Map<String, Object> serialize(T value);
}
