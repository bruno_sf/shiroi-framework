package me.ddevil.shiroi.util.provider;

import com.google.common.cache.LoadingCache;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by bruno on 22/11/2016.
 */
public abstract class LocalProvider<K, V> extends BaseLocalProvider<K, V> {

    protected final File folder;
    protected final String fileExtension;

    public LocalProvider(LoadingCache<K, V> cache, File folder, String fileExtension) {
        super(cache);
        this.folder = folder;
        this.fileExtension = fileExtension;
        ensureFolder();
    }

    public LocalProvider(long timeOut, TimeUnit unit, File folder, String fileExtension) {
        super(timeOut, unit);
        this.folder = folder;
        this.fileExtension = fileExtension;
        ensureFolder();
    }

    private void ensureFolder() {
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    @Override
    protected File getFile(K key) {
        return new File(folder, key.toString() + fileExtension);
    }
}