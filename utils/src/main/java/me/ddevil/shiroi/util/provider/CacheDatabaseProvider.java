package me.ddevil.shiroi.util.provider;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

/**
 * Created by bruno on 28/11/2016.
 */
public abstract class CacheDatabaseProvider<K, V> extends BaseDatabaseProvider<K, V> {
    
    private final LoadingCache<K, V> cache;

    public CacheDatabaseProvider(InetAddress host, String username, String password, String database, String clanCollection, String userCollection, LoadingCache<K, V> cache) {
        super(host, username, password, database, clanCollection, userCollection);
        this.cache = cache;
    }

    public CacheDatabaseProvider(InetAddress host, String username, String password, String database, String clanCollection, String userCollection, int timeout, TimeUnit timeUnit) {
        super(host, username, password, database, clanCollection, userCollection);
        this.cache = CacheBuilder.newBuilder()
                .expireAfterWrite(timeout, timeUnit)
                .build(createLoader());
    }

    protected abstract CacheLoader<K, V> createLoader();
}
