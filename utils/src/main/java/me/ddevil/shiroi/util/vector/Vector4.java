package me.ddevil.shiroi.util.vector;
import org.jetbrains.annotations.NotNull;

public class Vector4<N extends Number> extends Vector3<N> {

    @NotNull
    private N a;

    public Vector4(@NotNull N x, @NotNull N y, @NotNull N z, @NotNull N a) {
        super(x, y, z);
        this.a = a;
    }

    @NotNull
    public N getA() {
        return a;
    }

    public void setA(@NotNull N a) {
        this.a = a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector4<?> vector2 = (Vector4<?>) o;
        return x.equals(vector2.x) && z.equals(vector2.z) && a.equals(vector2.a) && y.equals(vector2.y);

    }

    @Override
    public String
    toString() {
        return "Vector4{x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", a=" + a +
                '}';
    }
}
