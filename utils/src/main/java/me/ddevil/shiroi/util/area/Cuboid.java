package me.ddevil.shiroi.util.area;

import me.ddevil.shiroi.misc.Serializable;
import me.ddevil.shiroi.util.vector.Vector3;
import me.ddevil.shiroi.util.area.position.FakeLocation;

import org.jetbrains.annotations.NotNull;
import java.util.*;


public class Cuboid implements Cloneable, Serializable {
    public static final String CUBOID_X1_IDENTIFIER = "x1";
    public static final String CUBOID_Y1_IDENTIFIER = "y1";
    public static final String CUBOID_Z1_IDENTIFIER = "z1";
    public static final String CUBOID_X2_IDENTIFIER = "x2";
    public static final String CUBOID_Y2_IDENTIFIER = "y2";
    public static final String CUBOID_Z2_IDENTIFIER = "z2";

    private int x1;
    private int y1;
    private int z1;
    private int x2;
    private int y2;
    private int z2;
    protected String world;

    /**
     * Construct a Cuboid given two FakeLocation objects which represent any two corners of the Cuboid.
     * Note: The 2 locations must be on the same world.
     *
     * @param l1 - One of the corners
     * @param l2 - The other corner
     */
    public Cuboid(FakeLocation l1, FakeLocation l2) {
        if (!l1.getWorld().equals(l2.getWorld()))
            throw new IllegalArgumentException("Locations must be on the same world");
        this.x1 = Math.min(l1.getBlockX(), l2.getBlockX());
        this.y1 = Math.min(l1.getBlockY(), l2.getBlockY());
        this.z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
        this.x2 = Math.max(l1.getBlockX(), l2.getBlockX());
        this.y2 = Math.max(l1.getBlockY(), l2.getBlockY());
        this.z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());
    }

    public Cuboid(FakeLocation l1, FakeLocation l2, String world) {
        this(l1, l2);
        this.world = world;
    }

    /**
     * Construct a one-block Cuboid at the given FakeLocation of the Cuboid.
     *
     * @param l1 location of the Cuboid
     */
    public Cuboid(FakeLocation l1) {
        this(l1, l1);
    }

    public Cuboid(FakeLocation l1, String world) {
        this(l1);
        this.world = world;
    }

    /**
     * Copy constructor.
     *
     * @param other - The Cuboid to copy
     */
    public Cuboid(Cuboid other) {
        this(other.x1, other.y1, other.z1, other.x2, other.y2, other.z2, other.getWorld());
    }

    /**
     * Construct a Cuboid in the given world name and xyz co-ordinates.
     *
     * @param x1 - X co-ordinate of corner 1
     * @param y1 - Y co-ordinate of corner 1
     * @param z1 - Z co-ordinate of corner 1
     * @param x2 - X co-ordinate of corner 2
     * @param y2 - Y co-ordinate of corner 2
     * @param z2 - Z co-ordinate of corner 2
     */
    public Cuboid(int x1, int y1, int z1, int x2, int y2, int z2) {
        this.x1 = Math.min(x1, x2);
        this.x2 = Math.max(x1, x2);
        this.y1 = Math.min(y1, y2);
        this.y2 = Math.max(y1, y2);
        this.z1 = Math.min(z1, z2);
        this.z2 = Math.max(z1, z2);
    }

    /**
     * Construct a Cuboid using a map with the following keys: worldName, x1, x2, y1, y2, z1, z2
     *
     * @param map - The map of keys.
     */
    public Cuboid(Map<String, Object> map) {
        this.x1 = ((Number) map.get(CUBOID_X1_IDENTIFIER)).intValue();
        this.y1 = ((Number) map.get(CUBOID_Y1_IDENTIFIER)).intValue();
        this.z1 = ((Number) map.get(CUBOID_Z1_IDENTIFIER)).intValue();
        this.x2 = ((Number) map.get(CUBOID_X2_IDENTIFIER)).intValue();
        this.y2 = ((Number) map.get(CUBOID_Y2_IDENTIFIER)).intValue();
        this.z2 = ((Number) map.get(CUBOID_Z2_IDENTIFIER)).intValue();
    }

    public Cuboid(int x1, int y1, int z1, int x2, int y2, int z2, String world) {
        this(x1, y1, z1, x2, y2, z2);
        this.world = world;
    }

    public Axis getXAxis() {
        Vector3<Integer> center = getCenter();
        int y = center.getY();
        int z = center.getZ();
        Vector3<Integer> pos1 = new Vector3<>(x1, y, z);
        Vector3<Integer> pos2 = new Vector3<Integer>(x2, y, z);
        return new Axis(pos1, pos2, AxisType.X);
    }

    public Axis getYAxis() {
        Vector3<Integer> center = getCenter();
        int x = center.getX();
        int z = center.getZ();
        Vector3<Integer> pos1 = new Vector3<Integer>(x, y1, z);
        Vector3<Integer> pos2 = new Vector3<Integer>(x, y2, z);
        return new Axis(pos1, pos2, AxisType.Y);
    }

    public Axis getZAxis() {
        Vector3<Integer> center = getCenter();
        int y = center.getY();
        int x = center.getX();
        Vector3<Integer> pos1 = new Vector3<>(x, y, z1);
        Vector3<Integer> pos2 = new Vector3<>(x, y, z2);
        return new Axis(pos1, pos2, AxisType.Z);
    }

    public List<Axis> getAxises() {
        return Arrays.asList(new Axis[]{getXAxis(), getYAxis(), getZAxis()});
    }

    public List<Axis> getFlatAxises() {
        return Arrays.asList(new Axis[]{getXAxis(), getZAxis()});
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public String getWorld() {
        return world;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put(CUBOID_X1_IDENTIFIER, this.x1);
        map.put(CUBOID_Y1_IDENTIFIER, this.y1);
        map.put(CUBOID_Z1_IDENTIFIER, this.z1);
        map.put(CUBOID_X2_IDENTIFIER, this.x2);
        map.put(CUBOID_Y2_IDENTIFIER, this.y2);
        map.put(CUBOID_Z2_IDENTIFIER, this.z2);
        return map;
    }

    /**
     * Get the FakeLocation of the lower northeast corner of the Cuboid (minimum XYZ co-ordinates).
     *
     * @return FakeLocation of the lower northeast corner
     */
    public Vector3<Integer> getLowerNE() {
        return new Vector3<Integer>(this.x1, this.y1, this.z1);
    }

    /**
     * Get the FakeLocation of the upper southwest corner of the Cuboid (maximum XYZ co-ordinates).
     *
     * @return FakeLocation of the upper southwest corner
     */
    public Vector3<Integer> getUpperSW() {
        return new Vector3<Integer>(this.x2, this.y2, this.z2);
    }

    /**
     * Get the the centre of the Cuboid.
     *
     * @return FakeLocation at the centre of the Cuboid
     */
    public Vector3<Integer> getCenter() {
        int x1 = this.getUpperX() + 1;
        int y1 = this.getUpperY() + 1;
        int z1 = this.getUpperZ() + 1;
        return new Vector3<>(this.getLowerX() + (x1 - this.getLowerX()) / 2, this.getLowerY() + (y1 - this.getLowerY()) / 2, this.getLowerZ() + (z1 - this.getLowerZ()) / 2);
    }

    /**
     * Get the size of this Cuboid along the X axis
     *
     * @return Size of Cuboid along the X axis
     */
    public int getSizeX() {
        return (this.x2 - this.x1) + 1;
    }

    /**
     * Get the size of this Cuboid along the Y axis
     *
     * @return Size of Cuboid along the Y axis
     */
    public int getSizeY() {
        return (this.y2 - this.y1) + 1;
    }

    /**
     * Get the size of this Cuboid along the Z axis
     *
     * @return Size of Cuboid along the Z axis
     */
    public int getSizeZ() {
        return (this.z2 - this.z1) + 1;
    }

    /**
     * Get the minimum X co-ordinate of this Cuboid
     *
     * @return the minimum X co-ordinate
     */
    public int getLowerX() {
        return this.x1;
    }

    /**
     * Get the minimum Y co-ordinate of this Cuboid
     *
     * @return the minimum Y co-ordinate
     */
    public int getLowerY() {
        return this.y1;
    }

    /**
     * Get the minimum Z co-ordinate of this Cuboid
     *
     * @return the minimum Z co-ordinate
     */
    public int getLowerZ() {
        return this.z1;
    }

    /**
     * Get the maximum X co-ordinate of this Cuboid
     *
     * @return the maximum X co-ordinate
     */
    public int getUpperX() {
        return this.x2;
    }

    /**
     * Get the maximum Y co-ordinate of this Cuboid
     *
     * @return the maximum Y co-ordinate
     */
    public int getUpperY() {
        return this.y2;
    }

    /**
     * Get the maximum Z co-ordinate of this Cuboid
     *
     * @return the maximum Z co-ordinate
     */
    public int getUpperZ() {
        return this.z2;
    }

    public Axis getSmallestAxis() {
        return Collections.min(getAxises());
    }

    public Axis getWidestAxis() {
        return Collections.max(getAxises());
    }

    public Axis getSmallestFlatAxis() {
        return Collections.min(getFlatAxises());
    }

    public Axis getWidestFlatAxis() {
        return Collections.max(getFlatAxises());
    }

    /**
     * Expand the Cuboid in the given direction by the given amount.  Negative amounts will shrink the Cuboid in the given direction.  Shrinking a cuboid's face past the opposite face is not an error and will return a valid Cuboid.
     *
     * @param dir    - The direction in which to expand
     * @param amount - The number of blocks by which to expand
     * @return A new Cuboid expanded by the given direction and amount
     */
    public Cuboid expand(CuboidDirection dir, int amount) {
        switch (dir) {
            case North:
                return new Cuboid(this.x1 - amount, this.y1, this.z1, this.x2, this.y2, this.z2);
            case South:
                return new Cuboid(this.x1, this.y1, this.z1, this.x2 + amount, this.y2, this.z2);
            case East:
                return new Cuboid(this.x1, this.y1, this.z1 - amount, this.x2, this.y2, this.z2);
            case West:
                return new Cuboid(this.x1, this.y1, this.z1, this.x2, this.y2, this.z2 + amount);
            case Down:
                return new Cuboid(this.x1, this.y1 - amount, this.z1, this.x2, this.y2, this.z2);
            case Up:
                return new Cuboid(this.x1, this.y1, this.z1, this.x2, this.y2 + amount, this.z2);
            default:
                throw new IllegalArgumentException("Invalid direction " + dir);
        }
    }

    /**
     * Shift the Cuboid in the given direction by the given amount.
     *
     * @param dir    - The direction in which to shift
     * @param amount - The number of blocks by which to shift
     * @return A new Cuboid shifted by the given direction and amount
     */
    public Cuboid shift(CuboidDirection dir, int amount) {
        return expand(dir, amount).expand(dir.opposite(), -amount);
    }

    /**
     * Outset (grow) the Cuboid in the given direction by the given amount.
     *
     * @param dir    - The direction in which to outset (must be Horizontal, Vertical, or Both)
     * @param amount - The number of blocks by which to outset
     * @return A new Cuboid outset by the given direction and amount
     */
    public Cuboid outset(CuboidDirection dir, int amount) {
        Cuboid c;
        switch (dir) {
            case Horizontal:
                c = expand(CuboidDirection.North, amount).expand(CuboidDirection.South, amount).expand(CuboidDirection.East, amount).expand(CuboidDirection.West, amount);
                break;
            case Vertical:
                c = expand(CuboidDirection.Down, amount).expand(CuboidDirection.Up, amount);
                break;
            case Both:
                c = outset(CuboidDirection.Horizontal, amount).outset(CuboidDirection.Vertical, amount);
                break;
            default:
                throw new IllegalArgumentException("Invalid direction " + dir);
        }
        return c;
    }

    /**
     * Inset (shrink) the Cuboid in the given direction by the given amount.  Equivalent
     * to calling outset() with a negative amount.
     *
     * @param dir    - The direction in which to inset (must be Horizontal, Vertical, or Both)
     * @param amount - The number of blocks by which to inset
     * @return A new Cuboid inset by the given direction and amount
     */
    public Cuboid inset(CuboidDirection dir, int amount) {
        return this.outset(dir, -amount);
    }

    /**
     * Return true if the point at (x,y,z) is contained within this Cuboid.
     *
     * @param x - The X co-ordinate
     * @param y - The Y co-ordinate
     * @param z - The Z co-ordinate
     * @return true if the given point is within this Cuboid, false otherwise
     */
    public boolean contains(int x, int y, int z) {
        return x >= this.x1 && x <= this.x2 && y >= this.y1 && y <= this.y2 && z >= this.z1 && z <= this.z2;
    }

    /**
     * Check if the given FakeLocation is contained within this Cuboid.
     *
     * @param l - The FakeLocation to check for
     * @return true if the FakeLocation is within this Cuboid, false otherwise
     */
    public boolean contains(Vector3<?> l) {
        return this.contains(l.getX().intValue(), l.getY().intValue(), l.getZ().intValue());
    }

    /**
     * Get the volume of this Cuboid.
     *
     * @return The Cuboid volume, in blocks
     */
    public int getVolume() {
        return this.getSizeX() * this.getSizeY() * this.getSizeZ();
    }

    /**
     * Get the Cuboid representing the face of this Cuboid.  The resulting Cuboid will be one block thick in the axis perpendicular to the requested face.
     *
     * @param dir - which face of the Cuboid to loadValue
     * @return The Cuboid representing this Cuboid's requested face
     */
    public Cuboid getFace(CuboidDirection dir) {
        switch (dir) {
            case Down:
                return new Cuboid(this.x1, this.y1, this.z1, this.x2, this.y1, this.z2);
            case Up:
                return new Cuboid(this.x1, this.y2, this.z1, this.x2, this.y2, this.z2);
            case North:
                return new Cuboid(this.x1, this.y1, this.z1, this.x1, this.y2, this.z2);
            case South:
                return new Cuboid(this.x2, this.y1, this.z1, this.x2, this.y2, this.z2);
            case East:
                return new Cuboid(this.x1, this.y1, this.z1, this.x2, this.y2, this.z1);
            case West:
                return new Cuboid(this.x1, this.y1, this.z2, this.x2, this.y2, this.z2);
            default:
                throw new IllegalArgumentException("Invalid direction " + dir);
        }
    }

    /**
     * Get the Cuboid big enough to hold both this Cuboid and the given one.
     *
     * @param other - The other cuboid.
     * @return A new Cuboid large enough to hold this Cuboid and the given Cuboid
     */
    public Cuboid getBoundingCuboid(Cuboid other) {
        if (other == null) return this;

        int xMin = Math.min(this.getLowerX(), other.getLowerX());
        int yMin = Math.min(this.getLowerY(), other.getLowerY());
        int zMin = Math.min(this.getLowerZ(), other.getLowerZ());
        int xMax = Math.max(this.getUpperX(), other.getUpperX());
        int yMax = Math.max(this.getUpperY(), other.getUpperY());
        int zMax = Math.max(this.getUpperZ(), other.getUpperZ());

        return new Cuboid(xMin, yMin, zMin, xMax, yMax, zMax);
    }

    /**
     * Get a block relative to the lower NE point of the Cuboid.
     *
     * @param x - The X co-ordinate
     * @param y - The Y co-ordinate
     * @param z - The Z co-ordinate
     * @return The block at the given position
     */
    public Vector3<Integer> getRelativeBlock(int x, int y, int z) {
        return new Vector3<Integer>(this.x1 + x, this.y1 + y, this.z1 + z);
    }

    public Iterator<Vector3<Integer>> iterator() {
        return new CuboidIterator(this.x1, this.y1, this.z1, this.x2, this.y2, this.z2);
    }

    @Override
    public Cuboid clone() {
        return new Cuboid(this);
    }

    @Override
    public String toString() {
        return "Cuboid{" +
                "x1=" + x1 +
                ", y1=" + y1 +
                ", z1=" + z1 +
                ", x2=" + x2 +
                ", y2=" + y2 +
                ", z2=" + z2 +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cuboid cuboid = (Cuboid) o;

        if (x1 != cuboid.x1) return false;
        if (y1 != cuboid.y1) return false;
        if (z1 != cuboid.z1) return false;
        if (x2 != cuboid.x2) return false;
        if (y2 != cuboid.y2) return false;
        return z2 == cuboid.z2;

    }

    @Override
    public int hashCode() {
        int result = x1;
        result = 31 * result + y1;
        result = 31 * result + z1;
        result = 31 * result + x2;
        result = 31 * result + y2;
        result = 31 * result + z2;
        return result;
    }

    public enum AxisType {
        X, Y, Z, UNKNOWN
    }

    public enum CuboidDirection {
        North, East, South, West, Up, Down, Horizontal, Vertical, Both, Unknown;

        public CuboidDirection opposite() {
            switch (this) {
                case North:
                    return South;
                case East:
                    return West;
                case South:
                    return North;
                case West:
                    return East;
                case Horizontal:
                    return Vertical;
                case Vertical:
                    return Horizontal;
                case Up:
                    return Down;
                case Down:
                    return Up;
                case Both:
                    return Both;
                default:
                    return Unknown;
            }
        }

    }
}