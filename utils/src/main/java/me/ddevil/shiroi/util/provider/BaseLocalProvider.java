package me.ddevil.shiroi.util.provider;

import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.concurrent.TimeUnit;

public abstract class BaseLocalProvider<K, V> extends CacheProvider<K, V> {
    public BaseLocalProvider(LoadingCache<K, V> cache) {
        super(cache);
    }

    public BaseLocalProvider(long timeOut, TimeUnit unit) {
        super(timeOut, unit);
    }

    @Override
    protected final CacheLoader<K, V> createLoader() {
        return new CacheLoader<K, V>() {
            @Override
            public V load(K key) throws Exception {
                File file = getFile(key);
                if (!file.exists()) {
                    throw new IllegalStateException("Couldn't find file for '" + key + "'");
                }
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String s = new String();
                while (reader.ready()) {
                    s += (char) reader.read();
                }
                return BaseLocalProvider.this.loadValue(s);
            }
        };
    }

    @Override
    public final boolean has(K key) {
        return getFile(key).exists();
    }

    protected abstract V loadValue(String loadedFile);

    protected abstract File getFile(K key);

}
