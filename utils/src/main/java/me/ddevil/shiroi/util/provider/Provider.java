package me.ddevil.shiroi.util.provider;

/**
 * Created by bruno on 22/11/2016.
 */
public interface Provider<K, V> {
    V get(K key);

    void save(V object);

    boolean has(K key);

}
