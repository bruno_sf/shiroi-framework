package me.ddevil.shiroi.util;

/**
 * Created by bruno on 26/10/2016.
 */
public class MiscUtils {
    public static <E extends Comparable<E>> E min(Iterable<E> list) {
        E minValue = null;
        for (E e : list) {
            if (minValue == null || minValue.compareTo(e) > 0) {
                minValue = e;
            }
        }
        return minValue;
    }

    public static <E extends Comparable<E>> E max(Iterable<E> list) {
        E minValue = null;
        for (E e : list) {
            if (minValue == null || minValue.compareTo(e) < 0) {
                minValue = e;
            }
        }
        return minValue;
    }
}
