package me.ddevil.shiroi.util.design;

import com.google.common.collect.Maps;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by bruno on 11/10/2016.
 */
public enum MinecraftColor {
    /**
     * Represents black
     */
    BLACK('0', new Color(0, 0, 0)),
    /**
     * Represents dark blue
     */
    DARK_BLUE('1', new Color(0, 0, 128)),

    /**
     * Represents dark green
     */
    DARK_GREEN('2', new Color(0, 128, 0)),

    /**
     * Represents dark blue (aqua)
     */
    DARK_AQUA('3', new Color(0, 128, 128)),

    /**
     * Represents dark red
     */
    DARK_RED('4', new Color(128, 0, 0)),

    /**
     * Represents dark purple
     */
    DARK_PURPLE('5', new Color(128, 0, 128)),

    /**
     * Represents gold
     */
    GOLD('6', new Color(255, 165, 0)),

    /**
     * Represents gray
     */
    GRAY('7', new Color(80, 80, 80)),

    /**
     * Represents dark gray
     */
    DARK_GRAY('8', new Color(35, 35, 35)),

    /**
     * Represents blue
     */
    BLUE('9', new Color(0, 0, 255)),

    /**
     * Represents green
     */
    GREEN('a', new Color(0, 255, 0)),

    /**
     * Represents aqua
     */
    AQUA('b', new Color(0, 255, 255)),

    /**
     * Represents red
     */
    RED('c', new Color(255, 0, 0)),

    /**
     * Represents light purple
     */
    LIGHT_PURPLE('d', new Color(255, 0, 255)),

    /**
     * Represents yellow
     */
    YELLOW('e', new Color(255, 255, 0)),

    /**
     * Represents white
     */
    WHITE('f', new Color(255, 255, 255)),

    /**
     * Represents magical characters that change around randomly
     */
    MAGIC('k'),

    /**
     * Makes the text bold.
     */
    BOLD('l'),

    /**
     * Makes a line appear through the text.
     */
    STRIKETHROUGH('m'),

    /**
     * Makes the text appear underlined.
     */
    UNDERLINE('n'),

    /**
     * Makes the text italic.
     */
    ITALIC('o'),

    /**
     * Resets all previous chat colors or formats.
     */
    RESET('r');

    /**
     * The special character which prefixes all chat colour codes. Use this if
     * you need to dynamically convert colour codes from your custom format.
     */
    public static final char COLOR_CHAR = '\u00A7';
    private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("[&§][0-9a-fA-FR]");
    private static final Pattern STRIP_FORMAT_PATTERN = Pattern.compile("[&§][k-oK-OrR]");
    private final Color color;
    private final char code;
    private final boolean isFormat;
    private final String toString;
    private final static Map<Character, MinecraftColor> BY_CHAR = loadByChar();

    private static Map<Character, MinecraftColor> loadByChar() {
        Map<Character, MinecraftColor> map = Maps.newHashMap();
        for (MinecraftColor minecraftColor : MinecraftColor.values()) {
            map.put(minecraftColor.code, minecraftColor);
        }
        return map;
    }

    MinecraftColor(char code, Color color) {
        this.code = code;
        this.color = color;
        this.isFormat = false;
        this.toString = new String(new char[]{COLOR_CHAR, code});
    }

    MinecraftColor(char code) {
        this.color = null;
        this.code = code;
        this.isFormat = true;
        this.toString = new String(new char[]{COLOR_CHAR, code});
    }

    /**
     * @return The color's alternative color, for example, {@link #LIGHT_PURPLE} alternative color is {@link #DARK_PURPLE}
     */
    public MinecraftColor getAlternativeColor() {
        switch (this) {
            case AQUA:
                return DARK_AQUA;
            case BLACK:
                return WHITE;
            case BLUE:
                return DARK_BLUE;
            case DARK_AQUA:
                return AQUA;
            case DARK_BLUE:
                return BLUE;
            case DARK_GRAY:
                return GRAY;
            case DARK_GREEN:
                return GREEN;
            case DARK_PURPLE:
                return LIGHT_PURPLE;
            case GOLD:
                return YELLOW;
            case DARK_RED:
                return RED;
            case GRAY:
                return DARK_GRAY;
            case GREEN:
                return DARK_GREEN;
            case LIGHT_PURPLE:
                return DARK_PURPLE;
            case RED:
                return DARK_RED;
            case YELLOW:
                return GOLD;
            case WHITE:
                return BLACK;
            default:
                return null;
        }
    }

    /**
     * Gets the char value associated with this design
     *
     * @return A char value of this design code
     */
    public char getChar() {
        return code;
    }

    @Override
    public String toString() {
        return toString;
    }

    /**
     * Checks if this code is a format code as opposed to a design code.
     *
     * @return whether this MinecraftColor is a format code
     */
    public boolean isFormat() {
        return isFormat;
    }

    /**
     * Checks if this code is a design code as opposed to a format code.
     *
     * @return whether this MinecraftColor is a design code
     */
    public boolean isColor() {
        return !isFormat && this != RESET;
    }

    /**
     * Gets the design represented by the specified design code
     *
     * @param code Code to check
     * @return Associative {@link MinecraftColor} with the given code,
     * or null if it doesn't exist
     */
    public static MinecraftColor getByChar(char code) {
        return BY_CHAR.get(code);
    }

    /**
     * Gets the design represented by the specified design code
     *
     * @param code Code to check
     * @return Associative {@link MinecraftColor} with the given code,
     * or null if it doesn't exist
     */
    public static MinecraftColor getByChar(String code) {
        return BY_CHAR.get(code.charAt(0));
    }

    /**
     * Strips the given message of all design codes
     *
     * @param input String to strip of design
     * @return A copy of the input string, without any coloring
     */
    public static String stripAll(final String input) {
        if (input == null) {
            return null;
        }
        return stripColor(stripFormat(input));
    }

    public static String stripColor(final String input) {
        if (input == null) {
            return null;
        }
        return STRIP_COLOR_PATTERN.matcher(input).replaceAll("");
    }

    public static String stripFormat(final String input) {
        if (input == null) {
            return null;
        }
        return STRIP_FORMAT_PATTERN.matcher(input).replaceAll("");
    }

    /**
     * Translates a string using an alternate design code character into a
     * string that uses the internal MinecraftColor.COLOR_CODE design code
     * character. The alternate design code character will only be replaced if
     * it is immediately followed by 0-9, A-F, a-f, K-O, k-o, R or r.
     *
     * @param altColorChar    The alternate design code character to replace. Ex: {@literal &}
     * @param textToTranslate Text containing the alternate design code character.
     * @return Text containing the MinecraftColor.COLOR_CODE design code character.
     */
    public static String translateAlternateColorCodes(char altColorChar, String textToTranslate) {
        char[] b = textToTranslate.toCharArray();
        for (int i = 0; i < b.length - 1; i++) {
            if (b[i] == altColorChar && "0123456789AaBbCcDdEeFfKkLlMmNnOoRr".indexOf(b[i + 1]) > -1) {
                b[i] = MinecraftColor.COLOR_CHAR;
                b[i + 1] = Character.toLowerCase(b[i + 1]);
            }
        }
        return new String(b);
    }

    /**
     * Gets the ChatColors used at the end of the given input string.
     *
     * @param input Input string to retrieve the colors from.
     * @return Any remaining ChatColors to pass onto the next line.
     */
    public static String getLastColors(String input) {
        String result = "";
        int length = input.length();

        // Search backwards from the end as it is faster
        for (int index = length - 1; index > -1; index--) {
            char section = input.charAt(index);
            if (section == COLOR_CHAR && index < length - 1) {
                char c = input.charAt(index + 1);
                MinecraftColor color = getByChar(c);

                if (color != null) {
                    result = color.toString() + result;

                    // Once we find a design or reset we can stop searching
                    if (color.isColor() || color.equals(RESET)) {
                        break;
                    }
                }
            }
        }
        return result;
    }

    public Color getColor() {
        return color;
    }
}
