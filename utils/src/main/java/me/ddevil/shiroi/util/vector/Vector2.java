package me.ddevil.shiroi.util.vector;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Serializable;
import me.ddevil.shiroi.ShiroiConstants;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by bruno on 08/10/2016.
 */
public class Vector2<N extends Number> implements Serializable, java.io.Serializable {
    public static final Vector2<Double> ZERO_2 = new Vector2<>(0d, 0d);
    public static final Vector2<Integer> INT_ZERO_2 = new Vector2<>(0, 0);

    private static Number get(String key, Map<String, Object> map) {
        return ((Number) map.get(key));
    }

    public static Vector2<Integer> deserializeInt(Map<String, Object> map) {
        return new Vector2<>(
                get(ShiroiConstants.X_IDENTIFIER, map).intValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).intValue()
        );
    }

    public static Vector2<Long> deserializeLong(Map<String, Object> map) {
        return new Vector2<>(
                get(ShiroiConstants.X_IDENTIFIER, map).longValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).longValue()
        );
    }

    public static Vector2<Float> deserializeFloat(Map<String, Object> map) {
        return new Vector2<>(
                get(ShiroiConstants.X_IDENTIFIER, map).floatValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).floatValue()
        );
    }

    public static Vector2<Double> deserializeDouble(Map<String, Object> map) {
        return new Vector2<>(
                get(ShiroiConstants.X_IDENTIFIER, map).doubleValue(),
                get(ShiroiConstants.Y_IDENTIFIER, map).doubleValue()
        );
    }

    @NotNull
    protected N x;
    @NotNull
    protected N y;

    public Vector2(@NotNull N x, @NotNull N y) {
        this.x = x;
        this.y = y;
    }

    public void setX(@NotNull N x) {
        this.x = x;
    }

    public void setY(@NotNull N y) {
        this.y = y;
    }

    @NotNull
    public N getX() {
        return x;
    }

    @NotNull
    public N getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector2<?> vector2 = (Vector2<?>) o;

        if (!x.equals(vector2.x)) return false;
        return y.equals(vector2.y);

    }

    @Override
    public int hashCode() {
        int result = x.hashCode();
        result = 31 * result + y.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Vector2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(ShiroiConstants.X_IDENTIFIER, x)
                .put(ShiroiConstants.Y_IDENTIFIER, y)
                .build();
    }

    public double distance(Vector2<?> o) {
        double xDist = Math.abs(x.doubleValue() - o.x.doubleValue());
        double yDist = Math.abs(y.doubleValue() - o.y.doubleValue());
        return Math.sqrt(xDist * xDist + yDist * yDist);
    }
}
