package me.ddevil.shiroi.util.serialization;

import java.util.Map;

/**
 * Created by bruno on 24/11/2016.
 */
public interface Deserializer<T> {
    T deserialize(Map<String, Object> map);
}
