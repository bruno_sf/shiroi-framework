package me.ddevil.shiroi.util.design;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.exception.design.InvalidColorException;
import me.ddevil.shiroi.misc.Serializable;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

/**
 * Created by bruno on 11/10/2016.
 */
public class ColorDesign implements Serializable {
    public static final String PRIMARY_COLOR_IDENTIFIER = "primaryColor";
    public static final String SECONDARY_COLOR_IDENTIFIER = "secondaryColor";
    @NotNull
    private MinecraftColor primary;
    @NotNull
    private MinecraftColor secondary;

    public ColorDesign(@NotNull MinecraftColor primary, @NotNull MinecraftColor secondary) {
        if (!primary.isColor()) {
            throw new InvalidColorException(primary);
        }
        this.primary = primary;
        if (!secondary.isColor()) {
            throw new InvalidColorException(secondary);
        }
        this.secondary = secondary;
    }

    protected ColorDesign(String ps, String ss) {
        this(ps.length() > 1 ? MinecraftColor.valueOf(ps) : MinecraftColor.getByChar(ps.charAt(0)),
                ss.length() > 1 ? MinecraftColor.valueOf(ss) : MinecraftColor.getByChar(ss.charAt(0)));
    }

    public ColorDesign(Map<String, Object> map) {
        this(String.valueOf(map.get(PRIMARY_COLOR_IDENTIFIER)), String.valueOf(map.get(SECONDARY_COLOR_IDENTIFIER)));
    }

    @NotNull
    public MinecraftColor getPrimary() {
        return primary;
    }

    public void setPrimary(@NotNull MinecraftColor primary) {
        this.primary = primary;
    }

    @NotNull
    public MinecraftColor getSecondary() {
        return secondary;
    }

    public void setSecondary(@NotNull MinecraftColor secondary) {
        this.secondary = secondary;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(PRIMARY_COLOR_IDENTIFIER, primary.name())
                .put(SECONDARY_COLOR_IDENTIFIER, secondary.name())
                .build();
    }
}
