package me.ddevil.shiroi.util.area;


import me.ddevil.shiroi.util.vector.Vector3;

public class Axis implements Comparable<Axis> {
    private int x1, x2, y1, y2, z1, z2;
    private Cuboid.AxisType axisType;

    public Axis(int x1, int x2, int y1, int y2, int z1, int z2, Cuboid.AxisType axisType) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        this.z1 = z1;
        this.z2 = z2;
        this.axisType = axisType;
    }

    public Axis(Vector3<Integer> pos1, Vector3<Integer> pos2, Cuboid.AxisType axisType) {
        this(pos1.getX(), pos1.getY(), pos1.getZ(), pos2.getX(), pos2.getY(), pos2.getZ(), axisType);
    }

    public Axis mix(Axis axis) {
        Vector3<Integer> pos1 = axis.getPos1();
        Vector3<Integer> pos2 = axis.getPos2();
        Vector3<Integer> pos11 = getPos1();
        Vector3<Integer> pos21 = getPos2();
        return new Axis(
                (int) ((pos1.getX() + pos11.getX()) / 2),
                (int) ((pos1.getY() + pos11.getY()) / 2),
                (int) ((pos1.getZ() + pos11.getZ()) / 2),
                (int) ((pos2.getX() + pos21.getX()) / 2),
                (int) ((pos2.getY() + pos21.getY()) / 2),
                (int) ((pos2.getZ() + pos21.getZ()) / 2),
                Cuboid.AxisType.UNKNOWN
        );
    }

    public Vector3<Integer> getPos1() {
        return new Vector3<Integer>(x1, y1, z1);
    }

    public Vector3<Integer> getPos2() {
        return new Vector3<Integer>(x2, y2, z2);
    }

    public double getWidth() {
        return getPos1().distance(getPos2());
    }

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public int getY1() {
        return y1;
    }

    public int getY2() {
        return y2;
    }

    public int getZ1() {
        return z1;
    }

    public int getZ2() {
        return z2;
    }

    public Cuboid.AxisType getAxisType() {
        return axisType;
    }

    @Override
    public int compareTo(Axis o) {
        return Double.compare(getWidth(), o.getWidth());
    }
}
