package me.ddevil.shiroi.util.provider;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


public abstract class CacheProvider<K, V> implements Provider<K, V> {
    private final LoadingCache<K, V> cache;

    public CacheProvider(LoadingCache<K, V> cache) {
        this.cache = cache;
    }

    public CacheProvider(long timeOut, TimeUnit unit) {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(timeOut, unit)
                .build(createLoader());
    }

    protected abstract CacheLoader<K, V> createLoader();


    @Override
    public final V get(K key) {
        if (!has(key)) {
            return null;
        }
        try {
            return cache.get(key);
        } catch (ExecutionException e) {
            throw new IllegalStateException("There was a provider while loading object with key " + key + "!", e);
        }
    }
}
