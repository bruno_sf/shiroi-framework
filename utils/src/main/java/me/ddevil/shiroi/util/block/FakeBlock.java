package me.ddevil.shiroi.util.block;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Serializable;
import me.ddevil.shiroi.util.vector.BlockVector;
import me.ddevil.shiroi.util.item.Material;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

public class FakeBlock implements Serializable {
    private static final String LOCATION_IDENTIFIER = "location";
    private static final String DATA_IDENTIFIER = "data";
    private final BlockVector location;
    private Material material;
    private byte data;

    public FakeBlock(BlockVector location, Material material, byte data) {
        this.location = location;
        this.material = material;
        this.data = data;
    }

    public FakeBlock(BlockVector location, Material material) {

        this.location = location;
        this.material = material;
    }

    public BlockVector getLocation() {
        return location;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }

    public FakeBlock(BlockVector location) {
        this.location = location;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(LOCATION_IDENTIFIER, location.serialize())
                .put(Material.MATERIAL_IDENTIFIER, material.name())
                .put(DATA_IDENTIFIER, data)
                .build();
    }
}
