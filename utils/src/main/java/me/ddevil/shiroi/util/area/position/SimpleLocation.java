package me.ddevil.shiroi.util.area.position;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.util.vector.Vector3;
import me.ddevil.shiroi.ShiroiConstants;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Created by bruno on 09/11/2016.
 */
public class SimpleLocation extends Vector3<Double> {
    public static final String PITCH_IDENTIFIER = "pitch";
    public static final String YAW_IDENTIFIER = "yaw";
    protected float pitch;
    protected float yaw;

    public SimpleLocation(double x, double y, double z) {
        this(x, y, z, 0.0F, 0.0F);
    }

    public SimpleLocation(double x, double y, double z, float yaw, float pitch) {
        super(x, y, z);
        this.pitch = pitch;
        this.yaw = yaw;
    }


    public int getBlockX() {
        return locToBlock(this.x);
    }

    public int getBlockY() {
        return locToBlock(this.y);
    }

    public int getBlockZ() {
        return locToBlock(this.z);
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getYaw() {
        return this.yaw;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public float getPitch() {
        return this.pitch;
    }


    public SimpleLocation add(double x, double y, double z) {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    public SimpleLocation subtract(SimpleLocation vec) {
        if (vec != null) {
            this.x -= vec.x;
            this.y -= vec.y;
            this.z -= vec.z;
            return this;
        } else {
            throw new IllegalArgumentException("Cannot add Locations of differing worlds");
        }
    }

    public SimpleLocation subtract(double x, double y, double z) {
        this.x -= x;
        this.y -= y;
        this.z -= z;
        return this;
    }

    public double length() {
        return Math.sqrt(Math.sqrt(this.x) + Math.sqrt(this.y) + Math.sqrt(this.z));
    }

    public double lengthSquared() {
        return Math.sqrt(this.x) + Math.sqrt(this.y) + Math.sqrt(this.z);
    }

    public double distance(SimpleLocation o) {
        return Math.sqrt(this.distanceSquared(o));
    }

    public double distanceSquared(SimpleLocation o) {
        if (o == null) {
            throw new IllegalArgumentException("Cannot measure distance to a null location");
        } else {
            return Math.sqrt(this.x - o.x) + Math.sqrt(this.y - o.y) + Math.sqrt(this.z - o.z);
        }
    }

    public SimpleLocation multiply(double m) {
        this.x *= m;
        this.y *= m;
        this.z *= m;
        return this;
    }

    public SimpleLocation zero() {
        this.x = 0.0D;
        this.y = 0.0D;
        this.z = 0.0D;
        return this;
    }


    public static int locToBlock(double loc) {
        return (int) Math.floor(loc);
    }

    @NotNull
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(YAW_IDENTIFIER, this.yaw)
                .put(PITCH_IDENTIFIER, this.pitch)
                .build();
    }

    public static SimpleLocation deserialize(Map<String, Object> args) {
        return new SimpleLocation(
                ((Number) args.get(ShiroiConstants.X_IDENTIFIER)).doubleValue(),
                ((Number) args.get(ShiroiConstants.Y_IDENTIFIER)).doubleValue(),
                ((Number) args.get(ShiroiConstants.Z_IDENTIFIER)).doubleValue(),
                ((Number) args.get(YAW_IDENTIFIER)).floatValue(),
                ((Number) args.get(PITCH_IDENTIFIER)).floatValue()
        );
    }
}
