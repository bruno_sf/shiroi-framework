package me.ddevil.shiroi;

/**
 * Created by bruno on 03/10/2016.
 */
public class ShiroiConstants {
    public static final String NAMEABLE_NAME_IDENTIFIER = "name";
    public static final String NAMEABLE_ALIAS_IDENTIFIER = "alias";
    public static final String X_IDENTIFIER = "x";
    public static final String Y_IDENTIFIER = "y";
    public static final String Z_IDENTIFIER = "z";
    public static final String ALPHA_IDENTIFIER = "a";
    public static final String PARENT_IDENTIFIER = "parent";
}
