package me.ddevil.shiroi.misc.internal;

import me.ddevil.shiroi.misc.Toggleable;

/**
 * Created by bruno on 05/10/2016.
 */
public abstract class BaseToggleable implements Toggleable {
    private boolean enabled = false;

    @Override
    public final void setup() {
        enabled = true;
        setup0();
    }

    protected abstract void setup0();


    @Override
    public final void disable() {
        enabled = false;
        disable0();
    }

    protected abstract void disable0();


    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
