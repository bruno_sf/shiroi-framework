package me.ddevil.shiroi.misc.request.internal;

import me.ddevil.shiroi.misc.request.Result;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;

public abstract class HttpBaseRequest<R extends Result> extends BaseRequest<R> {


    private final HttpClient client;

    protected HttpBaseRequest() {
        this(HttpClientBuilder.create().build());
    }

    protected HttpBaseRequest(HttpClient client) {
        this.client = client;
    }

    @Override
    protected R call0(long startTime) throws Exception {
        HttpResponse response = client.execute(createRequest());
        return handle(response, startTime);
    }

    protected abstract R handle(HttpResponse response, long startTime);


    protected abstract HttpUriRequest createRequest();
}
