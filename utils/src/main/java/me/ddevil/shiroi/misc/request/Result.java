package me.ddevil.shiroi.misc.request;

public interface Result {

    long getStartTime();

    long getEndTime();

    long getTotalTime();

}
