package me.ddevil.shiroi.misc;


import org.jetbrains.annotations.NotNull;
import java.util.Map;

public interface Serializable {
    @NotNull
    Map<String, Object> serialize();
}
