package me.ddevil.shiroi.misc;

import org.jetbrains.annotations.NotNull;

public interface Nameable extends Serializable {
    @NotNull
    String getName();


    void setName(@NotNull String name);

    @NotNull
    String getAlias();

    void setAlias(@NotNull String alias);

    default String getFullName() {
        return getName() + "(" + getAlias() + ")";
    }
}
