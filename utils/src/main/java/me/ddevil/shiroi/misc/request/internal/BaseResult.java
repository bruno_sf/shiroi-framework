package me.ddevil.shiroi.misc.request.internal;

import me.ddevil.shiroi.misc.request.Result;

/**
 * Created by bruno on 08/12/2016.
 */
public class BaseResult implements Result {
    private final long startTime;
    private final long endTime;
    private final long totalTime;

    public BaseResult(long startTime, long endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.totalTime = endTime - startTime;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public long getEndTime() {
        return endTime;
    }

    @Override
    public long getTotalTime() {
        return totalTime;
    }
}
