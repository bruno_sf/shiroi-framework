package me.ddevil.shiroi.misc;

/**
 * Created by bruno on 24/09/2016.
 */
public interface Toggleable {

    void setup();

    void disable();

    boolean isEnabled();

}
