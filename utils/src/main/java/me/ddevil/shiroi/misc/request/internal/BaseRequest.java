package me.ddevil.shiroi.misc.request.internal;

import me.ddevil.shiroi.misc.request.Request;
import me.ddevil.shiroi.misc.request.Result;

/**
 * Created by bruno on 09/11/2016.
 */
public abstract class BaseRequest<T extends Result> implements Request<T> {
    protected T result;

    @Override
    public T getResult() {
        return result;
    }

    @Override
    public T call() throws Exception {
        long startTime = System.currentTimeMillis();
        return this.result = call0(startTime);
    }

    protected abstract T call0(long startTime) throws Exception;

}
