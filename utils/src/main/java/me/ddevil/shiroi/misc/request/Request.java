package me.ddevil.shiroi.misc.request;


import java.util.concurrent.Callable;

public interface Request<R extends Result> extends Callable<R> {

    R getResult();

}
