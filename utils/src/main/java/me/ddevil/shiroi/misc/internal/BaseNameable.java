package me.ddevil.shiroi.misc.internal;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Nameable;
import me.ddevil.shiroi.ShiroiConstants;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

/**
 * Created by bruno on 06/10/2016.
 */
public class BaseNameable implements Nameable {
    protected String name;
    protected String alias;

    public BaseNameable(@NotNull String name, @NotNull String alias) {
        this.name = name;
        this.alias = alias;
    }

    public BaseNameable(@NotNull Map<String, Object> map) {
        this.name = String.valueOf(map.get(ShiroiConstants.NAMEABLE_NAME_IDENTIFIER));
        this.alias = String.valueOf(map.get(ShiroiConstants.NAMEABLE_ALIAS_IDENTIFIER));
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    @Override
    public String getAlias() {
        return alias;
    }

    @Override
    public void setAlias(@NotNull String alias) {
        this.alias = alias;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(ShiroiConstants.NAMEABLE_NAME_IDENTIFIER, name)
                .put(ShiroiConstants.NAMEABLE_ALIAS_IDENTIFIER, alias)
                .build();
    }
}
