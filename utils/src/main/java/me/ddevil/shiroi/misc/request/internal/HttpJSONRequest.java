package me.ddevil.shiroi.misc.request.internal;

import me.ddevil.shiroi.misc.request.Result;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequestFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultHttpRequestFactory;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public abstract class HttpJSONRequest<R extends Result, J extends JSONAware> extends HttpBaseRequest<R> {

    private final HttpRequestFactory factory = new DefaultHttpRequestFactory();

    public HttpJSONRequest() {
        super();
    }

    protected HttpJSONRequest(HttpClient client) {
        super(client);
    }

    @Override
    protected R handle(HttpResponse response, long startTime) {
        J json;
        try {
            InputStream content = response.getEntity().getContent();
            json = (J) new JSONParser().parse(new InputStreamReader(content));
        } catch (Exception e) {
            throw new IllegalStateException("There was a problem while handling response " + response);
        }
        return handle(json, response, startTime, System.currentTimeMillis());
    }

    protected abstract R handle(J json, HttpResponse response, long startTime, long endTime);

    @Override
    protected final HttpUriRequest createRequest() {
        HttpUriRequest request = createRequest0();
        try {
            if (request instanceof HttpEntityEnclosingRequest) {
                JSONObject json = createJson();
                if (json != null) {
                    ((HttpEntityEnclosingRequest) request).setEntity(new StringEntity(json.toJSONString()));
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("There was a problem while writing json body on request!", e);
        }
        return request;
    }

    protected abstract HttpUriRequest createRequest0();

    protected abstract JSONObject createJson();

}
