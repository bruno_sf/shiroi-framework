package me.ddevil.shiroi.exception.design;

import me.ddevil.shiroi.util.design.MinecraftColor;

/**
 * Created by bruno on 14/11/2016.
 */
public class InvalidColorException extends IllegalStateException {

    private final MinecraftColor color;

    public InvalidColorException(MinecraftColor color) {
        super(color.name() + " is not a valid color!");
        this.color = color;
    }

    public MinecraftColor getColor() {
        return color;
    }
}
