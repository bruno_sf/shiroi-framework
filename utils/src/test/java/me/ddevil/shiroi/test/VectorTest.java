package me.ddevil.shiroi.test;

import me.ddevil.shiroi.util.vector.Vector2;
import me.ddevil.shiroi.util.vector.Vector3;
import org.junit.Test;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by bruno on 11/10/2016.
 */
public class VectorTest {
    private static final Logger logger = Logger.getLogger("VectorTest");

    public static void main(String[] args) {
        int i = 109283;
        String string = String.valueOf(i);
        int valorFinal = 0;
        for (char c : string.toCharArray()) {
            valorFinal += Character.getNumericValue(c);
        }
        System.out.println(valorFinal);
    }

    @Test
    public void distanceTest() {
        Vector2<Double> int2dVectorBase = new Vector2<>(0d, 0d);
        logger.info("Using " + int2dVectorBase + " as the int2dVector for testing");
        for (int i = -10; i < 10; i++) {
            double v = i * i;
            Vector2<Double> loc = new Vector2<>(v, v / 10);
            logger.info("Distance from " + int2dVectorBase + " to " + loc + " is " + int2dVectorBase.distance(loc));
        }

        Vector3<Integer> int3dVectorBase = new Vector3<>(0, 0, 0);
        logger.info("Using " + int3dVectorBase + " as the int3dVector for testing");
        for (int i = -10; i < 10; i++) {
            double v = i * i;
            Vector3<Double> loc = new Vector3<>(v, v / 10, v);
            logger.info("Distance from " + int3dVectorBase + " to " + loc + " is " + int3dVectorBase.distance(loc));
        }
    }
}
