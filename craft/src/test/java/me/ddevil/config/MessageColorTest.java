package me.ddevil.config;

import me.ddevil.shiroi.misc.PluginColorDesign;
import org.bukkit.ChatColor;
import org.json.simple.JSONObject;
import org.junit.Test;

/**
 * Created by bruno on 31/10/2016.
 */
public class MessageColorTest {


    @Test
    public void test() {
        System.out.println(translateColors("$1Imma test $2message"));
    }

    @Test
    public void designTest() {
        System.out.println(new JSONObject(PluginColorDesign.SHIROI_COLOR_DESIGN.serialize()).toJSONString());
    }

    public String translateColors(String input) {
        char[] b = input.toCharArray();
        for (int i = 0; i < b.length - 1; i++) {
            char possibleColorChar = b[i + 1];
            if (b[i] == '$' && isValidColor(possibleColorChar)) {
                b[i] = ChatColor.COLOR_CHAR;
                b[i + 1] = getColor(Character.getNumericValue(possibleColorChar));
            }
        }
        return ChatColor.translateAlternateColorCodes('&', new String(b));
    }

    public char getColor(int i) {
        switch (i) {
            case 1:
                return 'a';
            case 2:
                return 'b';
            case 3:
                return '7';
            case 4:
                return 'r';
            default:
                return 'r';
        }
    }

    public boolean isValidColor(char c) {
        return c == '1' ||
                c == '2' ||
                c == '3' ||
                c == '4';
    }
}
