package me.ddevil.shiroi.message;

import me.ddevil.shiroi.misc.PluginColorDesign;
import me.ddevil.shiroi.misc.Toggleable;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Created by bruno on 24/09/2016.
 */
public interface MessageManager extends Toggleable {

    void setColorDesign(PluginColorDesign colorDesign);

    void reload();

    void sendMessage(CommandSender p, String... messages);

    void broadcastMessage(String... messages);

    char getColor(int i);

    String translateTags(String input);

    String translateColors(String input);

    String translateAll(String input);

    List<String> translateTags(Iterable<String> input);

    List<String> translateColors(Iterable<String> input);

    List<String> translateAll(Iterable<String> input);

}
