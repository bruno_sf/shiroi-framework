package me.ddevil.shiroi.message.lang;

import me.ddevil.shiroi.config.ConfigFileKey;
import me.ddevil.shiroi.config.ConfigValue;

public interface LangValue<R extends LangRequest, K extends ConfigFileKey> extends ConfigValue<String, K> {

    String getValue(String original, R request);

}
