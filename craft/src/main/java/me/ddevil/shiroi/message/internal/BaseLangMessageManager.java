package me.ddevil.shiroi.message.internal;

import me.ddevil.shiroi.plugin.PublicPlugin;
import me.ddevil.shiroi.config.ConfigFileKey;
import me.ddevil.shiroi.config.ConfigManager;
import me.ddevil.shiroi.config.ConfigValue;
import me.ddevil.shiroi.message.LangMessageManager;
import me.ddevil.shiroi.message.lang.LangRequest;
import me.ddevil.shiroi.message.lang.LangValue;
import org.bukkit.command.CommandSender;

import java.util.*;

/**
 * Created by bruno on 17/11/2016.
 */
public abstract class BaseLangMessageManager<L extends Enum & LangValue<R, K>, K extends ConfigFileKey, R extends LangRequest, P extends PublicPlugin<?, ConfigManager<K>, ?>> extends PluginMessageManager<P> implements LangMessageManager<L, R> {
    private final HashMap<L, String> langMessageMap = new HashMap<>();
    private final List<L> langs;

    public BaseLangMessageManager(P plugin, Class<L> langClass, String pluginPrefix, String messageSeparator, char colorChar) {
        super(plugin, pluginPrefix, messageSeparator, colorChar);

        this.langs = Arrays.asList(langClass.getEnumConstants());
        reload();
    }

    public <K extends ConfigFileKey> BaseLangMessageManager(P plugin, Class<L> langClass, ConfigManager<K> configManager, ConfigValue<String, K> pluginPrefix, ConfigValue<String, K> messageSeparator, ConfigValue<String, K> colorChar) {
        this(plugin, langClass, configManager, pluginPrefix, messageSeparator, configManager.getValue(colorChar).charAt(0));
    }

    public <K extends ConfigFileKey> BaseLangMessageManager(P plugin, Class<L> langClass, ConfigManager<K> configManager, ConfigValue<String, K> pluginPrefix, ConfigValue<String, K> messageSeparator, char colorChar) {
        this(plugin, langClass, configManager.getValue(pluginPrefix), configManager.getValue(messageSeparator), colorChar);
    }

    @Override
    public final void sendMessage(CommandSender p, R request, L... messages) {
        for (L message : messages) {
            sendMessage(p, getLang(message, request));
        }
    }


    @Override
    public String getLang(L message, R request) {
        return message.getValue(langMessageMap.get(message), request);
    }

    @Override
    public void reload() {
        langMessageMap.clear();
        ConfigManager<K> configManager = plugin.getConfigManager();
        langs.forEach(l -> langMessageMap.put(l, translateColors(configManager.getValue(l))));
    }
}
