package me.ddevil.shiroi.message;

/**
 * Created by bruno on 28/09/2016.
 */
public enum MessageColor {
    PRIMARY,
    SECONDARY,
    NEUTRAL,
    WARNING;

    @Override
    public String toString() {
        return "$" + (ordinal() + 1);
    }
}
