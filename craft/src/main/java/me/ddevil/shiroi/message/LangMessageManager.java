package me.ddevil.shiroi.message;

import me.ddevil.shiroi.message.lang.LangRequest;
import me.ddevil.shiroi.message.lang.LangValue;
import org.bukkit.command.CommandSender;

/**
 * Created by bruno on 17/11/2016.
 */
public interface LangMessageManager<L extends LangValue<R, ?>, R extends LangRequest> extends MessageManager {

    void sendMessage(CommandSender p, R request, L... messages);

    String getLang(L message, R request);

}
