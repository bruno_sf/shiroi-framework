package me.ddevil.shiroi.message.internal;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.config.ConfigFileKey;
import me.ddevil.shiroi.config.ConfigManager;
import me.ddevil.shiroi.config.ConfigValue;
import org.bukkit.command.CommandSender;

/**
 * Created by bruno on 15/11/2016.
 */
public abstract class PluginMessageManager<P extends PrivatePlugin<?, ?>> extends BaseMessageManager<P> {
    private final String pluginPrefix;
    private final String messageSeparator;


    public <K extends ConfigFileKey> PluginMessageManager(P plugin, ConfigManager<K> configManager, ConfigValue<String, K> pluginPrefix, ConfigValue<String, K> messageSeparator, ConfigValue<String, K> colorChar) {
        this(plugin, configManager, pluginPrefix, messageSeparator, configManager.getValue(colorChar).charAt(0));
    }

    public <K extends ConfigFileKey> PluginMessageManager(P plugin, ConfigManager<K> configManager, ConfigValue<String, K> pluginPrefix, ConfigValue<String, K> messageSeparator, char colorChar) {
        this(plugin, configManager.getValue(pluginPrefix), configManager.getValue(messageSeparator), colorChar);
    }

    public PluginMessageManager(P plugin, String pluginPrefix, String messageSeparator, char colorChar) {
        super(plugin, colorChar);
        this.pluginPrefix = translateColors(pluginPrefix);
        this.messageSeparator = translateColors(messageSeparator);
    }

    @Override
    public String translateTags(String input) {
        return translateTags0(input.replace("{prefix}", pluginPrefix).replace("{separator}", messageSeparator));
    }

    protected abstract String translateTags0(String replace);

    @Override
    protected void sendMessage0(CommandSender p, String message) {
        p.sendMessage(pluginPrefix + messageSeparator + translateAll(message));
    }
}
