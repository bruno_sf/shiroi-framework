package me.ddevil.shiroi.message.internal;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.message.MessageManager;
import me.ddevil.shiroi.misc.PluginColorDesign;
import me.ddevil.shiroi.misc.internal.BaseToggleable;
import me.ddevil.shiroi.util.design.MinecraftColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.*;
import java.util.function.Function;

/**
 * Created by bruno on 14/11/2016.
 */
public abstract class BaseMessageManager<P extends PrivatePlugin<?, ?>> extends BaseToggleable implements MessageManager {
    protected final P plugin;
    private PluginColorDesign colorDesign;
    private final char colorChar;

    public BaseMessageManager(P plugin, char colorChar) {
        this.plugin = plugin;
        this.colorDesign = plugin.getColorDesign();
        this.colorChar = colorChar;
    }

    @Override
    public final List<String> translateTags(Iterable<String> input) {
        return translate(input, this::translateTags);
    }

    @Override
    public final List<String> translateColors(Iterable<String> input) {
        return translate(input, this::translateColors);
    }

    @Override
    public String translateAll(String input) {
        return translateColors(translateTags(input));
    }

    @Override
    public char getColor(int i) {
        MinecraftColor color;
        switch (i) {
            case 1:
                color = colorDesign.getPrimary();
                break;
            case 2:
                color = colorDesign.getSecondary();
                break;
            case 3:
                color = colorDesign.getNeutral();
                break;
            case 4:
                color = colorDesign.getWarning();
                break;
            default:
                color = MinecraftColor.RESET;
        }
        return color.getChar();
    }

    @Override
    public void broadcastMessage(String... messages) {
        translateAll(Arrays.asList(messages)).forEach(Bukkit::broadcastMessage);
    }

    @Override
    public String translateColors(String input) {
        char[] b = input.toCharArray();
        for (int i = 0; i < b.length - 1; i++) {
            if (b[i] == colorChar && isValidColor(b[i + 1])) {
                b[i] = ChatColor.COLOR_CHAR;
                b[i + 1] = getColor(Character.getNumericValue(b[i + 1]));
            }
        }
        return ChatColor.translateAlternateColorCodes('&', new String(b));
    }

    private boolean isValidColor(char c) {
        return c == '1' || c == '2' || c == '3' || c == '4';
    }

    @Override
    public final List<String> translateAll(Iterable<String> input) {
        return translate(input, this::translateAll);
    }

    private List<String> translate(Iterable<String> input, Function<String, String> function) {
        ArrayList<String> list = new ArrayList<>();
        input.forEach(s -> list.add(function.apply(s)));
        return list;
    }

    @Override
    public void setColorDesign(PluginColorDesign colorDesign) {
        this.colorDesign = colorDesign;
    }

    @Override
    public void sendMessage(CommandSender p, String... messages) {
        for (String message : messages) {
            sendMessage0(p, message);
        }
    }

    protected abstract void sendMessage0(CommandSender p, String message);


}
