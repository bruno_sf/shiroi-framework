package me.ddevil.shiroi.config;

/**
 * Created by bruno on 07/11/2016.
 */
public interface ConfigFileKey {
    String getName();

    String getResourcePath();

    String getFolderPath();

}
