package me.ddevil.shiroi.config;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.misc.PluginColorDesign;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Created by bruno on 17/11/2016.
 */
public class IndependentConfigManager<K extends Enum & ConfigFileKey> extends BaseConfigManager<K> {
    private final ConfigValue<ConfigurationSection, K> colorDesignKey;

    public IndependentConfigManager(PrivatePlugin plugin, Class<K> config, ConfigValue<ConfigurationSection, K> colorDesignKey) {
        super(plugin, config);
        this.colorDesignKey = colorDesignKey;
    }

    @Override
    public PluginColorDesign loadColorDesign() {
        return new PluginColorDesign(getValue(colorDesignKey));
    }
}
