package me.ddevil.shiroi.config;

/**
 * Created by bruno on 27/09/2016.
 */
public interface ConfigValue<T, K extends ConfigFileKey> {
    Class<T> getValueClass();

    K getKey();

    String getPath();
}
