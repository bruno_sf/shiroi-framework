package me.ddevil.shiroi.config;

/**
 * Created by bruno on 28/09/2016.
 */
public class BaseConfigValue<T, K extends ConfigFileKey> implements ConfigValue<T, K> {
    private final K key;
    private final String path;
    private final Class<T> valueClass;

    public BaseConfigValue(K key, String path, Class<T> valueClass) {
        this.key = key;
        this.path = path;
        this.valueClass = valueClass;
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public String getPath() {
        return path;
    }

}
