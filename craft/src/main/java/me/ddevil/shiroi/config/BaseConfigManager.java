package me.ddevil.shiroi.config;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.misc.BukkitToggleable;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;


public abstract class BaseConfigManager<K extends Enum & ConfigFileKey> extends BukkitToggleable<PrivatePlugin> implements ConfigManager<K> {
    private final FileConfiguration defaultConfig;
    private final Map<K, FileConfiguration> keyConfigMap;
    private final Map<String, FileConfiguration> configMap;
    private final Map<ConfigValue, Object> cache = new HashMap<>();

    public BaseConfigManager(PrivatePlugin plugin, Class<K> config) {
        super(plugin);
        configMap = new HashMap<>();
        this.keyConfigMap = Arrays.stream(config.getEnumConstants()).collect(Collectors.toMap(
                k -> k,
                k -> {
                    FileConfiguration keyConfig;
                    File file = new File(plugin.getDataFolder(), k.getFolderPath());
                    if (!file.exists()) {
                        keyConfig = plugin.loadResource(file, k.getResourcePath());
                    } else {
                        keyConfig = YamlConfiguration.loadConfiguration(file);
                    }
                    configMap.put(k.getName(), keyConfig);
                    return keyConfig;
                }
        ));
        this.defaultConfig = this.plugin.getConfig();
    }


    public Set<K> getAvailableConfigs() {
        return keyConfigMap.keySet();
    }

    @Override
    public FileConfiguration getFileConfiguration(K configName) {
        return keyConfigMap.get(configName);
    }

    @Override
    public FileConfiguration getFileConfiguration(String configName) {
        return configMap.get(configName);
    }

    @Override
    public <T> T getValue(ConfigValue<T, K> value) {

        if (value == null) {
            throw new IllegalArgumentException("You cannot get a null value!");
        }
        if (cache.containsKey(value)) {
            return (T) cache.get(value);
        }
        Object obj = getFileConfiguration(value.getKey()).get(value.getPath());
        if (obj == null) {
            throw new IllegalStateException("Couldn't find value for config value " + value.getPath() + "! Is your configuration correct?");
        }
        if (value.getValueClass().isAssignableFrom(obj.getClass())) {
            T t = (T) obj;
            cache.put(value, t);
            return t;
        } else {
            throw new IllegalArgumentException("The contained value in path " + value.getPath() + " (" + obj.getClass().getName() + ") cannot be cast to " + value.getValueClass().getName());
        }
    }


    @Override
    public void reload() {
        cache.clear();
    }
}
