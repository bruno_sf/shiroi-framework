package me.ddevil.shiroi.config;

import me.ddevil.shiroi.misc.PluginColorDesign;
import me.ddevil.shiroi.misc.Toggleable;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Set;

/**
 * Created by bruno on 27/09/2016.
 */
public interface ConfigManager<K extends ConfigFileKey> extends Toggleable {

    <T> T getValue(ConfigValue<T, K> value);

    void reload();

    Set<K> getAvailableConfigs();

    FileConfiguration getFileConfiguration(K configKey);

    FileConfiguration getFileConfiguration(String configName);

    PluginColorDesign loadColorDesign();

}
