package me.ddevil.shiroi;

/**
 * Created by bruno on 10/12/2016.
 */
public final class CraftShiroiConstants {
    public static final String SHIROI_FOLDER_NAME = "Shiroi";
    public static final String SHIROI_COLOR_FILE_NAME = "colors.yml";
    public static final String GLOBAL_CONFIG_COLOR_DESIGN_PREFIX = "mainColorDesign";
    public static final String GLOBAL_CONFIG_COLOR_DESIGN_ENABLED = "useMainColorDesign";
}
