package me.ddevil.shiroi.misc;

import me.ddevil.shiroi.plugin.ShiroiPlugin;
import me.ddevil.shiroi.message.MessageManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by BRUNO II on 15/08/2016.
 */
public class Invite implements Listener {
    private final ShiroiPlugin<?, ?> plugin;
    private final Player inviter;
    private final MessageManager messageManager;
    private final Player player;
    private final BukkitRunnable canceller;
    private final long timeOutLimit;
    private OnFinishListener listener;

    public Invite(ShiroiPlugin<?, ?> plugin, Player inviter, Player player, long timeout) {
        this.plugin = plugin;
        this.inviter = inviter;
        this.messageManager = plugin.getMessageManager();
        this.player = player;
        this.timeOutLimit = timeout;
        this.canceller = new BukkitRunnable() {
            @Override
            public void run() {
                complete(Result.TIMED_OUT);
            }
        };
    }

    public ShiroiPlugin<?, ?> getPlugin() {
        return plugin;
    }

    public Player getInviter() {
        return inviter;
    }

    public Player getPlayer() {
        return player;
    }

    public double getTimeOutLimit() {
        return timeOutLimit;
    }

    public OnFinishListener getListener() {
        return listener;
    }

    public void send(OnFinishListener listener) {
        this.listener = listener;
        plugin.registerListener(this);
        canceller.runTaskLater(plugin, timeOutLimit);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        if (player.getUniqueId().equals(this.player.getUniqueId())) {
            complete(Result.DISCONNECTED);
        }
    }

    public void complete(Result result) {
        if (listener != null) {
            listener.onResult(result);
        }
        canceller.cancel();
        plugin.unregisterListener(this);
    }

    public interface OnFinishListener {
        void onResult(Result result);
    }

    public enum Result {
        ACCEPTED,
        DECLINED,
        TIMED_OUT,
        DISCONNECTED
    }
}
