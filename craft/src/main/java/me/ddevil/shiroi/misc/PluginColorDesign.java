package me.ddevil.shiroi.misc;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.exception.design.InvalidColorException;
import me.ddevil.shiroi.util.design.ColorDesign;
import me.ddevil.shiroi.util.design.MinecraftColor;
import org.bukkit.configuration.ConfigurationSection;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

/**
 * Created by bruno on 05/11/2016.
 */
public class PluginColorDesign extends ColorDesign {
    public static final PluginColorDesign SHIROI_COLOR_DESIGN = new PluginColorDesign(MinecraftColor.AQUA, MinecraftColor.DARK_AQUA);
    private static final String NEUTRAL_COLOR_IDENTIFIER = "neutralColor";
    private static final String WARNING_COLOR_IDENTIFIER = "warningColor";
    private MinecraftColor neutralColor;
    private MinecraftColor warningColor;

    public PluginColorDesign(@NotNull MinecraftColor primary, @NotNull MinecraftColor secondary) {
        this(primary, secondary, MinecraftColor.GRAY, MinecraftColor.RED);
    }

    public PluginColorDesign(MinecraftColor primary, MinecraftColor secondary, MinecraftColor neutral, MinecraftColor warning) {
        super(primary, secondary);
        this.neutralColor = neutral;
        this.warningColor = warning;
    }

    protected PluginColorDesign(String ps, String ss, String ns, String ws) {
        super(ps, ss);
        if (ns.length() > 1) {
            this.neutralColor = MinecraftColor.valueOf(ns);
        } else {
            this.neutralColor = MinecraftColor.getByChar(ns.charAt(0));
        }
        if (!neutralColor.isColor()) {
            throw new InvalidColorException(neutralColor);
        }
        if (ws.length() > 1) {
            this.warningColor = MinecraftColor.valueOf(ns);
        } else {
            this.warningColor = MinecraftColor.getByChar(ns.charAt(0));
        }
        if (!warningColor.isColor()) {
            throw new InvalidColorException(warningColor);
        }
    }

    public PluginColorDesign(ConfigurationSection config) {
        this(config.getString(PRIMARY_COLOR_IDENTIFIER),
                config.getString(SECONDARY_COLOR_IDENTIFIER),
                config.getString(NEUTRAL_COLOR_IDENTIFIER),
                config.getString(WARNING_COLOR_IDENTIFIER));

    }

    public PluginColorDesign(Map<String, Object> map) {
        super(map);
        this.neutralColor = MinecraftColor.getByChar(String.valueOf(map.get(NEUTRAL_COLOR_IDENTIFIER)).charAt(0));
        this.warningColor = MinecraftColor.getByChar(String.valueOf(map.get(WARNING_COLOR_IDENTIFIER)).charAt(0));
    }

    public MinecraftColor getNeutral() {
        return neutralColor;
    }

    public void setNeutralColor(MinecraftColor neutralColor) {
        this.neutralColor = neutralColor;
    }

    public MinecraftColor getWarning() {
        return warningColor;
    }

    public void setWarningColor(MinecraftColor warningColor) {
        this.warningColor = warningColor;
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(NEUTRAL_COLOR_IDENTIFIER, neutralColor.name())
                .put(WARNING_COLOR_IDENTIFIER, warningColor.name())
                .build();
    }
}
