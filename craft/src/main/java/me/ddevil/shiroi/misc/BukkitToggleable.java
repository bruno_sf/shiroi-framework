package me.ddevil.shiroi.misc;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.misc.internal.BaseToggleable;
import org.bukkit.event.Listener;

/**
 * Created by bruno on 25/09/2016.
 */
public class BukkitToggleable<P extends PrivatePlugin> extends BaseToggleable implements Listener {
    protected final P plugin;

    public BukkitToggleable(P plugin) {
        this.plugin = plugin;
    }

    public P getPlugin() {
        return plugin;
    }

    @Override
    protected void setup0() {
        plugin.registerListener(this);
    }

    @Override
    protected void disable0() {
        plugin.unregisterListener(this);
    }

}
