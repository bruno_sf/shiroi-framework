/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ddevil.shiroi.task;

import java.util.ArrayList;

/**
 * @author Selma
 */
public abstract class ProgressableBukkitRunnable extends AdvancedBukkitRunnable {
    public interface UpdateListener {

        void onUpdate();
    }

    private final ArrayList<UpdateListener> updateListeners = new ArrayList<>();

    public final void addUpdateListener(final UpdateListener listener) {
        updateListeners.add(listener);
    }

    public final void removeUpdateListener(final UpdateListener listener) {
        updateListeners.remove(listener);
    }

    protected void notifyUpdateListeners() {
        updateListeners.forEach(UpdateListener::onUpdate);
    }
}
