package me.ddevil.shiroi.plugin;

import me.ddevil.shiroi.command.CommandManager;
import me.ddevil.shiroi.config.ConfigManager;
import me.ddevil.shiroi.message.MessageManager;
import me.ddevil.shiroi.misc.PluginColorDesign;
import org.jetbrains.annotations.NotNull;

/**
 * Created by bruno on 01/10/2016.
 */
public abstract class PublicPlugin<M extends MessageManager, C extends ConfigManager, CM extends CommandManager> extends AbstractShiroiPlugin<M, CM> {
    @NotNull
    protected final C configManager;
    @NotNull
    protected final M messageManager;
    @NotNull
    protected final CM commandManager;

    protected PublicPlugin() {
        this.configManager = loadConfigManager();
        this.messageManager = loadMessageManager();
        this.commandManager = loadCommandManager();
    }

    @NotNull
    protected final C loadConfigManager() {
        onLoadCommandManager();
        C commandManager = loadConfigManager0();
        commandManager.setup();
        return commandManager;
    }

    @NotNull
    protected abstract C loadConfigManager0();


    @NotNull
    @Override
    protected M loadMessageManager0() {
        return loadMessageManager1(configManager, getColorDesign());
    }


    @NotNull
    protected abstract M loadMessageManager1(@NotNull C configManager, @NotNull PluginColorDesign loadedColorDesign);


    @NotNull
    public C getConfigManager() {
        return configManager;
    }

    @Override
    protected final void doReload() {
        configManager.reload();
        doReload0();
    }

    protected abstract void doReload0();

    @NotNull
    @Override
    public M getMessageManager() {
        return messageManager;
    }

    @NotNull
    @Override
    public CM getCommandManager() {
        return commandManager;
    }
}
