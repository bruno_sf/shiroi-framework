package me.ddevil.shiroi.plugin;

import me.ddevil.shiroi.command.CommandManager;
import me.ddevil.shiroi.message.MessageManager;
import org.jetbrains.annotations.NotNull;

public abstract class PrivatePlugin<M extends MessageManager, CM extends CommandManager> extends AbstractShiroiPlugin<M, CM> {
    @NotNull
    protected final M messageManager;
    @NotNull
    protected final CM commandManager;

    public PrivatePlugin() {
        this.messageManager = loadMessageManager();
        this.commandManager = loadCommandManager();
    }

    @NotNull
    public M getMessageManager() {
        return messageManager;
    }

    @NotNull
    public CM getCommandManager() {
        return commandManager;
    }

}
