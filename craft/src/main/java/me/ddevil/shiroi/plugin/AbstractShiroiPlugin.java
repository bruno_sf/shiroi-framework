package me.ddevil.shiroi.plugin;

import me.ddevil.shiroi.CraftShiroiConstants;
import me.ddevil.shiroi.command.CommandManager;
import me.ddevil.shiroi.message.MessageColor;
import me.ddevil.shiroi.message.MessageManager;
import me.ddevil.shiroi.misc.DebugLevel;
import me.ddevil.shiroi.misc.PluginColorDesign;
import me.ddevil.shiroi.util.display.ActionBar;
import me.ddevil.shiroi.util.item.ItemUtils;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public abstract class AbstractShiroiPlugin<M extends MessageManager, C extends CommandManager> extends JavaPlugin implements ShiroiPlugin<M, C> {
    @NotNull
    private final PluginSettings pluginSettings;
    @NotNull
    private DebugLevel minimumDebugLevel;
    @NotNull
    private PluginColorDesign colorDesign;

    public AbstractShiroiPlugin() {
        if (getClass().isAnnotationPresent(PluginSettings.class)) {
            this.pluginSettings = getClass().getAnnotation(PluginSettings.class);
        } else {
            throw new IllegalStateException("Plugin author didn't specify PluginSettings in the main class!");
        }
        //Check for shiroi design
        if (pluginSettings.allowShiroiDesign()) {
            File shiroiFolder = new File(getDataFolder().getParentFile(), CraftShiroiConstants.SHIROI_FOLDER_NAME);
            File colorDesignFile = new File(shiroiFolder, CraftShiroiConstants.SHIROI_COLOR_FILE_NAME);
            if (!shiroiFolder.exists()) {
                shiroiFolder.mkdir();
            }
            if (!colorDesignFile.exists()) {
                YamlConfiguration yamlConfiguration = new YamlConfiguration();
                yamlConfiguration.set(CraftShiroiConstants.GLOBAL_CONFIG_COLOR_DESIGN_PREFIX, PluginColorDesign.SHIROI_COLOR_DESIGN.serialize());
                yamlConfiguration.set(CraftShiroiConstants.GLOBAL_CONFIG_COLOR_DESIGN_ENABLED, false);
                try {
                    yamlConfiguration.save(colorDesignFile);
                } catch (IOException e) {
                    throw new IllegalStateException("There was an error while trying to create main color design file!", e);
                }
            }


            FileConfiguration colorDesignConfig = YamlConfiguration.loadConfiguration(colorDesignFile);

            if (colorDesignConfig.getBoolean(CraftShiroiConstants.GLOBAL_CONFIG_COLOR_DESIGN_ENABLED)) {
                colorDesign = new PluginColorDesign((ConfigurationSection) colorDesignConfig.get(CraftShiroiConstants.GLOBAL_CONFIG_COLOR_DESIGN_PREFIX));
            } else {
                PluginColorDesign colorDesign = loadColorDesign();

            }

        }
        if (pluginSettings.loadConfig()) {
            //Get plugin folder
            File pluginFolder = getDataFolder();
            if (!pluginFolder.exists()) {
                debug("Plugin folder not found, making one...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                pluginFolder.mkdir();
            }
            //Configuration
            File pluginConfigFile = new File(pluginFolder, "config.yml");
            if (!pluginConfigFile.exists()) {
                //Load from plugin
                debug("Config file not found, making one...", DebugLevel.SHOULDNT_HAPPEN_BUT_WE_CAN_HANDLE_IT);
                loadResource(pluginConfigFile, "config.yml");
            }
        }
        if (pluginSettings.autoUpdate()) {
            //TODO auto updater
        }
        this.minimumDebugLevel = pluginSettings.defaultDebugLevel();
        ItemUtils.setup(this);
        ActionBar.setup(this);
    }

    @Override
    public final FileConfiguration loadResource(File saveTo, String resource) {
        if (!saveTo.exists()) {
            saveResource(resource, true);
            File savedTo = new File(getDataFolder(), resource);
            if (!savedTo.equals(saveTo)) {
                try {
                    FileUtils.moveFile(savedTo, saveTo);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
        return YamlConfiguration.loadConfiguration(saveTo);
    }

    //<editor-fold desc="Design" defaultstate="collapsed">
    @Nullable
    protected abstract PluginColorDesign tryLoadColorDesign();

    @NotNull
    private PluginColorDesign loadColorDesign() {
        @Nullable PluginColorDesign colorDesign = tryLoadColorDesign();
        if (colorDesign == null) {
            colorDesign = PluginColorDesign.SHIROI_COLOR_DESIGN;
            debug("Shiroi wasn't able to load a color design for plugin '" + getName() + "', loading default color design...");
        }
        return colorDesign;
    }

    @NotNull
    @Override
    public final PluginColorDesign getColorDesign() {
        return colorDesign;
    }

    @Override
    public final void setColorDesign(PluginColorDesign colorDesign) {
        this.colorDesign = colorDesign;
    }

    //</editor-fold>
    //<editor-fold desc="Manager handling" defaultstate="collapsed">
    public void onLoadCommandManager() {
    }

    public void onLoadMessageManager() {
    }

    @NotNull
    protected final M loadMessageManager() {
        onLoadMessageManager();
        M messageManager = loadMessageManager0();
        messageManager.setup();
        return messageManager;
    }

    protected abstract M loadMessageManager0();

    @NotNull
    protected final C loadCommandManager() {
        onLoadCommandManager();
        C commandManager = loadCommandManager0();
        commandManager.setup();
        return commandManager;
    }


    protected abstract C loadCommandManager0();

    //</editor-fold>
    //<editor-fold desc="Reloading" defaultstate="collapsed">
    @Override
    public final void reload(@NotNull CommandSender sender) {
        @NotNull M messageManager = getMessageManager();
        long start = System.currentTimeMillis();
        messageManager.sendMessage(sender, "Reloading...");
        this.colorDesign = loadColorDesign();
        doReload();

        messageManager.reload();
        messageManager.setColorDesign(getColorDesign());
        long end = System.currentTimeMillis();
        long time = end - start;
        messageManager.sendMessage(sender, "Reloaded! Took " + (time / 1000) + " seconds! " + MessageColor.SECONDARY + "(" + time + "ms)");
    }

    protected abstract void doReload();

    //</editor-fold>
    //<editor-fold desc="Debugging" defaultstate="collapsed">
    @Override
    public DebugLevel getMinimumDebugLevel() {
        return minimumDebugLevel;
    }

    @Override
    public void setMinimumDebugLevel(DebugLevel minimumDebugLevel) {
        this.minimumDebugLevel = minimumDebugLevel;
    }

    @Override
    public void debugException(String s, Throwable e) {
        debug(s);
        debug(e.getMessage());
        debug("<-- Error -->");
        e.printStackTrace();
        debug("<-- Error -->");
    }

    @Override
    public void forceDebug(String message) {
        Bukkit.getLogger().log(Level.ALL, message);
    }

    @Override
    public void debug(String message) {
        debug(message, DebugLevel.NO_BIG_DEAL);
    }

    @Override
    public final void debug(String s, DebugLevel level) {
        if (level.isHigherThan(minimumDebugLevel)) {
            Bukkit.getLogger().log(level.getJavaLevel(), s);
        }
    }

    //</editor-fold>
    //<editor-fold desc="Misc" defaultstate="collapsed">
    @Override
    public PluginSettings getPluginSettings() {
        return pluginSettings;
    }

    @Override
    public void registerListener(@NotNull Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    @Override
    public void unregisterListener(@NotNull Listener listener) {
        HandlerList.unregisterAll(listener);
    }
    //</editor-fold>
}
