package me.ddevil.shiroi.plugin;

import me.ddevil.shiroi.command.CommandManager;
import me.ddevil.shiroi.message.MessageManager;
import me.ddevil.shiroi.misc.DebugLevel;
import me.ddevil.shiroi.misc.PluginColorDesign;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * Created by bruno on 10/12/2016.
 */
public interface ShiroiPlugin<M extends MessageManager, C extends CommandManager> extends Plugin {

    PluginSettings getPluginSettings();

    @NotNull
    M getMessageManager();

    @NotNull
    C getCommandManager();

    FileConfiguration loadResource(File saveTo, String resource);

    void registerListener(@NotNull Listener listener);

    void unregisterListener(@NotNull Listener listener);

    void reload(@NotNull CommandSender sender);

    @NotNull
    PluginColorDesign getColorDesign();

    void setColorDesign(@NotNull PluginColorDesign colorDesign);

    DebugLevel getMinimumDebugLevel();

    void setMinimumDebugLevel(DebugLevel level);

    void forceDebug(String message);

    void debugException(String message, Throwable e);

    void debug(String message);

    void debug(String message, DebugLevel level);
}
