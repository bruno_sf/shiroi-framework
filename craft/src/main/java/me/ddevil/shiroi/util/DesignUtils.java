package me.ddevil.shiroi.util;

import me.ddevil.shiroi.util.design.MinecraftColor;
import org.bukkit.ChatColor;

/**
 * Created by bruno on 27/10/2016.
 */
public class DesignUtils {
    public static ChatColor toChatColor(MinecraftColor color) {
        return ChatColor.getByChar(color.getChar());
    }
}
