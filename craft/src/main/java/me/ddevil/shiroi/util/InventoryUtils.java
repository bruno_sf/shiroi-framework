package me.ddevil.shiroi.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public final class InventoryUtils {
    public static void clearInventory(Player p) {
        clearInventory(p.getInventory());
    }

    public static void dropPlayer(Player p) {
        PlayerInventory inventory = p.getInventory();
        clearInventory(inventory);
        dropInventory(inventory , p.getLocation());
    }

    public static void clearInventory(PlayerInventory inventory) {
        inventory.clear();
        inventory.setHelmet(null);
        inventory.setChestplate(null);
        inventory.setLeggings(null);
        inventory.setBoots(null);
    }

    public static void dropInventory(Inventory i, Location l) {
        World world = l.getWorld();
        for (ItemStack itemStack : i) {
            if (itemStack != null) {
                if (itemStack.getType() != Material.AIR) {
                    world.dropItem(l, itemStack);
                }
            }
        }
        i.clear();
    }
    public static Inventory createInventory(String title, int totalLanes) {
        return Bukkit.createInventory(null, totalLanes * 9, title);
    }
}
