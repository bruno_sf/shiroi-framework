package me.ddevil.shiroi.util;

import me.ddevil.shiroi.util.vector.Vector3;
import me.ddevil.shiroi.util.area.position.FakeLocation;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

/**
 * Created by bruno on 27/10/2016.
 */
public final class CraftVectorUtils {
    public static Location toLocation(World world, Vector3<?> vector) {
        return new Location(world, vector.getX().doubleValue(), vector.getY().doubleValue(), vector.getZ().doubleValue());
    }

    public static FakeLocation toFakeLocation(String world, Vector3<?> vector) {
        return new FakeLocation(world, vector.getX().doubleValue(), vector.getY().doubleValue(), vector.getZ().doubleValue());
    }

    public static Vector toVector(Vector3<?> vector) {
        return new Vector(vector.getX().doubleValue(), vector.getY().doubleValue(), vector.getZ().doubleValue());
    }

    public static Vector3<Double> fromLocation(Location location) {
        return new Vector3<>(location.getX(), location.getY(), location.getZ());
    }

    public static Vector3<Double> fromFakeLocation(FakeLocation location) {
        return new Vector3<>(location.getX(), location.getY(), location.getZ());
    }

    public static Vector3<Double> fromVector(Vector location) {
        return new Vector3<>(location.getX(), location.getY(), location.getZ());
    }
}
