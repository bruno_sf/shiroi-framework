package me.ddevil.shiroi.util.area;

import me.ddevil.shiroi.util.LocationUtils;
import me.ddevil.shiroi.util.area.position.FakeLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Map;

/**
 * Created by bruno on 27/10/2016.
 */
public class CraftCuboid extends Cuboid {

    public CraftCuboid(FakeLocation l1, FakeLocation l2) {
        super(l1, l2);
    }

    public CraftCuboid(FakeLocation l1, FakeLocation l2, String world) {
        super(l1, l2, world);
    }

    public CraftCuboid(FakeLocation l1) {
        super(l1);
    }

    public CraftCuboid(FakeLocation l1, String world) {
        super(l1, world);
    }

    public CraftCuboid(Cuboid other) {
        super(other);
    }

    public CraftCuboid(int x1, int y1, int z1, int x2, int y2, int z2) {
        super(x1, y1, z1, x2, y2, z2);
    }

    public CraftCuboid(Map<String, Object> map) {
        super(map);
    }

    public CraftCuboid(int x1, int y1, int z1, int x2, int y2, int z2, String world) {
        super(x1, y1, z1, x2, y2, z2, world);
    }

    public CraftCuboid(Location l1, Location l2) {
        this(LocationUtils.toFakeLocation(l1), LocationUtils.toFakeLocation(l2));
    }

    @Override
    public CraftAxis getXAxis() {
        return new CraftAxis(0, 0, 0, 0, 0, 0, AxisType.UNKNOWN);
    }

    public World getBukkitWorld() {
        return Bukkit.getWorld(world);
    }
}
