package me.ddevil.shiroi.util;

import me.ddevil.shiroi.util.area.position.FakeLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Created by bruno on 01/10/2016.
 */
public class LocationUtils {
    public static Location toBukkitLocation(FakeLocation location) {
        return new Location(Bukkit.getWorld(location.getWorld()), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public static FakeLocation toFakeLocation(Location location) {
        return new FakeLocation(location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }
}
