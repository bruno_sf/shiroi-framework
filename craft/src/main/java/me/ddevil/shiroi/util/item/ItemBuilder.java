/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.util.item;

import me.ddevil.shiroi.message.MessageManager;
import me.ddevil.shiroi.message.lang.LangRequest;
import me.ddevil.shiroi.message.lang.LangValue;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.Colorable;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;

import java.util.Collections;
import java.util.List;

public class ItemBuilder {
    private final MessageManager messageManager;

    public static ItemBuilder createItem(ConfigurationSection itemSection, MessageManager messageManager) throws IllegalArgumentException {
        try {
            String itemName = null;
            if (itemSection.contains("name")) {
                itemName = messageManager.translateAll(itemSection.getString("name"));
            }
            List<String> itemLore = null;
            if (itemSection.contains("lore")) {
                itemLore = messageManager.translateAll(itemSection.getStringList("lore"));
            }
            Material m = Material.valueOf(itemSection.getString("type"));
            byte data = ((Integer) itemSection.getInt("data")).byteValue();
            ItemBuilder itemBuilder = new ItemBuilder(new ItemStack(m, 1, (short) 0, data), messageManager);
            if (itemLore != null) {
                itemBuilder.setLore(itemLore);
            }
            if (itemName != null) {
                itemBuilder.setName(itemName);
            }
            return itemBuilder;
        } catch (Exception e) {
            if (itemSection == null) {
                throw new IllegalArgumentException("Given configuration section is null! ", e);
            } else {
                throw new IllegalStateException("Configuration Section " + itemSection.getCurrentPath() + " is baddly formated!", e);
            }
        }
    }

    private ItemStack is;

    public ItemBuilder(Material m, MessageManager messageManager) {
        this(m, 1, messageManager);
    }

    public ItemBuilder(ItemStack is, MessageManager messageManager) {
        this.messageManager = messageManager;
        this.is = is;
    }

    public ItemBuilder(Material m, int amount, MessageManager messageManager) {
        this.messageManager = messageManager;
        is = new ItemStack(m, amount);
    }

    public ItemBuilder clone() {
        return new ItemBuilder(is, messageManager);
    }

    public ItemBuilder setDurability(short dur) {
        is.setDurability(dur);
        return this;
    }

    public ItemBuilder setName(String name) {
        ItemMeta im = is.getItemMeta();
        if (messageManager != null) {
            if (messageManager != null) {
                name = messageManager.translateAll(name);
            }
        }
        im.setDisplayName(name);
        is.setItemMeta(im);
        return this;
    }

    /**
     * Sets the {@link Color} of a part of leather armor
     *
     * @param color the {@link Color} to use
     * @return this builder for chaining
     * @since 1.1
     */
    public ItemBuilder color(Color color) {
        Material type = is.getType();
        if (type == Material.LEATHER_BOOTS || type == Material.LEATHER_CHESTPLATE || type == Material.LEATHER_HELMET
                || type == Material.LEATHER_LEGGINGS) {
            LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
            meta.setColor(color);
            is.setItemMeta(meta);
        } else if (type == Material.WOOL) {
            Wool wool = (Wool) is.getData();
            wool.setColor(DyeColor.getByColor(color));
        } else if (type == Material.STAINED_CLAY || type == Material.STAINED_GLASS_PANE || type == Material.STAINED_CLAY) {
            is.getData().setData(DyeColor.getByColor(color).getWoolData());
        } else {
            throw new IllegalArgumentException("color() only applicable for colorable things!");
        }
        return this;
    }


    public ItemBuilder addUnsafeEnchantment(Enchantment ench, int level) {
        is.addUnsafeEnchantment(ench, level);
        return this;
    }

    public ItemBuilder removeEnchantment(Enchantment ench) {
        is.removeEnchantment(ench);
        return this;
    }

    public ItemBuilder setSkullOwner(String owner) {
        try {
            SkullMeta im = (SkullMeta) is.getItemMeta();
            im.setOwner(owner);
            is.setItemMeta(im);
        } catch (ClassCastException expected) {
        }
        return this;
    }

    public ItemBuilder addEnchant(Enchantment ench, int level) {
        ItemMeta im = is.getItemMeta();
        im.addEnchant(ench, level, true);
        is.setItemMeta(im);
        return this;
    }

    public ItemBuilder setInfinityDurability() {
        is.setDurability(Short.MAX_VALUE);
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        ItemMeta im = is.getItemMeta();
        if (messageManager != null) {
            lore = messageManager.translateAll(lore);
        }
        im.setLore(lore);
        is.setItemMeta(im);
        return this;
    }

    /**
     * Clears the lore of the {@link ItemStack}
     *
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder clearLore() {
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(Collections.emptyList());
        is.setItemMeta(meta);
        return this;
    }

    /**
     * Clears the list of {@link Enchantment}s of the {@link ItemStack}
     *
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder clearEnchantments() {
        for (final Enchantment e : is.getEnchantments().keySet()) {
            is.removeEnchantment(e);
        }
        return this;
    }

    public ItemStack toItemStack() {
        return is;
    }
}


