package me.ddevil.shiroi.util;

import org.bukkit.configuration.ConfigurationSection;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bruno on 07/12/2016.
 */
public final class CraftSerializationUtils {
    @NotNull
    public static Map<String, Object> extractConfiguration(@NotNull ConfigurationSection config) {
        HashMap<String, Object> map = new HashMap<>();
        for (Map.Entry<String, Object> entry : config.getValues(false).entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof ConfigurationSection) {
                map.put(key, extractConfiguration((ConfigurationSection) value));
            } else {
                map.put(key, value);
            }
        }
        return map;
    }
}
