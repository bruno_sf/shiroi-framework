/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */
package me.ddevil.shiroi.util.item;

import me.ddevil.shiroi.plugin.ShiroiPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Selma
 */
public final class ItemUtils {
    //geral
    public static ItemStack NA;
    public static ItemStack GRAY;

    public static ItemStack toItemStack(Item shiroiItem) {
        return new ItemStack(getBukkitMaterial(shiroiItem.getType()), shiroiItem.getAmount(), shiroiItem.getDurability());
    }

    private static Material getBukkitMaterial(me.ddevil.shiroi.util.item.Material material) {
        return Material.valueOf(material.name());
    }

    private static ShiroiPlugin<?, ?> plugin;
    private static boolean glowRegistered = false;

    public static void setup(ShiroiPlugin<?, ?> plugin) {
        ItemUtils.plugin = plugin;
        GRAY = new ItemBuilder(Material.IRON_FENCE, plugin.getMessageManager()).setName(plugin.getMessageManager().translateAll("&r")).toItemStack();
        NA = new ItemBuilder(Material.BARRIER, plugin.getMessageManager()).setName(ChatColor.RED.toString() + ChatColor.BOLD.toString() + "N/A").toItemStack();
    }

    public static String toString(ItemStack i) {
        return i.getType() + ":" + i.getData().getData();
    }

    public static ItemStack clearLore(ItemStack i) {
        ItemMeta itemMeta = i.getItemMeta();
        itemMeta.setLore(null);
        i.setItemMeta(itemMeta);
        return i;
    }

    public static ItemStack addToLore(ItemStack is, String... strings) {
        List<String> lore = getLore(is);
        for (String toAdd : strings) {
            lore.add(toAdd);
        }
        ItemStack i = new ItemStack(is);
        ItemMeta itemMeta = is.getItemMeta();
        itemMeta.setLore(lore);
        i.setItemMeta(itemMeta);
        return i;
    }


    public static ItemStack addToLore(ItemStack i, List<String> strings) {
        List<String> lore = getLore(i);
        for (String toAdd : strings) {
            lore.add(toAdd);
        }
        ItemMeta itemMeta = i.getItemMeta();
        itemMeta.setLore(lore);
        i.setItemMeta(itemMeta);
        return i;
    }

    public static void addGlow(ItemStack i) {
        if (!glowRegistered) {
            registerGlow();
        }
        Glow glow = new Glow(70);
        ItemMeta im = i.getItemMeta();
        im.addEnchant(glow, 1, true);
        i.setItemMeta(im);
    }

    public static boolean checkDisplayName(ItemStack i) {
        if (checkItemMeta(i)) {
            ItemMeta im = i.getItemMeta();
            if (im.getDisplayName() != null) {
                return true;
            }
        }

        return false;
    }

    public static boolean checkItemMeta(ItemStack i) {
        ItemMeta im = i.getItemMeta();
        return im != null;
    }

    public static boolean checkLore(ItemStack i) {
        if (checkItemMeta(i)) {
            ItemMeta im = i.getItemMeta();
            if (im.getLore() != null) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValid(ItemStack item) {
        return isValid(item, true);
    }

    public static boolean isValid(ItemStack item, boolean checkName) {
        if (item != null) {
            if (item.getItemMeta() != null) {
                if (checkName) {
                    if (item.getItemMeta().getDisplayName() != null) {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public static List<String> getLore(ItemStack i) {
        if (checkLore(i)) {
            return i.getItemMeta().getLore();
        } else {
            return new ArrayList();
        }
    }

    public static ItemStack convertFromInput(String input, String name) throws ItemConversionException {
        return new ItemBuilder(convertFromInput(input), plugin.getMessageManager()).setName(name).toItemStack();
    }

    public static ItemStack convertFromInput(String input) throws ItemConversionException {
        try {
            String[] materialanddata = input.split(":");
            Material mat = Material.valueOf(materialanddata[0]);
            Byte b;
            if (materialanddata.length > 1) {
                try {
                    b = Byte.valueOf(materialanddata[1]);
                } catch (NumberFormatException exception) {
                    plugin.debug(materialanddata[1] + " in " + input + "isn't a number! Setting byte to 0");
                    b = 0;
                }
            } else {
                b = 0;
            }
            return new ItemStack(mat, 1, (short) 0, b);
        } catch (Exception e) {
            throw new ItemConversionException(input);
        }
    }

    public static boolean equals(ItemStack a, ItemStack b) {
        if (!checkDisplayName(a) || !checkDisplayName(b)) {
            return false;
        } else {
            return a.getItemMeta().getDisplayName().equalsIgnoreCase(b.getItemMeta().getDisplayName());
        }
    }

    public static boolean equalMaterial(ItemStack a, ItemStack b) {
        if (a == null || b == null) {
            return false;
        } else {
            if (a.getType() == b.getType()) {

                if (a.getData().
                        getData()
                        == b.getData()
                        .getData() || a.
                        getData()
                        .getData()
                        == -1 || b.
                        getData()
                        .getData()
                        == -1) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void registerGlow() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {

        }
        try {
            Glow glow = new Glow(70);
            Enchantment.registerEnchantment(glow);
            glowRegistered = true;
        } catch (IllegalArgumentException e) {
        }
    }

    public static class Deserializer {

        private static final Class ITEM_META_DESERIALIZATOR = getOBCClass("inventory.CraftMetaItem").getClasses()[0];
        private static final Method DESERIALIZE = getDeserialize();

        /**
         * Gets an item back from the Map created by {@link Deserializer#serialize(ItemStack)}
         *
         * @param map The map to deserialize from.
         * @return The deserialized item.
         * @throws IllegalAccessException    Things can go wrong.
         * @throws IllegalArgumentException  Things can go wrong.
         * @throws InvocationTargetException Things can go wrong.
         */
        public static ItemStack deserialize(Map<String, Object> map) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            ItemStack i = ItemStack.deserialize(map);
            if (map.containsKey("meta")) {
                try {
                    //  org.bukkit.craftbukkit.v1_8_R3.CraftMetaItem$SerializableMeta
                    //  CraftMetaItem.SerializableMeta.deserialize(Map<String, Object>)
                    if (ITEM_META_DESERIALIZATOR != null) {
                        ItemMeta im = (ItemMeta) DESERIALIZE.invoke(i, map.get("meta"));
                        i.setItemMeta(im);
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    throw e;
                }
            }
            return i;
        }

        /**
         * Serializes an ItemStack and it's ItemMeta, use {@link Deserializer#deserialize(Map)}
         * to loadValue the item back.
         *
         * @param item Item to serialize
         * @return A HashMap with the serialized item
         */
        public static Map<String, Object> serialize(ItemStack item) {
            HashMap<String, Object> itemDocument = new HashMap(item.serialize());
            if (item.hasItemMeta()) {
                itemDocument.put("meta", new HashMap(item.getItemMeta().serialize()));
            }
            return itemDocument;
        }

        //Below here lays some crazy shit that make the above methods work :D yay!
        // <editor-fold desc="Some crazy shit" defaultstate="collapsed">
    /*
         * @return The string used in the CraftBukkit package for the version.
         */
        public static String getVersion() {
            String name = Bukkit.getServer().getClass().getPackage().getName();
            //String name = org.bukkit.craftbukkit.v1_10_R1.CraftServer.class.getPackage().getName();
            String version = name.substring(name.lastIndexOf('.') + 1) + ".";
            return version;
        }

        /**
         * @param className
         * @return
         */
        public static Class<?> getOBCClass(String className) {
            String fullName = "org.bukkit.craftbukkit." + getVersion() + className;
            Class<?> clazz = null;
            try {
                clazz = Class.forName(fullName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return clazz;
        }

        private static Method getDeserialize() {

            try {
                return ITEM_META_DESERIALIZATOR.getMethod("deserialize", Map.class);
            } catch (NoSuchMethodException | SecurityException ex) {
                return null;
            }
        }
        // </editor-fold>

    }

    private static class Glow extends Enchantment {

        public Glow(int id) {
            super(id);
        }

        @Override
        public String getName() {
            return "Glow";
        }

        @Override
        public int getMaxLevel() {
            return 1;
        }

        @Override
        public int getStartLevel() {
            return 1;
        }

        @Override
        public EnchantmentTarget getItemTarget() {
            return EnchantmentTarget.ALL;
        }

        @Override
        public boolean isTreasure() {
            return false;
        }

        @Override
        public boolean conflictsWith(Enchantment e) {
            return false;
        }

        @Override
        public boolean canEnchantItem(ItemStack is) {
            return true;
        }
    }

}

