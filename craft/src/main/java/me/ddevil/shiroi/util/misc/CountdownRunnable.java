package me.ddevil.shiroi.util.misc;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 * Created by ddevil on 02/09/2016.
 */
public abstract class CountdownRunnable extends BukkitRunnable {
    private final long startTime;
    private long currentTime;
    private final OnEndListener listener;
    private final long refreshRate;

    protected CountdownRunnable(long startTime, long refreshRate, OnEndListener listener) {
        this.startTime = startTime;
        this.refreshRate = refreshRate;
        this.currentTime = startTime;
        this.listener = listener;
    }

    @Override
    public synchronized BukkitTask runTask(Plugin plugin) throws IllegalArgumentException, IllegalStateException {
        return super.runTaskTimer(plugin, refreshRate, refreshRate);
    }

    @Override
    public synchronized BukkitTask runTaskAsynchronously(Plugin plugin) throws IllegalArgumentException, IllegalStateException {
        return super.runTaskTimerAsynchronously(plugin, refreshRate, refreshRate);
    }

    @Override
    public synchronized BukkitTask runTaskLater(Plugin plugin, long delay) throws IllegalArgumentException, IllegalStateException {
        return runTask(plugin);
    }

    @Override
    public synchronized BukkitTask runTaskLaterAsynchronously(Plugin plugin, long delay) throws IllegalArgumentException, IllegalStateException {
        return runTask(plugin);
    }

    @Override
    public synchronized BukkitTask runTaskTimer(Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
        return runTask(plugin);
    }

    @Override
    public synchronized BukkitTask runTaskTimerAsynchronously(Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
        return runTask(plugin);
    }

    public long getRefreshRate() {
        return refreshRate;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    @Override
    public void run() {
        onUpdate();
        currentTime -= refreshRate / 20;
        if (currentTime <= 0) {
            if (listener != null) {
                listener.onEnd();
            }
        }
        cancel();
    }

    protected abstract void onUpdate();

    public interface OnEndListener {
        void onEnd();
    }
}
