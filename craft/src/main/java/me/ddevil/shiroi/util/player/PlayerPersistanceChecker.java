package me.ddevil.shiroi.util.player;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.misc.BukkitToggleable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

/**
 * Created by bruno on 16/09/2016.
 */
public class PlayerPersistanceChecker extends BukkitToggleable<PrivatePlugin> {
    private final UUID player;
    private final OnQuitListener listener;

    public PlayerPersistanceChecker(PrivatePlugin plugin, OnQuitListener listener, UUID player) {
        super(plugin);
        this.listener = listener;
        this.player = player;
    }

    public PrivatePlugin getPlugin() {
        return plugin;
    }


    public UUID getPlayer() {
        return player;
    }


    public OnQuitListener getListener() {
        return listener;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        if (player.getUniqueId().equals(this.player)) {
            notifyQuit();
        }
    }

    public void notifyQuit() {
        if (listener != null) {
            listener.onQuit();
        }
        plugin.unregisterListener(this);
    }

    public interface OnQuitListener {
        void onQuit();
    }
}
