package me.ddevil.shiroi.util.area;

import me.ddevil.shiroi.util.vector.Vector3;

/**
 * Created by bruno on 27/10/2016.
 */
public class CraftAxis extends Axis {
    public CraftAxis(int x1, int x2, int y1, int y2, int z1, int z2, Cuboid.AxisType axisType) {
        super(x1, x2, y1, y2, z1, z2, axisType);
    }

    public CraftAxis(Vector3<Integer> pos1, Vector3<Integer> pos2, Cuboid.AxisType axisType) {
        super(pos1, pos2, axisType);
    }

}
