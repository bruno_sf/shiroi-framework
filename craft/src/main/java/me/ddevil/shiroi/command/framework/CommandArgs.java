/*
 *
 *  * Copyright 2015-2016 Bruno Silva Freire. All rights reserved.
 *  *
 *  *  Redistribution and use in source and binary forms, with or without modification, are
 *  *  permitted provided that the following conditions are met:
 *  *
 *  *     1. Redistributions of source code must retain the above copyright notice, this list of
 *  *        conditions and the following disclaimer.
 *  *
 *  *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  *        of conditions and the following disclaimer in the documentation and/or other materials
 *  *        provided with the distribution.
 *  *
 *  *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *
 *  *  The views and conclusions contained in the software and documentation are those of the
 *  *  authors and contributors and should not be interpreted as representing official policies,
 *  *  either expressed or implied, of anybody else.
 *
 */

package me.ddevil.shiroi.command.framework;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command Framework - CommandArgs <br>
 * This class is passed to the com.minnymin.command methods and contains various utilities as
 * well as the com.minnymin.command info.
 * 
 * @author minnymin3
 * 
 */
public class CommandArgs {

	private CommandSender sender;
	private org.bukkit.command.Command command;
	private String label;
	private String[] args;

	protected CommandArgs(CommandSender sender, org.bukkit.command.Command command, String label, String[] args,
			int subCommand) {
		String[] modArgs = new String[args.length - subCommand];
		for (int i = 0; i < args.length - subCommand; i++) {
			modArgs[i] = args[i + subCommand];
		}

		StringBuffer buffer = new StringBuffer();
		buffer.append(label);
		for (int x = 0; x < subCommand; x++) {
			buffer.append("." + args[x]);
		}
		String cmdLabel = buffer.toString();
		this.sender = sender;
		this.command = command;
		this.label = cmdLabel;
		this.args = modArgs;
	}

	/**
	 * Gets the com.minnymin.command sender
	 * 
	 * @return
	 */
	public CommandSender getSender() {
		return sender;
	}

	/**
	 * Gets the original com.minnymin.command internal
	 * 
	 * @return
	 */
	public org.bukkit.command.Command getCommand() {
		return command;
	}

	/**
	 * Gets the label including sub com.minnymin.command labels of this com.minnymin.command
	 * 
	 * @return Something like 'test.subcommand'
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Gets all the arguments after the com.minnymin.command's label. ie. if the com.minnymin.command
	 * label was test.subcommand and the arguments were subcommand foo foo, it
	 * would only return 'foo foo' because 'subcommand' is part of the com.minnymin.command
	 * 
	 * @return
	 */
	public String[] getArgs() {
		return args;
	}
	
	/**
	 * Gets the argument at the specified index
	 * @param index The index to loadValue
	 * @return The string at the specified index
	 */
	public String getArgs(int index) {
		return args[index];
	}

	/**
	 * Returns the length of the com.minnymin.command arguments
	 * @return int length of args
	 */
	public int length() {
		return args.length;
	}
	
	public boolean isPlayer() {
		return sender instanceof Player;
	}

	public Player getPlayer() {
		if (sender instanceof Player) {
			return (Player) sender;
		} else {
			return null;
		}
	}

}
