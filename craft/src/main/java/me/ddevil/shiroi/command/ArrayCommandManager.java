package me.ddevil.shiroi.command;

import me.ddevil.shiroi.plugin.PrivatePlugin;

/**
 * Created by bruno on 15/09/2016.
 */
public class ArrayCommandManager<P extends PrivatePlugin> extends CommandFrameworkCommandManager<P> {
    public ArrayCommandManager(P plugin) {
        super(plugin);
    }

    @Override
    public void setup0() {
        Class[] classes = plugin.getPluginSettings().commandClasses();
        for (Class clazz : classes) {
            try {
                registerCommand(clazz);
            } catch (Exception e) {
                plugin.debugException("There was an error while trying to register command " + clazz.getName() + "! Skipping", e);
            }
        }
    }
}
