package me.ddevil.shiroi.command;

import me.ddevil.shiroi.plugin.PrivatePlugin;
import me.ddevil.shiroi.command.framework.CommandFramework;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by BRUNO II on 05/07/2016.
 */
public class CommandFrameworkCommandManager<P extends PrivatePlugin> extends BasicCommandManager<P> {
    protected final CommandFramework framework;

    public CommandFrameworkCommandManager(P plugin) {
        super(plugin);
        this.framework = new CommandFramework(plugin);
    }

    @Override
    public void registerCommand(Class<?> command) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        if (PluginCommand.class.isAssignableFrom(command)) {
            Constructor gg = null;
            for (Constructor constructor : command.getConstructors()) {
                Class[] parameterTypes = constructor.getParameterTypes();
                if (parameterTypes.length == 1) {
                    if (PrivatePlugin.class.isAssignableFrom(parameterTypes[0])) {
                        gg = constructor;
                        break;
                    }
                }
            }
            if (gg == null) {
                throw new InstantiationError("Couldn't find a appropriate constructor!");
            }
            plugin.debug("Registering command " + command.getName());
            framework.registerCommands(gg.newInstance(plugin));
        } else {
            plugin.debug("Registering command " + command.getName());
            framework.registerCommands(command.newInstance());
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }
}
